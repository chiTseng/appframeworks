import UIKit
import GUIKit
class VC_LoginViewController: VC_Base,V_Base_Login_delegate,V_Login_Module_delegate {
    
    var v_base_login : V_Base_Login!
    var v_Login : V_Login!
    var v_Login_Module : V_Login_Module!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        v_Login_Module = V_Login_Module()
        v_Login_Module.delegate = self
        v_base_login = V_Base_Login(frame: self.view.frame)
        v_base_login.delegate = self
        self.setContaintView(v_base_login)
    }
    func v_Base_Login_CallBack(_ btn: UIButton!) {
        v_Login_Module.call_Login(account: v_base_login.txtAccount.text!, password: v_base_login.txtPassword.text!)
    }
    func update(dic:NSMutableDictionary,error:Error?) {
        print("%@",dic)
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "登入成功", message: dic["AdministratorAccount"] as? String, preferredStyle: .alert)
            let ok = UIAlertAction(title: "ok", style: .default) { (action) in
                let vc = VC_BT()
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            alert.addAction(ok)
            self.present(alert, animated: false, completion: nil)
        }
    }
}
