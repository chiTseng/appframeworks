//
//  V_Login_Module.swift
//  GSHAdminAppSample
//
//  Created by Yu Chi on 2020/1/17.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

import UIKit
import GUtility
import GAPIKit
protocol V_Login_Module_delegate {
    func update(dic:NSMutableDictionary,error:Error?)
}
class V_Login_Module: NSObject,WebServiceAPIDelegate {
    var service : AuthenticateService!
    var delegate : V_Login_Module_delegate!
    func call_Login(account:String,password:String)
    {
        service = AuthenticateService()
        service.service_LoginByDietitiansV3(withUserAccount: account, password: password, with: self)
    }
    func webServiceCallFinished(_ returnData: Data!, soapAction: String!) {
        let dic = TBXML.getAllChildData(withReturn: returnData, response: "LoginByDietitiansV3Response")
        if delegate != nil
        {
            delegate.update(dic: dic,error: nil)
        }
    }
    func webServiceCallError(_ error: Error!, soapAction: String!) {
        if delegate != nil
        {
            delegate.update(dic: NSMutableDictionary() ,error: error)
        }
    }
}
