//
//  ViewController.swift
//  GSHAdminAppSample
//
//  Created by gsh_mac_2018 on 2020/1/8.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

import UIKit
import GAPIKit
import GUtility

class ViewController: UIViewController,UITextFieldDelegate,WebServiceAPIDelegate {
    
    var scrollView : UIScrollView!
    var stackView : UIStackView!
    var input_Account : UITextField!
    var input_Password : UITextField!
    var btn_Login : UIButton!
    var service : AuthenticateService!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    func initView()
    {
        stackView = UIStackView()
        stackView.frame = self.view.frame
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.backgroundColor = .gray
        stackView.spacing = 5
        stackView.axis = .vertical
        stackView.frame.size.height = 130
        self.view.addSubview(stackView)
        
        let view1 = UIView()
        view1.frame = stackView.frame
        view1.backgroundColor = .green
        self.stackView.addArrangedSubview(view1)
        
        
        input_Account = UITextField()
        input_Account.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width * 0.8, height: 40)
        input_Account.borderStyle = .line
        input_Account.delegate = self
        input_Account.text = "gsh2015"
        view1.addSubview(input_Account)
        
        input_Password = UITextField()
        input_Password.frame = CGRect(x: 0, y: 45, width: self.view.frame.size.width * 0.8, height: 40)
        input_Password.borderStyle = .line
        input_Password.delegate = self
        input_Password.text = "Gsh53786079"
        view1.addSubview(input_Password)
        
        btn_Login = UIButton(type: .custom)
        btn_Login.frame = CGRect(x: 0, y: 90, width: 100, height: 40)
        btn_Login.setTitle("Login", for: .normal)
        btn_Login.setTitleColor(.black, for: .normal)
        btn_Login.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        btn_Login.backgroundColor = .blue
        btn_Login.addTarget(self, action: #selector(click_btn_Login), for: .touchUpInside)
        view1.addSubview(btn_Login)
        
        stackView.center = self.view.center

    }
    @objc func click_btn_Login()
    {
        input_Password.resignFirstResponder()
        input_Account.resignFirstResponder()
        service = AuthenticateService()
        service.service_LoginByDietitiansV3(withUserAccount: input_Account.text, password: input_Password.text, with: self)
        
    }
    //MARK: - textField delegate -
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK: - webServiceDelegate -
    func webServiceCallFinished(_ returnData: Data!, soapAction: String!) {
        print("==== \(soapAction)")
        let str = String(data: returnData!, encoding: .utf8)
        print(str)
        let dic = TBXML.getAllChildData(withReturn: returnData, response: "LoginByDietitiansV3Response")
        print(dic)

    }
    
    func webServiceCallError(_ error: Error!, soapAction: String!) {
        
    }
}

