//
//  VC_BT.swift
//  GSHAdminAppSample
//
//  Created by Yu Chi on 2020/1/17.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

import UIKit
import DeviceManager
import GUIKit
class VC_BT: VC_Base,BLEDeviceManagerDelegate,V_Base_BT_delegate {
    
    var blemanager : BLESeriesManager!
    var v_bt : V_Base_BT!
    override func viewDidLoad() {
        super.viewDidLoad()
        blemanager = BLESeriesManager(delegate: self, seriesType: Int32(BLEDEVICE_BT_SERIES))
        v_bt = V_Base_BT(frame: self.view.frame)
        v_bt.delegate = self
        self.setContaintView(v_bt)
    }
    func v_Base_BT_CallBack(_ btn: UIButton) {
        blemanager.startScan()
    }
    func bleManager(_ manager: Any!, updateStatus status: Int32) {
        
    }
    
    func bleManager(_ manager: Any!, device resultDevice: Any!, resultData data: NSObject!) {
        blemanager.disconnectBT()
        let resultData = (data as! BTDeviceData)
        v_bt.valueLabel.text = "\(resultData.fltTempDataResult)"
    }

    

}
