//
//  v_Login.swift
//  GSHAdminAppSample
//
//  Created by Yu Chi on 2020/1/17.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

import UIKit

protocol V_Login_delegate : NSObject {
    func v_Login_callback(btn:UIButton)
}
class V_Login: UIView,UITextFieldDelegate {

    var scrollView : UIScrollView!
    var stackView : UIStackView!
    var input_Account : UITextField!
    var input_Password : UITextField!
    var btn_Login : UIButton!
    var delegate : V_Login_delegate!
    init(frame:CGRect,delegate:V_Login_delegate) {
        super.init(frame: frame)
        self.initView(delegate: delegate)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func initView(delegate:V_Login_delegate)
    {
        self.delegate = delegate
        stackView = UIStackView()
        stackView.frame = self.frame
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.backgroundColor = .gray
        stackView.spacing = 5
        stackView.axis = .vertical
        stackView.frame.size.height = 130
        self.addSubview(stackView)
        
        let view1 = UIView()
        view1.frame = stackView.frame
        view1.backgroundColor = .green
        self.stackView.addArrangedSubview(view1)
        
        
        input_Account = UITextField()
        input_Account.frame = CGRect(x: 0, y: 0, width: self.frame.size.width * 0.8, height: 40)
        input_Account.borderStyle = .line
        input_Account.delegate = self
        input_Account.text = "gsh2015"
        view1.addSubview(input_Account)
        
        input_Password = UITextField()
        input_Password.frame = CGRect(x: 0, y: 45, width: self.frame.size.width * 0.8, height: 40)
        input_Password.borderStyle = .line
        input_Password.delegate = self
        input_Password.text = "Gsh53786079"
        view1.addSubview(input_Password)
        
        btn_Login = UIButton(type: .custom)
        btn_Login.frame = CGRect(x: 0, y: 90, width: 100, height: 40)
        btn_Login.setTitle("Login", for: .normal)
        btn_Login.setTitleColor(.black, for: .normal)
        btn_Login.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        btn_Login.backgroundColor = .blue
        btn_Login.addTarget(self, action: #selector(click_btn_Login), for: .touchUpInside)
        view1.addSubview(btn_Login)
        
        stackView.center = self.center
    }
    @objc func click_btn_Login()
    {
        input_Password.resignFirstResponder()
        input_Account.resignFirstResponder()
        if delegate != nil
        {
            self.delegate.v_Login_callback(btn: self.btn_Login)
        }
    }
    //MARK: - textField delegate -
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
