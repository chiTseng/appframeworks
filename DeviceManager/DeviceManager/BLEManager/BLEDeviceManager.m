//
//  GSHBLEDevice.m
//  SoHappy
//
//  Created by elaine on 2017/8/24.
//
//
#import "BLEDeviceManager.h"

@interface BLEDeviceManager()
{
    
    
    CBCharacteristic        *m_characteristic;
    NSMutableArray          *uuids;
    
    int                     m_connectionStatus;
    BOOL                    isScan;
    BOOL                    blnScanList;
    NSMutableDictionary     *arrPeripheral;
}
@end
@implementation BLEDeviceManager
@synthesize delegate,devices,device,strBleNames,arrBleNames;
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initBle];
    }
    return self;
}
- (instancetype)initWithDelegate:(id)_delegate devices:(NSMutableArray*)_devices
{
    self = [super init];
    if (self) {
        if (devices == nil) {
            devices = [NSMutableArray new];
        }
        devices = _devices.mutableCopy;
        self.delegate = _delegate;
        [self initBle];
    }
    return self;
}
#pragma mark - Ble setting
- (void)initBle{

    DLog(@"init");

    if (m_manager == nil) {
        @synchronized(m_manager){
            m_connectionStatus = G_BLE_STATUS_IDLE;
        }
        blnScanList = false;
        m_manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
    }
}
-(void)startScan{
    
    if (m_connectionStatus == G_BLE_STATUS_SCANNING) {
        return;
    }
    if (arrPeripheral != nil) {
        [arrPeripheral removeAllObjects];
    }
    else
    {
        arrPeripheral = [[NSMutableDictionary alloc] init];
    }
    blnScanList = false;
    [arrPeripheral removeAllObjects];
    [self performSelector:@selector(bleScan) withObject:nil afterDelay:1.5];
}
-(void)startScanList
{
    if (m_connectionStatus == G_BLE_STATUS_SCANNING) {
        return;
    }
    if (arrPeripheral != nil) {
        [arrPeripheral removeAllObjects];
    }
    else
    {
        arrPeripheral = [[NSMutableDictionary alloc] init];
    }
    blnScanList = true;
    [arrPeripheral removeAllObjects];
    [self performSelector:@selector(bleScan) withObject:nil afterDelay:1.5];
}
-(void)reScan
{
    
}
-(void)setUUIDs
{
    uuids = [NSMutableArray new];
    for (BaseBLEDevice *_device in devices) {
        if (_device.uuids.count > 0) {
            for (int i = 0; i<_device.uuids.count; i++) {
                [uuids addObject:[CBUUID UUIDWithString:[_device.uuids objectAtIndex:i]]];
            }
        }
    }
}
-(void)bleScan {
    DLog(@"startScan");
    @synchronized(m_manager){
        m_connectionStatus = G_BLE_STATUS_SCANNING;
    }
    
    if ([self isLECapableHardware]) {
        [self setUUIDs];
        NSDictionary *options = @{CBCentralManagerScanOptionAllowDuplicatesKey:@(YES)};
        [m_manager scanForPeripheralsWithServices:uuids options:options];
        DLog(@"%@", uuids);
        if (delegate != nil) {
            [delegate bleManager:self updateStatus:G_BLE_STATUS_SCANNING];
        }
    }
    else
    {
        if (delegate != nil) {
            [delegate bleManager:self updateStatus:G_BLE_ERROR];
        }
    }
}

-(void)stopScan{
    DLog(@"stopScan");
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(bleScan) object:nil];
    @synchronized(m_manager){
        if(m_connectionStatus != G_BLE_STATUS_CONNECTED){
            m_connectionStatus = G_BLE_STATUS_IDLE;
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BLE_STOP" object:nil];
    [m_manager stopScan];

    
}
-(void)disconnectBT
{
    [self stopScan];
    [self disconnectDevice:m_peripheral];
    m_peripheral = nil;
    m_characteristic = nil;
    
}

- (void)disconnectDevice: (CBPeripheral *)aPeripheral {
    DLog(@"disconnectDevice %@",aPeripheral.identifier.UUIDString);
    @synchronized(m_manager){
        switch (m_connectionStatus) {
            case G_BLE_STATUS_CONNECTED:
            case G_BLE_STATUS_CONNECTING:
            case G_BLE_STATUS_SCANNING:
                m_connectionStatus = G_BLE_STATUS_DISCONNECTED_BYUSER;
                break;
                
            default:{
                DLog(@"%s:do not need dissconnect",__FUNCTION__);
                return;
                break;
            }
        }
    }
    if(m_peripheral) {
        [m_manager cancelPeripheralConnection:aPeripheral];
        if (delegate != nil) {
            [delegate bleManager:self updateStatus:G_BLE_STATUS_DISCONNECTED_BYUSER];
        }
    }
    
}

-(void)showErrorMessage:(NSString *)message {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:message  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
    [self stopScan];
}

-(void)checkLECapableHardware {
    [self isLECapableHardware];
}

- (BOOL) isLECapableHardware
{
    if(!m_manager){
        DLog(@"CBCentralManager == nil");
        return NO;
    }
    NSString * state = nil;
    switch ([m_manager state])
    {
        case CBCentralManagerStateUnsupported:
            state = NSLocalizedString(@"The platform/hardware doesn't support Bluetooth Low Energy.", @"The platform/hardware doesn't support Bluetooth Low Energy.");
            [self showErrorMessage:state];
            DLog(@"Central manager state: %@", state);
            break;
        case CBCentralManagerStateUnauthorized:
            state = NSLocalizedString(@"The app is not authorized to use Bluetooth Low Energy.",nil);
            [self showErrorMessage:state];
            DLog(@"Central manager state: %@", state);
            break;
        case CBCentralManagerStatePoweredOff:
            state = NSLocalizedString(@"Bluetooth is currently powered off.",nil);
            [self showErrorMessage:state];
            DLog(@"Central manager state: %@", state);
            break;
        case CBCentralManagerStatePoweredOn:
            DLog(@"Central manager state: CBCentralManagerStatePoweredOn");
            return TRUE;
        case CBCentralManagerStateResetting:
            DLog(@"CBCentralManagerStateResetting");
        case CBCentralManagerStateUnknown:
        default:
            state = NSLocalizedString(@"Bluetooth manager state: Unknow",nil);
            DLog(@"Central manager state: Unknow");
            [self showErrorMessage:state];
            return FALSE;
    }
    return FALSE;
}
- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    DLog(@"centralManagerDidUpdateState state = %ld", (long)central.state);
    
}
- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (blnScanList == false) {
        [self bleScanConnect:central didDiscoverPeripheral:aPeripheral advertisementData:advertisementData RSSI:RSSI];
    }
    else
    {
        /*if ([self.strBleNames containsString:aPeripheral.name]) {
            
        
            NSString *name = aPeripheral.name;
            NSString *uuid = aPeripheral.identifier.UUIDString;
            NSMutableDictionary *deviceInfo = [[NSMutableDictionary alloc] init];
            NSDate *date = [NSDate new];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            NSString *strDate = [formatter stringFromDate:date];
            NSNumber *number = [NSNumber numberWithInteger:date.timeIntervalSince1970];
            [deviceInfo setValue:name forKey:@"name"];
            [deviceInfo setValue:uuid forKey:@"uuid"];
            [deviceInfo setValue:RSSI forKey:@"rssi"];
            [deviceInfo setValue:advertisementData forKey:@"advertisementData"];
            [deviceInfo setValue:aPeripheral forKey:@"aPeripheral"];
            [deviceInfo setValue:strDate forKey:@"lastUpdatedate"];
            [deviceInfo setValue:number forKey:@"timeIntervalSince1970"];
            [arrPeripheral setValue:deviceInfo forKey:uuid];
        }*/
        if (aPeripheral == nil) {
            return;
        }
        for (int i = 0; i<self.arrBleNames.count; i++) {
            if (aPeripheral.name == nil || [aPeripheral.name isEqualToString:@""]) {
                return;
            }
            
            NSArray *arrNames = [self.arrBleNames[i] componentsSeparatedByString:@","];
            
            if (arrNames.count > 1) {
                for (int j = 0; j < arrNames.count; j++) {
                    if ([aPeripheral.name isEqualToString:arrNames[j]]) {
                        NSString *name = aPeripheral.name;
                        NSString *uuid = aPeripheral.identifier.UUIDString;
                        NSMutableDictionary *deviceInfo = [[NSMutableDictionary alloc] init];
                        NSDate *date = [NSDate new];
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                        NSString *strDate = [formatter stringFromDate:date];
                        NSNumber *number = [NSNumber numberWithInteger:date.timeIntervalSince1970];
                        [deviceInfo setValue:name forKey:@"name"];
                        [deviceInfo setValue:uuid forKey:@"uuid"];
                        [deviceInfo setValue:RSSI forKey:@"rssi"];
                        [deviceInfo setValue:advertisementData forKey:@"advertisementData"];
                        [deviceInfo setValue:aPeripheral forKey:@"aPeripheral"];
                        [deviceInfo setValue:strDate forKey:@"lastUpdatedate"];
                        [deviceInfo setValue:number forKey:@"timeIntervalSince1970"];
                        [arrPeripheral setValue:deviceInfo forKey:uuid];
                        break;
                    }
                }
                
            }
            else
            {
                if ([aPeripheral.name containsString:self.arrBleNames[i]]) {
                    NSString *name = aPeripheral.name;
                    NSString *uuid = aPeripheral.identifier.UUIDString;
                    NSMutableDictionary *deviceInfo = [[NSMutableDictionary alloc] init];
                    NSDate *date = [NSDate new];
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSString *strDate = [formatter stringFromDate:date];
                    NSNumber *number = [NSNumber numberWithInteger:date.timeIntervalSince1970];
                    [deviceInfo setValue:name forKey:@"name"];
                    [deviceInfo setValue:uuid forKey:@"uuid"];
                    [deviceInfo setValue:RSSI forKey:@"rssi"];
                    [deviceInfo setValue:advertisementData forKey:@"advertisementData"];
                    [deviceInfo setValue:aPeripheral forKey:@"aPeripheral"];
                    [deviceInfo setValue:strDate forKey:@"lastUpdatedate"];
                    [deviceInfo setValue:number forKey:@"timeIntervalSince1970"];
                    [arrPeripheral setValue:deviceInfo forKey:uuid];
                    break;
                }
            }
            
           
        }
        
        
        if (delegate != nil)
        {
            if ([delegate respondsToSelector:@selector(bleManager:deviceList:)]) {
                [delegate bleManager:self deviceList:arrPeripheral];
            }
            
        }
    }
}
- (BOOL)connectDeviceByUUID:(NSString *)uuid
{
    
    NSMutableDictionary *deviceInfo = arrPeripheral[uuid];
    if (deviceInfo != nil) {
        m_peripheral = deviceInfo[@"aPeripheral"];
        NSDictionary *advertisementData = deviceInfo[@"advertisementData"];
        NSNumber *RSSI = deviceInfo[@"rssi"];
        [self bleScanConnect:m_manager didDiscoverPeripheral:m_peripheral advertisementData:advertisementData RSSI:RSSI];
        return true;
    }
    return false;
}
-(void)bleScanConnect:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (device != nil) {
        device.delegate = nil;
        device = nil;
    }
    DLog(@"%@", aPeripheral.name);
    DLog(@"%@",devices);
    for (int i = 0; i < devices.count; i++) {
        BaseBLEDevice *bleDevice = devices[i];
        DLog(@"======>%@<======", bleDevice.deviceName);
        NSArray *arrNames = [bleDevice.deviceName componentsSeparatedByString:@","];
        for (NSString *name in arrNames) {
            if ([name isEqualToString:aPeripheral.name])
            {
                device = devices[i];
                device.delegate = self;
                [m_manager stopScan];
                break;
            }
        }
        
    }
    if (device == nil) {
        return;
    }
    DLog(@"didDiscoverPeripheral RSSI=%d Name=%@ deviceName = %@",[RSSI intValue], aPeripheral.name,device.deviceName);
    
    @synchronized(m_manager){
        m_connectionStatus = G_BLE_STATUS_CONNECTING;
    }
    device.readSystemInfoComplete = 0;
    device.readSystemInfoStatus = 0;
    device.hardwareVersion = @"";
    device.manufacturerName = @"";
    device.softwareVersion = @"";
    device.firmwareVersion = @"";
    device.macAddress = @"";
    NSData *manufacturerData = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
    NSString *macAddress = [[manufacturerData description] stringByReplacingOccurrencesOfString:@" " withString:@""];
    device.macAddress = [[macAddress substringWithRange:NSMakeRange(1, [macAddress length] - 2)] substringFromIndex:4];
    DLog(@"device.macAddress is %@",device.macAddress);
    m_peripheral = aPeripheral;
    BOOL blnOK = [self connectDevice:central didDiscoverPeripheral:advertisementData RSSI:RSSI];
    if (blnOK == YES) {
        if (delegate != nil) {
            [delegate bleManager:self updateStatus:G_BLE_STATUS_CONNECTING];
            
        }
    }
    else
    {
        [self bleScan];
    }
}
-(BOOL)connectDevice:(CBCentralManager *)central didDiscoverPeripheral:(NSDictionary *)advertisementData RSSI:(NSNumber*)RSSI
{
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBConnectPeripheralOptionNotifyOnDisconnectionKey, nil];
    [device didDiscoverPeripheralHandler:central didDiscoverPeripheral:m_peripheral advertisementData:advertisementData RSSI:RSSI];
    [m_manager connectPeripheral:m_peripheral options:options];
    return YES;
    
}
- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    @synchronized(m_manager){
        m_connectionStatus = G_BLE_STATUS_CONNECTED;
    }
    DLog(@"didConnectPeripheral %@", aPeripheral.identifier.UUIDString);
    m_peripheral = aPeripheral;
    [m_peripheral setDelegate:self];
    device.connectIOSUUID = aPeripheral.identifier.UUIDString;
    device.connectIOSName = aPeripheral.name;
    [device didConnectPeripheralHandler:central didConnectPeripheral:aPeripheral];
    [m_peripheral discoverServices:uuids];
    if (delegate != nil) {
        [delegate bleManager:self updateStatus:G_BLE_STATUS_CONNECTED];
    }
}
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    DLog(@"didDisonnectPeripheral uuid = %@, error msg:%ld, %@, %@", aPeripheral.identifier.UUIDString, (long)error.code ,[error localizedFailureReason], [error localizedDescription]);
    [m_manager stopScan];
    m_peripheral = nil;
    m_characteristic = nil;
    m_connectionStatus = G_BLE_STATUS_IDLE;
    BOOL ret = [device didDisconnectPeripheral:central didDisconnectPeripheral:aPeripheral error:error];
    if ( ret == true && delegate != nil) {
        [delegate bleManager:self updateStatus:G_BLE_STATUS_DISCONNECTED];
    }
    
}
- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverServices:(NSError *)error
{
    for (CBService *aService in aPeripheral.services)
    {
        DLog(@"Service found with UUID: %@", aService.UUID);
        [aPeripheral discoverCharacteristics:nil forService:aService];
    }
}

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    DLog(@"didDiscoverCharacteristicsForService: %@", service.UUID);
    [device didDiscoverCharacteristicsForService:aPeripheral didDiscoverCharacteristicsForService:service error:error];
}


- (void) peripheral:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    @synchronized(m_manager){
        if(m_connectionStatus != G_BLE_STATUS_CONNECTED){
            DLog(@"Status Error :%d! DisconnectDevice!",m_connectionStatus);
        }
    }
    [device didUpdateValueForCharacteristic:aPeripheral didUpdateValueForCharacteristic:characteristic error:error];
}

- (void) peripheral:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"didWriteValueForCharacteristic error msg:%ld, %@, %@", (long)error.code ,[error localizedFailureReason], [error localizedDescription]);
    [device didWriteValueForCharacteristic:aPeripheral didWriteValueForCharacteristic:characteristic error:error];
}

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"didDiscoverDescriptorsForCharacteristic error msg:%ld, %@, %@", (long)error.code ,[error localizedFailureReason], [error localizedDescription]);
}

- (void) peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
    DLog(@"didUpdateValueForDescriptor");
}

-(void) peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"didUpdateNotificationStateForCharacteristic");
    [device didUpdateNotificationStateForCharacteristic:peripheral didUpdateNotificationStateForCharacteristic:characteristic error:error];
}
-(void) baseDevice:(id)resultDevice resultData:(NSObject*)data
{
    if (delegate != nil) {
        [delegate bleManager:self device:resultDevice resultData:data];
    }
}
-(void) baseDevice:(id)device
{
    if (delegate != nil)
    {
        if ([delegate respondsToSelector:@selector(bleManager:device:)]) {
            [delegate bleManager:self device:device];
        }
        
    }
}
@end
