//
//  BLETypesManager.m
//  SoHappy
//
//  Created by elaine on 2017/9/21.
//
//

#import "BLESeriesManager.h"

@interface BLESeriesManager()
{
    int intSeriesType;
    BOOL blnIsDebug;
    int intRssi;
    NSString *strUUIDString;
}
@end
/**
 使用者可以直接利用seriesType 來生成對應的系列
 */
@implementation BLESeriesManager

/**
 利用seriesType 來主動生成各系列設備 主要有BS BT BP BW ECG 五大系列
 */
- (instancetype)initWithDelegate:(id)delegate seriesType:(int)seriesType
{
    self = [super init];
    self.delegate = delegate;
    self.devices = [self createBLESeries:seriesType];
    return self;
}
- (instancetype)initWithDelegate:(id)delegate seriesType:(int)seriesType cusDevices:(NSMutableArray*)cusDevices
{
    self = [super init];
    self.delegate = delegate;
    self.devices = [self createBLESeries:seriesType];
    for (BaseBLEDevice*device in cusDevices) {
        [self.devices addObject:device];
    }
    return self;
}
-(instancetype)initDebugWithDelegate:(id)delegate seriesType:(int)seriesType RSSI:(int)rssi UUIDString:(NSString *)uuidString
{
    self = [super init];
    blnIsDebug = YES;
    intRssi = rssi;
    strUUIDString = uuidString;
    return [self initWithDelegate:delegate seriesType:seriesType];
    
}
-(NSMutableArray*)createBLESeries:(int)seriesType
{
    self.strBleNames = @"";
    self.arrBleNames = [[NSMutableArray alloc] init];
    NSMutableArray *arrDevices = [NSMutableArray new];
    switch (seriesType) {

        case BLEDEVICE_BP_SERIES:
        {
            BP822BleDevice      *bp822BleDevice = [BP822BleDevice new];
            BP0802A0BleDevice   *bp0802A0Device = [BP0802A0BleDevice new];
            BPGSHBPBleDevice    *bpGSHBPBleDevice = [BPGSHBPBleDevice new];
            BPGSHAVE2000BleDevice *bpGSHAVE2000BleDevice = [BPGSHAVE2000BleDevice new];
            BPGSH885BBleDevice * bpGSH885BBleDevice = [BPGSH885BBleDevice new];
            BPGSH823B *bpGSH823B = [BPGSH823B new];
            BPGSH822B *bpGSH822B = [BPGSH822B new];
            
            //BPZOES2BleDevice *bpZOES2BleDevice = [BPZOES2BleDevice new];
            [arrDevices addObject:bp0802A0Device];
            [arrDevices addObject:bp822BleDevice];
            [arrDevices addObject:bpGSHBPBleDevice];
            [arrDevices addObject:bpGSHAVE2000BleDevice];
            [arrDevices addObject:bpGSH885BBleDevice];
            [arrDevices addObject:bpGSH823B];
            [arrDevices addObject:bpGSH822B];
            self.strBleNames = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",bp0802A0Device.deviceName,bp822BleDevice.deviceName,bpGSHBPBleDevice.deviceName,bpGSHAVE2000BleDevice.deviceName,bpGSH885BBleDevice.deviceName];
            [self.arrBleNames addObject:bp0802A0Device.deviceName];
            [self.arrBleNames addObject:bp822BleDevice.deviceName];
            [self.arrBleNames addObject:bpGSHBPBleDevice.deviceName];
            [self.arrBleNames addObject:bpGSHAVE2000BleDevice.deviceName];
            [self.arrBleNames addObject:bpGSH885BBleDevice.deviceName];
            [self.arrBleNames addObject:bpGSH823B.deviceName];
            [self.arrBleNames addObject:bpGSH822B.deviceName];
        }
            break;
        case BLEDEVICE_BW_SERIES:
        {
            BWBleDevice         *bwBleDevice = [BWBleDevice new];
            BWBC418Device       *bwBC418Device = [BWBC418Device new];
            //BWG950BleDevice     *bwG950BleDevice = [BWG950BleDevice new];
            //G950需設定身高 年齡 性別 故無法用series方式來創建使用
            [arrDevices addObject:bwBleDevice];
            [arrDevices addObject:bwBC418Device];
            //[arrDevices addObject:bwG950BleDevice];
            self.strBleNames = [NSString stringWithFormat:@"%@,%@",bwBleDevice.deviceName,bwBC418Device.deviceName];
            [self.arrBleNames addObject:bwBleDevice.deviceName];
            [self.arrBleNames addObject:bwBC418Device.deviceName];
        }
            break;
        case BLEDEVICE_BT_SERIES:
        {
            BTBleDevice         *btBleDevice = [BTBleDevice new];
            BTBTS381BleDevice   *btBts381Device = [BTBTS381BleDevice new];
            [arrDevices addObject:btBleDevice];
            [arrDevices addObject:btBts381Device];
            self.strBleNames = [NSString stringWithFormat:@"%@,%@",btBleDevice.deviceName,btBts381Device.deviceName];
            [self.arrBleNames addObject:btBleDevice.deviceName];
            [self.arrBleNames addObject:btBts381Device.deviceName];
        }
            break;
        case BLEDEVICE_BS_SERIES:
        {
            BSBleDevice         *bsBleDevice = [BSBleDevice new];
            GLM76BleDevice      *glm76BleDevice = [GLM76BleDevice new];
            GSHBGM902Device     *gshbgm902Device = [GSHBGM902Device new];
            [arrDevices addObject:bsBleDevice];
            [arrDevices addObject:glm76BleDevice];
            [arrDevices addObject:gshbgm902Device];
            self.strBleNames = [NSString stringWithFormat:@"%@,%@,%@",bsBleDevice.deviceName,glm76BleDevice.deviceName,gshbgm902Device.deviceName];
            [self.arrBleNames addObject:bsBleDevice.deviceName];
            [self.arrBleNames addObject:glm76BleDevice.deviceName];
            [self.arrBleNames addObject:gshbgm902Device.deviceName];
        }
            break;
        case BLEDEVICE_ECG_SERIES:
        {
            ECGBleDevice        *ecgBleDevice = [ECGBleDevice new];
            GSH711BleDevice     *ecg711BleDevice = [GSH711BleDevice new];
            GSH752BleDevice     *ecg752BleDevice = [GSH752BleDevice new];
            [arrDevices addObject:ecgBleDevice];
            [arrDevices addObject:ecg711BleDevice];
            [arrDevices addObject:ecg752BleDevice];
            self.strBleNames = [NSString stringWithFormat:@"%@,%@,%@",ecgBleDevice.deviceName,ecg711BleDevice.deviceName,ecg752BleDevice.deviceName];
            [self.arrBleNames addObject:ecgBleDevice.deviceName];
            [self.arrBleNames addObject:ecg711BleDevice.deviceName];
            [self.arrBleNames addObject:ecg752BleDevice.deviceName];
        }
            break;
        default:
            break;
    }
    return arrDevices.mutableCopy;
}
-(void) changeSeriesTypeAndRescan:(int)seriesType
{
    [self disconnectBT];
    self.device = nil;
    self.devices = [self createBLESeries:seriesType];
    [self startScan];
    
}
-(void) changeSeriesTypeAndRescanByScanList:(int)seriesType
{
    [self disconnectBT];
    self.device = nil;
    self.devices = [self createBLESeries:seriesType];
    [self startScanList];
}
-(BOOL)connectDevice:(CBCentralManager *)central didDiscoverPeripheral:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (blnIsDebug == NO) {
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBConnectPeripheralOptionNotifyOnDisconnectionKey, nil];
        [self.device didDiscoverPeripheralHandler:central didDiscoverPeripheral:self->m_peripheral advertisementData:advertisementData RSSI:RSSI];
        [self->m_manager connectPeripheral:self->m_peripheral options:options];
        
        return YES;
    }
    else
    {
        if (RSSI.intValue > intRssi && [m_peripheral.identifier.UUIDString isEqualToString:strUUIDString]) {
            NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBConnectPeripheralOptionNotifyOnDisconnectionKey, nil];
            [self.device didDiscoverPeripheralHandler:central didDiscoverPeripheral:self->m_peripheral advertisementData:advertisementData RSSI:RSSI];
            [self->m_manager connectPeripheral:self->m_peripheral options:options];
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
}
@end
