//
//  BLETypesManager.h
//  SoHappy
//
//  Created by elaine on 2017/9/21.
//
//

#import "BLEDeviceManager.h"//繼承至這個管理器

#import "BSBleDevice.h"//BS系列設備
#import "GLM76BleDevice.h"
#import "GSHBGM902Device.h"

#import "ECGBleDevice.h"//ECG系列設備

#import "BWG950BleDevice.h"//BW系列設備

#import "BP0802A0BleDevice.h"//BP系列設備
#import "BP822BleDevice.h"
#import "BPZOES2BleDevice.h"
#import "BPGSHBPBleDevice.h"
#import "BWBC418Device.h"
#import "BPGSHAVE2000BleDevice.h"
#import "BPGSH885BBleDevice.h"
#import "BTBleDevice.h"//BT系列設備
#import "BTBTS381BleDevice.h"
#import "GSH711BleDevice.h"
#import "BPGSH823B.h"
#import "BPGSH822B.h"
#import "GSH752BleDevice.h"
enum
{
    BLEDEVICE_BS_SERIES           = 0,
    BLEDEVICE_BP_SERIES           = 1,
    BLEDEVICE_BW_SERIES           = 2,
    BLEDEVICE_BT_SERIES           = 3,
    BLEDEVICE_ECG_SERIES          = 4,
    BLEDEVICE_ALL                 = 5,
};
/**
 根據SERIES 可以產生相對應系列的所有藍牙設備的管理器
 此管理器不支援G950設備
 */
@interface BLESeriesManager : BLEDeviceManager

/**
 初始化
 @param delegate // 實作BLEDeviceManagerDelegate的物件
 @param seriesType // 傳入BS_SERIES ... enum 列舉
 */
- (instancetype)initWithDelegate:(id)delegate seriesType:(int)seriesType;
/**
 初始化
 @param delegate // 實作BLEDeviceManagerDelegate的物件
 @param seriesType // 傳入BS_SERIES ... enum 列舉
 @param cusDevices //傳入額外的device 到這個陣列裡
 */
- (instancetype)initWithDelegate:(id)delegate seriesType:(int)seriesType cusDevices:(NSMutableArray*)cusDevices;
/**
 初始化 除錯用
 @param delegate // 實作BLEDeviceManagerDelegate的物件
 @param seriesType // 傳入BS_SERIES ... enum 列舉
 @param rssi //傳入要限制的rssi訊號 一定要給一個 例如傳入-20 就會把 小於-20的都律掉
 @param uuidString //需要傳入的uuidString 讓你可以只連指定的uuid
 uuid 每台iPhone 設備 連到不同的藍牙設備  會有不同的uuid 所以只能針對同台iPhone 與 藍牙設備抓到的UUID傳入 所以這邊只能先從NSLog 上去取得 uuid 在寫到這裡來debug
 */
- (instancetype)initDebugWithDelegate:(id)delegate seriesType:(int)seriesType RSSI:(int)rssi UUIDString:(NSString*)uuidString;
-(void) changeSeriesTypeAndRescan:(int)seriesType;
-(void) changeSeriesTypeAndRescanByScanList:(int)seriesType;
@end
