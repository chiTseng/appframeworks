//
//  DeviceDatas.m
//  DeviceManager
//
//  Created by elaine on 2017/9/25.
//  Copyright © 2017年 GSH. All rights reserved.
//

#import "DeviceDatas.h"

@implementation DeviceDatas
@end

@implementation BPDeviceData
@synthesize intPulse,intDia,intSys,strVersion,intAVI,intAPI,strDeviceDate,strOSDate,strError;
@end

@implementation BSDeviceData
@synthesize intCholesterol,intGlucoseData;
@end

@implementation BTDeviceData
@synthesize intLocation,intMeasureUnit,strMacAddress,fltTempDataResult;
@end

@implementation BWDeviceData
@synthesize fltWeight,fltResistance;
@end

@implementation BWBC41DeviceData
@synthesize Year, Month, Date, Hour, Minute, Body_type, Gender, Height, Body_fat_percentage, Fat_mass, Fat_free_mass, Body_water_mass, Age, BMI, BMR;
@end

@implementation ECGDeviceData
@synthesize intLength,recData,recUUID;
@end

@implementation GLM76DeviceData
@synthesize Year, Month, Date, Hour, Minute, Second, DATA, MealStatus;
@end
