//
//  BleCommand.h
//  DeviceManager
//
//  Created by elaine on 2018/12/5.
//  Copyright © 2018年 GSH. All rights reserved.
//

#ifndef BleCommand_h
#define BleCommand_h
#define SERVICE_INFO_DEVICE @"180A"
#define SYSTEMID @"2A23"
#define SERIAL_NUMBER @"2A25"
#define FIRMWARE_VERSION @"2A26"
#define HARDWARE_VERSION @"2A27"
#define SOFTWARE_VERSION @"2A28"
#define MANUFACTURER_NAME @"2A29"

#define SUPPORT_SYSTEMID 1
#define SUPPORT_SERIALNUMBER 2
#define SUPPORT_FIRMWAREVERSION 4
#define SUPPORT_HARDWAREVERSION 8
#define SUPPORT_SOFTWAREVERSION 16
#define SUPPORT_MANUFACTURENAME 32
#endif /* BleCommand_h */
