//
//  BaseBLEDevice.m
//  BodyTemperatureSample
//
//  Created by elaine on 2017/8/25.
//  Copyright © 2017年 GSH. All rights reserved.
//

#import "BaseBLEDevice.h"

@implementation BaseBLEDevice
@synthesize uuids,firmwareVersion,softwareVersion,hardwareVersion,manufacturerName;
@synthesize readSystemInfoStatus,readSystemInfoComplete;
@synthesize connectIOSName,connectIOSUUID;
-(NSString *) StringConvert:(NSString *)str
{
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (str.length < 6 || str.length > 16)
    {
        return str;
    }
    //7a 05 1a 42 48 cf e5 e0
    //0e5efc8424a150a7
    //e0e5cf48421a057a
    //e0e5cf    1a057a
    unsigned long len;
    len = [str length];
    unichar a[len - 4];//排除中間兩位
    int aIndex = 0;
    for (int i = 0; i < len; i+=2) {
        if (i == 6 || i == 8)
        {//排除中間兩位
            continue;
        }
        if ((len - (i + 1) - 1) < 0) {
            unichar c = [str characterAtIndex:len - i - 1];
            a[aIndex] = c;
            aIndex += 1;
        }
        else
        {
            unichar c = [str characterAtIndex:len - i - 1];
            unichar c2 = [str characterAtIndex:len - (i + 1) - 1];
            a[aIndex] = c2;
            a[aIndex + 1] = c;
            aIndex += 2;
        }
        
    }
    NSString *newStr = [NSString stringWithCharacters:a length:len - 4];
    return newStr;
    
}
-(NSString *) NSDataToHexString:(NSData *)data
{
    const unsigned char *dbytes = [data bytes];
    NSMutableString *hexStr =
    [NSMutableString stringWithCapacity:[data length]*2];
    int i;
    for (i = 0; i < [data length]; i++) {
        [hexStr appendFormat:@"%02x ", dbytes[i]];
    }
    return [NSString stringWithString: hexStr];
}
-(NSData*)stringToByte:(NSString*)string
{//除非有特殊需求，不需要特別覆寫掉
    NSString *hexString=[[string uppercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([hexString length]%2!=0) {
        return nil;
    }
    Byte tempbyt[1]={0};
    NSMutableData* bytes=[NSMutableData data];
    for(int i=0;i<[hexString length];i++)
    {
        unichar hex_char1 = [hexString characterAtIndex:i]; //兩位16進制數中第一位(高位*16)
        int int_ch1;
        if(hex_char1 >= '0' && hex_char1 <='9')
            int_ch1 = (hex_char1-48)*16;   // 0 的Ascll - 48
        else if(hex_char1 >= 'A' && hex_char1 <='F')
            int_ch1 = (hex_char1-55)*16; // A 的Ascll - 65
        else
            return nil;
        i++;
        
        unichar hex_char2 = [hexString characterAtIndex:i]; //兩位16進制數中第二位(低位)
        int int_ch2;
        if(hex_char2 >= '0' && hex_char2 <='9')
            int_ch2 = (hex_char2-48); // 0 的Ascll - 48
        else if(hex_char2 >= 'A' && hex_char2 <='F')
            int_ch2 = hex_char2-55; // A 的Ascll - 65
        else
            return nil;
        
        tempbyt[0] = int_ch1+int_ch2;  //將轉化後的數值放入Byte數組
        
        [bytes appendBytes:tempbyt length:1];
    }
    return bytes;
}
/**掃到設備，準備與設備連線前可以做的事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    DLog(@"請實作didDiscoverPeripheralHandler");
}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    DLog(@"請實作didConnectPeripheralHandler");
}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    DLog(@"請實作didDisconnectPeripheral");
    return true;
}
/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    DLog(@"請實作didDiscoverCharacteristicsForService");
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateValueForCharacteristic");
}
/**對藍牙設備寫入一個命令成功後會回傳到這裡，並讓個別設備決定要如何處理*/
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
}
-(void)didUpdateNotificationStateForCharacteristic:(CBPeripheral *)aPeripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateNotificationStateForCharacteristic");
}
@end
