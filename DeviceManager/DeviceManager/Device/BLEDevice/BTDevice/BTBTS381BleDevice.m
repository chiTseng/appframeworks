#define BT_SERVICE_BH @"1809"//體溫設備的服務uuid
#define BT_SERVICE_BH_2 @"FFF0"//不知道是誰的服務uuid

#define BT_GSHBTS381 @"GSH_BTS381"


#define BT_UUID_BH    @"2A1C"//收取資料的命令

#import "BTBTS381BleDevice.h"
#import "BleCommand.h"
@interface BTBTS381BleDevice()
{
    /**
     BT_UUID_BH 對應的characteristic
     */
    CBCharacteristic    *m_characteristic;
    /**
     判斷是否是第一次連線，因為連線後會一直送資料過來，需要過濾
     */
    BOOL firstConnect;
    /**
     一樣是為了不要馬上收資料而設定的count 大概就是-1 0 到大於0
     */
    int  iReceiveCount;
}
@end
/**只有在count 不一樣才收資料 其他則判斷為相同資料或已收過*/
static int  dataCount = 0;
static NSMutableDictionary *linkMacs;
@implementation BTBTS381BleDevice
@synthesize measureUnit;
//MARK: - override funcs -
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    
    measureUnit = 0;
    dataCount = 0;
}
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    firstConnect = true;
    iReceiveCount = 0;
}
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    firstConnect = false;
    return true;
}
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
        
    }
    for (CBCharacteristic *aChar in service.characteristics)
    {
        DLog(@"readValueForCharacteristic: %@", [aChar.UUID data]);
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:BT_UUID_BH]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        }
    }
}
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    m_characteristic = nil;
    
    DLog(@"didUpdateValueForCharacteristic");
    DLog(@"Service: %@, property:%@", [characteristic.service.UUID data],  characteristic.UUID);
    DLog(@"data = %@", characteristic.value);
    m_characteristic = characteristic;
    if([m_characteristic.service.UUID isEqual:[CBUUID UUIDWithString:BT_SERVICE_BH]]){
        if([m_characteristic.UUID isEqual:[CBUUID UUIDWithString:BT_UUID_BH]])
        {
            
            if(!m_characteristic.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:m_characteristic];
                [self performSelector:@selector(delayBleDidReceiveData) withObject:nil afterDelay:2.0];
            }
            
            if (iReceiveCount >= 0) {
                iReceiveCount++;
                return;
            }
            NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
            [self bleDidReceiveData:m_characteristic.value length:(int)(m_characteristic.value.length) withUUID:[CBUUID UUIDWithString:BT_UUID_BH] withModelName:[aPeripheral.name copy] DeviceID:tmpUUIDstr];
        }
    }
    else if ([m_characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            NSString *value = [self NSDataToHexString:characteristic.value];
            value = [self StringConvert:value];
            DLog(@"mac is %@",value);
            self.macAddress = value;
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
//MARK: - 子物件 自己的功能 -
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:BT_SERVICE_BH,BT_SERVICE_BH_2,SERVICE_INFO_DEVICE,nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = [NSString stringWithFormat:@"%@",BT_GSHBTS381];
    linkMacs = [NSMutableDictionary new];
    
    
}

-(void)delayBleDidReceiveData
{
    if (m_characteristic.value == nil || iReceiveCount == 1) {
        firstConnect = false;
        dataCount = 0;
    }
    iReceiveCount = -1;
}
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID{
    
    NSString *modelNameTmp = [modelName copy];
    if ([modelNameTmp isEqualToString:BT_GSHBTS381])
    {
        NSUInteger len = [data length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [data bytes], len);
        NSData *MantissaData = [NSData dataWithBytes:&byteData[1] length:2];
        NSData *Mantissa2Data = [NSData dataWithBytes:&byteData[3] length:1];
        NSData *ExponentData = [NSData dataWithBytes:&byteData[4] length:1];
        NSData *locationData = [NSData dataWithBytes:&byteData[12] length:1];
        NSData *countData = [NSData dataWithBytes:&byteData[11] length:1];
        
        int value1 = CFSwapInt16LittleToHost(*(int*)([MantissaData bytes]));
        int value2 = CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes]));
        int value3 = CFSwapInt16LittleToHost(*(int*)([ExponentData bytes]));
        int value4 = CFSwapInt16LittleToHost(*(int*)([locationData bytes]));
        int value5 = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
        DLog(@"length 13 value3 = %d",value3);
        value3 = 256- value3;
        
        DLog(@"256 - value3 = %d,10^value3 = %f,test = %f",value3,pow(10,value3),pow(10,5));
        DLog(@"Temperature :%.1f",(value1+value2*pow(2,16))/pow(10,value3));
        DLog(@"location:%d",value4);
        DLog(@"count:%d",value5);
        
        NSString *BTCountKey = [NSString stringWithFormat:@"BT%@",deviceID];
        NSString *num = [[NSUserDefaults standardUserDefaults] objectForKey:BTCountKey];
        if (num == nil) {
            dataCount = 0;
        }
        else
        {
            dataCount = num.intValue;
        }
        
        
        
        
        if (firstConnect == true) {
            
            firstConnect = false;
            
        }
        else
        {
            if (value5 != dataCount) {
                
                dataCount = value5;
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",dataCount] forKey:BTCountKey];
                [linkMacs setObject:[NSString stringWithFormat:@"%d",value5]forKey:deviceID];
                DLog(@"256 - value3 = %d,10^value3 = %f,test = %f",value3,pow(10,value3),pow(10,5));
                DLog(@"Temperature :%.1f",(value1+value2*pow(2,16))/pow(10,value3));
                DLog(@"location:%d",value4);
                DLog(@"count:%d",value5);
                
                BTDeviceData *data = [BTDeviceData new];
                data.intMeasureUnit = measureUnit;
                data.fltTempDataResult = (value1+value2*pow(2,16))/pow(10,value3);
                data.intLocation = value4;
                data.strMacAddress = self.macAddress;
                data.intCount = dataCount;
                if (self.delegate != nil) {
                    [self.delegate baseDevice:self resultData:data];
                }
                free(byteData);
            }
        }
    }
}
@end
