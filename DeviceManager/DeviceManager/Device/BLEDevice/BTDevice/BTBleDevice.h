//
//  TestGSHBHBleDevice.h
//  SoHappy
//
//  Created by elaine on 2017/8/25.
//
//

#import "BaseBLEDevice.h"
#import "DeviceDatas.h"
/**
 支援體溫計GSH BH
 */
@interface BTBleDevice : BaseBLEDevice
/**
 體溫量測的單位
 */
@property (assign) int measureUnit;
@end
