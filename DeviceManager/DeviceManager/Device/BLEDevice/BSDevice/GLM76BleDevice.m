//
//  GLM76BleDevice.m
//  DeviceManager
//
//  Created by Lynn on 2018/10/23.
//  Copyright © 2018年 GSH. All rights reserved.
//
#import "GLM76BleDevice.h"
#import "BleCommand.h"
#define DeviceName_GSH_GLM76            @"Yasee_GLM76"
#define GSH_GLM76_RSSI_Limit            -75
#define GSH_GLM76_SERVICE_UUID          @"1808"
#define GSH_GLM76_DATA_Notify_UUID      @"2A18"

@interface GLM76BleDevice()
{
    CBPeripheral        *m_peripheral;
    CBCharacteristic    *m_characteristic_Write;
    CBCharacteristic    *m_characteristic_Data_Notify;
    int                 m_RSSI_limit;
    
}
@end

@implementation GLM76BleDevice

int m_RSSI_limit = GSH_GLM76_RSSI_Limit;

/**掃到設備，準備與設備連線前可以做的事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    m_peripheral = nil;
    m_characteristic_Write = nil;
    DLog(@"請實作didDiscoverPeripheralHandler");

    DLog(@"didDiscoverPeƒripheral RSSI=%d Name=%@",[RSSI intValue], aPeripheral.name);
    
    /* Check device Name and RSSI */
    if([aPeripheral.name hasPrefix:DeviceName_GSH_GLM76] && [RSSI intValue] > m_RSSI_limit && [RSSI intValue] < 0){
        DLog(@"BLELog:Peripheral found.");
        
//        DLog(@"BLELog:Stop Scanning.");
//        [m_manager stopScan];
        
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBConnectPeripheralOptionNotifyOnDisconnectionKey, nil];
        m_peripheral = aPeripheral;
        DLog(@"BLELog:Start Trying connect to device.");
 
    }

    m_characteristic_Data_Notify = nil;
    m_characteristic_Write = nil;
}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    DLog(@"請實作didConnectPeripheralHandler");
    
    m_peripheral = aPeripheral;

}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    DLog(@"請實作didDisconnectPeripheral");
    m_peripheral = nil;
    m_characteristic_Data_Notify = nil;
    m_characteristic_Write = nil;
    
    return true;
}

/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    DLog(@"請實作didDiscoverCharacteristicsForService");
    
    if([service.UUID isEqual:[CBUUID UUIDWithString:GSH_GLM76_SERVICE_UUID]]){
        DLog(@"BLELog:Discover GSH_BC418 SERVICE Found");
        for (CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_GLM76_DATA_Notify_UUID]]){
                m_characteristic_Data_Notify = aChar;
                [m_peripheral setNotifyValue:YES forCharacteristic:m_characteristic_Data_Notify];
                DLog(@"BLELog:Enable notify %@", m_characteristic_Data_Notify.UUID);
            }
            
            DLog(@"BLELog:UUID found %@", aChar.UUID);
        }
    }
    else
    {
        for(CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }
            else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            }
        }
    }
    
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateValueForCharacteristic");

    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:GSH_GLM76_SERVICE_UUID]]){
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_GLM76_DATA_Notify_UUID]]){
            
            [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:GSH_GLM76_DATA_Notify_UUID] withModelName:nil DeviceID:nil];

        }
    }
    else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            //self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            //DLog(@"SERIAL_NUMBER is %@",self.macAddress);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            NSString *value = [self NSDataToHexString:characteristic.value];
            value = [self StringConvert:value];
            DLog(@"mac is %@",value);
            self.macAddress = value;
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
    
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:GSH_GLM76_SERVICE_UUID,SERVICE_INFO_DEVICE, nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = DeviceName_GSH_GLM76;
    
}
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    
    NSData *tmpData = [[NSData alloc] initWithData:data];
    DLog(@"BLELog:bleDidReceiveData %@", tmpData);
    
    if([((CBUUID *)uuid) isEqual:[CBUUID UUIDWithString:GSH_GLM76_DATA_Notify_UUID]]){
        
        NSUInteger len = [tmpData length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [tmpData bytes], len);
        
        GLM76DeviceData *GLM76Data = [GLM76DeviceData new];
        GLM76Data.Month = byteData[5];
        GLM76Data.Date = byteData[6];
        GLM76Data.Hour = byteData[7];
        GLM76Data.Minute = byteData[8];
        
        /* Check before meal or not */
        uint8_t tempdata = byteData[0];
        tempdata = tempdata & 0xC0;
        if(tempdata == 0x00){
            GLM76Data.MealStatus = @"testLiquid";
        }else if(tempdata == 0x80){
            GLM76Data.MealStatus = @"afterMeal";
        }else if(tempdata == 0x40){
            GLM76Data.MealStatus = @"beforeMeal";
        }
        
        uint8_t calculate_tempdata_0 = 0;
        int8_t  icalculate_tempdata_0 = 0;
        int16_t calculate_tempdata_1 = 0;
        calculate_tempdata_0 = byteData[12];
        DLog(@"BLELog:calculate_tempdata_0 %d", calculate_tempdata_0);
        calculate_tempdata_0 = calculate_tempdata_0 & 0xF0;
        DLog(@"BLELog:calculate_tempdata_0 %d", calculate_tempdata_0);
        calculate_tempdata_0 = calculate_tempdata_0 >> 4;
        DLog(@"BLELog:calculate_tempdata_0 %d", calculate_tempdata_0);
        icalculate_tempdata_0 = (0x10 - calculate_tempdata_0);
        icalculate_tempdata_0 = -icalculate_tempdata_0;
        DLog(@"BLELog:calculate_tempdata_0 %d", icalculate_tempdata_0);
        
        calculate_tempdata_1 = byteData[12];
        DLog(@"BLELog:calculate_tempdata_1 %d", calculate_tempdata_1);
        calculate_tempdata_1 = calculate_tempdata_1 & 0x0F;
        DLog(@"BLELog:calculate_tempdata_1 %d", calculate_tempdata_1);
        calculate_tempdata_1 = calculate_tempdata_1 << 8;
        DLog(@"BLELog:calculate_tempdata_1 %d", calculate_tempdata_1);
        calculate_tempdata_1 = calculate_tempdata_1 + byteData[13];
        DLog(@"BLELog:calculate_tempdata_1 %d", calculate_tempdata_1);
        GLM76Data.DATA  = calculate_tempdata_1;
        GLM76Data.DATA  = GLM76Data.DATA * (float)(pow(10,icalculate_tempdata_0));
        DLog(@"BLELog:m_GLM76_BLE_Data.DATA %f", GLM76Data.DATA);
        
        free(byteData);
        
        if (self.delegate != nil) {
            [self.delegate baseDevice:self resultData:GLM76Data];
        }
    }
    
}
@end
