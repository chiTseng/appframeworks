//
//  BSBleDevice.m
//  SoHappy
//
//  Created by elaine on 2017/9/21.
//
//
#define GSH_GL  @"GSH-GL" //設備名稱
#define BS_SERVICE_UUID  @"FFF0"//設備上的服務uuid
#define BS_NOTIFY_UUID  @"FFF4"//notify 收值命令

#import "BSBleDevice.h"
#import "BleCommand.h"
@implementation BSBleDevice
/**這裡沒做任何事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    DLog(@"請實作didDiscoverPeripheralHandler");
}
/**這裡沒做任何事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    DLog(@"請實作didConnectPeripheralHandler");
}
/**這裡沒做任何事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    DLog(@"請實作didDisconnectPeripheral");
    return true;
}
/**根據設備上有的服務做相對應的處理 read write or setNotify*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    DLog(@"請實作didDiscoverCharacteristicsForService");
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:BS_NOTIFY_UUID]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        }
    }
}
/**根據藍牙設備回傳的資料透過bleDidReceiveData 處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateValueForCharacteristic");
    if(([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:BS_SERVICE_UUID]]) &&([characteristic.UUID isEqual:[CBUUID UUIDWithString:BS_NOTIFY_UUID]])){//Samico GL
        if(!characteristic.isNotifying){
            [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
        }else{
            NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
            [self bleDidReceiveData:characteristic.value length:(int)characteristic.value.length withUUID:[CBUUID UUIDWithString:BS_NOTIFY_UUID] withModelName:[aPeripheral.name copy] DeviceID:tmpUUIDstr]; //@2015/09/17
        }
    }
    else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            NSString *value = [self NSDataToHexString:characteristic.value];
            value = [self StringConvert:value];
            DLog(@"mac is %@",value);
            self.macAddress = value;
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
/**這裡沒有做任何事情*/
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
}
/**初始化*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:BS_SERVICE_UUID,SERVICE_INFO_DEVICE,nil];
        [self initDeviceValue];
    }
    return self;
}
/**設定設備名稱*/
-(void)initDeviceValue
{
    self.deviceName = GSH_GL;
    
}
/**藍牙資料處理的地方*/
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    if(length == 7 || length == 5){ //@2015/12/18 支援新的血糖機 length == 7
        
        NSUInteger len = [data length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [data bytes], len);
        NSData *GlucoseData = [NSData dataWithBytes:&byteData[1] length:2];
        BSDeviceData *bsDeviceData = [BSDeviceData new];
        bsDeviceData.intGlucoseData = CFSwapInt16BigToHost(*(int*)([GlucoseData bytes]));
        bsDeviceData.intCholesterol = 0;
        free(byteData);
        if (self.delegate != nil) {
            [self.delegate baseDevice:self resultData:bsDeviceData];
        }
    }
}
@end
