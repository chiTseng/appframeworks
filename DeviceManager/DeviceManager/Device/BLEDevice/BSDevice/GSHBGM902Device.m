//
//  GSHBGM902Device.m
//  DeviceManager
//
//  Created by gsh_mac_2018 on 2019/12/31.
//  Copyright © 2019 GSH. All rights reserved.
//

#import "GSHBGM902Device.h"
#import "BleCommand.h"
#define DeviceName_GSH_BGM902            @"GSH_BGM902"
#define GSH_BGM902_RSSI_Limit            -75
#define GSH_BGM902_SERVICE_UUID          @"FFF0"
#define GSH_BGM902_CMD_WRITE_UUID        @"FFF3"
#define GSH_BGM902_DATA_Notify_UUID      @"FFF4"
#define GSH_BGM902_MAC_UUID              @"2A25"
#define GSH_BGM902_FWV_UUID              @"2A26"

#define GSH_BGM902_CMD_Password          @"CC"
#define GSH_BGM902_CMD_Got_Data          @"B00000"
#define GSH_BGM902_CMD_DFU               @"5A0000"
@interface GSHBGM902Device()
{
    CBPeripheral        *m_peripheral;
    CBCharacteristic    *m_characteristic_Write;
    CBCharacteristic    *m_characteristic_Data_Notify;
    
    NSString            *m_passwordString;
    
    NSTimer             *passwordTimer;
    uint8_t             m_resend_password_counter;
    
    bool password_reply;
    bool password_timer_running;
    bool mac_address_get;
    bool fw_version_get;
}
@end
@implementation GSHBGM902Device
/**掃到設備，準備與設備連線前可以做的事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (passwordTimer != nil) {
        [passwordTimer invalidate];
        passwordTimer = nil;
    }
    m_peripheral = nil;
    m_characteristic_Write = nil;
    m_characteristic_Data_Notify = nil;
    password_reply = false;
    password_timer_running = false;
    mac_address_get = false;
    fw_version_get = false;
    NSData *manufacturerData = NULL;
    manufacturerData = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
    
    if(manufacturerData == NULL)
        return;
    
    //NSData *manufacturerData = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
    //m_macAddress = [[manufacturerData description] stringByReplacingOccurrencesOfString:@" " withString:@""];
    //m_macAddress = [[m_macAddress substringWithRange:NSMakeRange(1, [m_macAddress length] - 2)] substringFromIndex:4];
    
    NSUInteger len = [manufacturerData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [manufacturerData bytes], len);
    
    int total = 0;
    for(int i=0;i<6;i++) {
        total += CFSwapInt16LittleToHost(*(int*)([[NSData dataWithBytes:&byteData[i+2] length:1] bytes]));
    }
    m_passwordString = [NSString stringWithFormat:@"CC%04x",total];
    m_peripheral = aPeripheral;
}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    DLog(@"請實作didConnectPeripheralHandler");
    
    
}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    DLog(@"請實作didDisconnectPeripheral");
    
    if (passwordTimer != nil) {
        [passwordTimer invalidate];
        passwordTimer = nil;
    }
    m_peripheral = nil;
    m_characteristic_Write = nil;
    m_characteristic_Data_Notify = nil;
    return true;
}

/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    DLog(@"請實作didDiscoverCharacteristicsForService");
    
    if([service.UUID isEqual:[CBUUID UUIDWithString:GSH_BGM902_SERVICE_UUID]]){
        NSLog(@"BLELog:Discover GSH_BC418 SERVICE Found");
        for (CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_BGM902_DATA_Notify_UUID]]){
                m_characteristic_Data_Notify = aChar;
                [aPeripheral setNotifyValue:YES forCharacteristic:m_characteristic_Data_Notify];
                NSLog(@"BLELog:Enable notify %@", m_characteristic_Data_Notify.UUID);
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_BGM902_CMD_WRITE_UUID]]){
                m_characteristic_Write = aChar;
                if(!password_reply){
                    passwordTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                                                   selector:@selector(passwordTimerHandler:) userInfo:nil repeats:YES];
                    password_timer_running = true;
                    NSData *password = [self stringToByte:m_passwordString];
                    [aPeripheral writeValue:password forCharacteristic:m_characteristic_Write type:CBCharacteristicWriteWithResponse];
                }
                NSLog(@"BLELog:UUID %@ discoverd", m_characteristic_Write.UUID);
            }
            
            NSLog(@"BLELog:UUID found %@", aChar.UUID);
        }
    }
    else
    {
        for(CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }
            else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            }
            else if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_BGM902_MAC_UUID]]){
                [aPeripheral readValueForCharacteristic:aChar];
                NSLog(@"BLELog:UUID %@ discoverd", aChar.UUID);
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_BGM902_FWV_UUID]]){
                [aPeripheral readValueForCharacteristic:aChar];
                NSLog(@"BLELog:UUID %@ discoverd", aChar.UUID);
            }
        }
    }
    
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateValueForCharacteristic");

    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:GSH_BGM902_SERVICE_UUID]]){
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_BGM902_DATA_Notify_UUID]]) {
            [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length)
                           withUUID:characteristic.UUID];
        }
    }
    else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            //self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            //DLog(@"SERIAL_NUMBER is %@",self.macAddress);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            NSString *value = [self NSDataToHexString:characteristic.value];
            value = [self StringConvert:value];
            DLog(@"mac is %@",value);
            self.macAddress = value;
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
    
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:GSH_BGM902_SERVICE_UUID, nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = DeviceName_GSH_BGM902;
    
}
-(void)bleDidReceiveData:(NSData *) data length:(int) length withUUID:(CBUUID *)uuid {
    
    NSData *tmpData = [[NSData alloc] initWithData:data];
    NSLog(@"BLELog:bleDidReceiveData %@", tmpData);

        
        NSUInteger len = [tmpData length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [tmpData bytes], len);
        
        // Byte 1 = H_byte
        // Byte 2 = L_byte
        if(len == 5){
            uint16_t tempdata = 0;
            tempdata = byteData[1];
            tempdata = tempdata << 8;
            tempdata = tempdata + byteData[2];
            //m_GSH_BGM902_BLE_Data.DATA = tempdata;
            NSData *appgetdatareply = [self stringToByte:GSH_BGM902_CMD_Got_Data];
            [m_peripheral writeValue:appgetdatareply forCharacteristic:m_characteristic_Write type:CBCharacteristicWriteWithResponse];
            BSDeviceData *bsDeviceData = [BSDeviceData new];
            bsDeviceData.intGlucoseData = tempdata;
            bsDeviceData.intCholesterol = 0;
            free(byteData);
            if (self.delegate != nil) {
                [self.delegate baseDevice:self resultData:bsDeviceData];
            }
        }else if(len == 3){
            NSData *cmd = [NSData dataWithBytes:&byteData[0] length:1];
            NSData *reply_H = [NSData dataWithBytes:&byteData[1] length:1];
            NSData *reply_L = [NSData dataWithBytes:&byteData[2] length:1];
            
            int intcmd = CFSwapInt16LittleToHost(*(int*)([cmd bytes]));
            int intreply_H = CFSwapInt16LittleToHost(*(int*)([reply_H bytes]));
            int intreply_L = CFSwapInt16LittleToHost(*(int*)([reply_L bytes]));
            
        if(intcmd == 0xCC && intreply_H == 0x4F && intreply_L == 0x4B){
            password_reply = true;
            NSLog(@"BLELog:Password reply get");
        }else if(intcmd == 0xB0 && intreply_H == 0x4F && intreply_L == 0x4B){
            NSLog(@"BLELog:App got data reply get");
        }else if(intcmd == 0x5A && intreply_H == 0x4F && intreply_L == 0x4B){
            NSLog(@"BLELog:DFU reply get");
        }
    }
}
-(void)passwordTimerHandler:(NSTimer *)sender{
    
    if(!password_reply){
        
        NSData *password = [self stringToByte:m_passwordString];
        [m_peripheral writeValue:password forCharacteristic:m_characteristic_Write type:CBCharacteristicWriteWithResponse];
        m_resend_password_counter++;
        if(m_resend_password_counter >= 10){
            NSString * state = nil;
            state = NSLocalizedString(@"BLE Password response timeout.",nil);
            NSLog(@"BLELog:BLE Password response timeout.");
        }
    }else{
        [passwordTimer invalidate];
    }
}
@end
