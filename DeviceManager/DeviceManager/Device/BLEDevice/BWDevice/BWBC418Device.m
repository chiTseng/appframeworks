//
//  BWBC418Device.m
//  DeviceManager
//
//  Created by Yu Chi on 2018/5/2.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "BWBC418Device.h"
#import "BleCommand.h"
#define DeviceName_GSH_BC418            @"GSH_BC418"

#define GSH_BC418_SERVICE_UUID          @"6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define GSH_BC418_NOTIFY_UUID           @"6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
#define GSH_BC418_WRITE_UUID            @"6E400002-B5A3-F393-E0A9-E50E24DCCA9E"

#define GSH_BC418_CMD_Read_Data         @"434D33"
#define GSH_BC418_CMD_Clear_Data        @"434D34"
@implementation GSH_BC418_BLE_Data
@synthesize Year, Month, Date, Hour, Minute, Body_type, Gender, Height, Weight, Body_fat_percentage, Fat_mass, Fat_free_mass, Body_water_mass, Age, BMI, BMR, Whole_body_impedance, Right_leg_impedance,Left_leg_impedance, Right_arm_impedance,Left_arm_impedance, Right_leg_body_fat_percentage,Right_leg_fat_mass, Right_leg_fat_free_mass, Right_leg_predicted_muscle_mass, Left_leg_body_fat_percentage, Left_leg_fat_mass, Left_leg_fat_free_mass, Left_leg_predicted_muscle_mass, Right_arm_body_fat_percentage,Right_arm_fat_mass, Right_arm_fat_free_mass, Right_arm_predicted_muscle_mass, Left_arm_body_fat_percentage, Left_arm_fat_mass, Left_arm_fat_free_mass, Left_arm_predicted_muscle_mass, Trunk_body_fat_percentage, Trunk_fat_mass, Trunk_fat_free_mass, Trunk_predicted_muscle_mass;
@end
@interface BWBC418Device()
{
    CBCharacteristic *m_characteristic_Write;
    CBPeripheral *m_peripheral;
    GSH_BC418_BLE_Data *m_BC418_BLE_Data;
    BOOL blnCmdReadSend;
    int intPacket_counter;
    NSTimer *tmrReset;
    
}
@end
@implementation BWBC418Device
@synthesize bwgDeviceData;
/**掃到設備，準備與設備連線前可以做的事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    DLog(@"請實作didDiscoverPeripheralHandler");
    blnCmdReadSend = false;
    m_characteristic_Write = nil;
    m_BC418_BLE_Data = nil;
    m_BC418_BLE_Data = [GSH_BC418_BLE_Data new];

    m_peripheral = nil;
    if (tmrReset != nil) {
        [tmrReset invalidate];
        tmrReset = nil;
    }
    
}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    m_peripheral = aPeripheral;
    DLog(@"請實作didConnectPeripheralHandler");
}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    DLog(@"請實作didDisconnectPeripheral");
    blnCmdReadSend = false;
    m_characteristic_Write = nil;
    m_BC418_BLE_Data = nil;
    m_peripheral = nil;
    if (tmrReset != nil) {
        [tmrReset invalidate];
        tmrReset = nil;
    }
    return true;
}
/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    DLog(@"BLELog:Discover GSH_BC418 SERVICE Found");
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_BC418_NOTIFY_UUID]]){
            if(!aChar.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:aChar];
                DLog(@"BLELog:Enable notify %@", aChar.UUID);
            }
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_BC418_WRITE_UUID]]){
            m_characteristic_Write = aChar;
            DLog(@"BLELog:Write UUID found %@", aChar.UUID);
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        }
    }
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_BC418_NOTIFY_UUID]]){
        if(!characteristic.isNotifying){
            [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
        }else{
            NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
            [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:GSH_BC418_NOTIFY_UUID] withModelName:tmpUUIDstr DeviceID:[aPeripheral.name copy]];
        }
    }
    else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
/**對藍牙設備寫入一個命令成功後會回傳到這裡，並讓個別設備決定要如何處理*/
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
}
-(void) didUpdateNotificationStateForCharacteristic:(CBPeripheral *)aPeripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_BC418_NOTIFY_UUID]]){
        if(!blnCmdReadSend){
            NSData *CMD;
            CMD = [self stringToByte:GSH_BC418_CMD_Read_Data];//@"434D33"
            DLog(@"BLELog:CMD = %@",CMD);
            [aPeripheral writeValue:CMD
                   forCharacteristic:m_characteristic_Write
                                type:CBCharacteristicWriteWithResponse];
            blnCmdReadSend = true;
        }
    }
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:GSH_BC418_SERVICE_UUID,SERVICE_INFO_DEVICE,nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = [NSString stringWithFormat:@"%@",DeviceName_GSH_BC418];
    
}
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    NSData *CMD;
    
    //_Label_Packet_count.text = [NSString stringWithFormat:@"Packet Count:%i", m_packetcount];
    
    NSData *tmpData = [[NSData alloc] initWithData:data];
    DLog(@"BLELog:bleDidReceiveData %@", tmpData);
    
    if([((CBUUID *)uuid) isEqual:[CBUUID UUIDWithString:GSH_BC418_NOTIFY_UUID]]){
        
        NSUInteger len = [tmpData length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [tmpData bytes], len);
        
        //_Label_Packet_ID.text = [NSString stringWithFormat:@"Packet ID:%i(%02X)", intID, intID];
        if(byteData[0] == 0xBB){
            switch(byteData[1]){
                case 0x01:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Date = (byteData[3]-0x30)*10 + (byteData[4]-0x30);
                    m_BC418_BLE_Data.Month = (byteData[6]-0x30)*10 + (byteData[7]-0x30);
                    m_BC418_BLE_Data.Year = (byteData[9]-0x30)*10 + (byteData[10]-0x30) + 2000;
                    m_BC418_BLE_Data.Hour = (byteData[14]-0x30)*10 + (byteData[15]-0x30);
                    m_BC418_BLE_Data.Minute = (byteData[17]-0x30)*10 + (byteData[18]-0x30);
                    break;
                    
                case 0x02:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Body_type = (byteData[3]-0x30);
                    m_BC418_BLE_Data.Gender = (byteData[5]-0x30);
                    m_BC418_BLE_Data.Height = ((byteData[7]-0x30)*10000)+((byteData[8]-0x30)*1000)+((byteData[9]-0x30)*100)+((byteData[10]-0x30)*10)+(byteData[11]-0x30);
                    m_BC418_BLE_Data.Weight = ((byteData[13]-0x30)*100)+((byteData[14]-0x30)*10)+(byteData[15]-0x30)+((byteData[17]-0x30)*0.1);
                    m_BC418_BLE_Data.Body_fat_percentage = ((byteData[19]-0x30)*10);
                    break;
                    
                case 0x03:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Body_fat_percentage = m_BC418_BLE_Data.Body_fat_percentage + (byteData[2]-0x30) + ((byteData[4]-0x30)*0.1);
                    m_BC418_BLE_Data.Fat_mass = ((byteData[6]-0x30)*100)+((byteData[7]-0x30)*10)+(byteData[8]-0x30)+((byteData[10]-0x30)*0.1);
                    m_BC418_BLE_Data.Fat_free_mass = ((byteData[12]-0x30)*100)+((byteData[13]-0x30)*10)+(byteData[14]-0x30)+((byteData[16]-0x30)*0.1);
                    m_BC418_BLE_Data.Body_water_mass = ((byteData[18]-0x30)*100)+((byteData[19]-0x30)*10);
                    break;
                    
                case 0x04:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Body_water_mass = m_BC418_BLE_Data.Body_water_mass + (byteData[2]-0x30)+((byteData[4]-0x30)*0.1);
                    m_BC418_BLE_Data.Age = ((byteData[6]-0x30)*10)+(byteData[7]-0x30);
                    m_BC418_BLE_Data.BMI = ((byteData[9]-0x30)*100)+((byteData[10]-0x30)*10)+(byteData[11]-0x30)+((byteData[13]-0x30)*0.1);
                    m_BC418_BLE_Data.BMR = ((byteData[15]-0x30)*10000)+((byteData[16]-0x30)*1000)+((byteData[17]-0x30)*100)+((byteData[18]-0x30)*10)+(byteData[19]-0x30);
                    break;
                    
                case 0x05:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Whole_body_impedance = ((byteData[3]-0x30)*100)+((byteData[4]-0x30)*10)+(byteData[5]-0x30);
                    m_BC418_BLE_Data.Right_leg_impedance = ((byteData[7]-0x30)*100)+((byteData[8]-0x30)*10)+(byteData[9]-0x30);
                    m_BC418_BLE_Data.Left_leg_impedance = ((byteData[11]-0x30)*100)+((byteData[12]-0x30)*10)+(byteData[13]-0x30);
                    m_BC418_BLE_Data.Right_arm_impedance = ((byteData[15]-0x30)*100)+((byteData[16]-0x30)*10)+(byteData[17]-0x30);
                    m_BC418_BLE_Data.Left_arm_impedance = ((byteData[19]-0x30)*100);
                    break;
                    
                case 0x06:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Left_arm_impedance = m_BC418_BLE_Data.Left_arm_impedance + ((byteData[2]-0x30)*10)+(byteData[3]-0x30);
                    m_BC418_BLE_Data.Right_leg_body_fat_percentage = ((byteData[5]-0x30)*10)+(byteData[6]-0x30)+((byteData[8]-0x30)*0.1);
                    m_BC418_BLE_Data.Right_leg_fat_mass = ((byteData[10]-0x30)*100)+((byteData[11]-0x30)*10)+(byteData[12]-0x30)+((byteData[14]-0x30)*0.1);
                    m_BC418_BLE_Data.Right_leg_fat_free_mass = ((byteData[16]-0x30)*100)+((byteData[17]-0x30)*10)+(byteData[18]-0x30);
                    break;
                    
                case 0x07:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Right_leg_fat_free_mass = m_BC418_BLE_Data.Right_leg_fat_free_mass + ((byteData[2]-0x30)*0.1);
                    m_BC418_BLE_Data.Right_leg_predicted_muscle_mass = ((byteData[4]-0x30)*100)+((byteData[5]-0x30)*10)+(byteData[6]-0x30)+((byteData[8]-0x30)*0.1);
                    m_BC418_BLE_Data.Left_leg_body_fat_percentage = ((byteData[10]-0x30)*10)+(byteData[11]-0x30)+((byteData[13]-0x30)*0.1);
                    m_BC418_BLE_Data.Left_leg_fat_mass = ((byteData[15]-0x30)*100)+((byteData[16]-0x30)*10)+(byteData[17]-0x30)+((byteData[19]-0x30)*0.1);
                    break;
                    
                case 0x08:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Left_leg_fat_free_mass = ((byteData[3]-0x30)*100)+((byteData[4]-0x30)*10)+(byteData[5]-0x30)+((byteData[7]-0x30)*0.1);
                    m_BC418_BLE_Data.Left_leg_predicted_muscle_mass = ((byteData[9]-0x30)*100)+((byteData[10]-0x30)*10)+(byteData[11]-0x30)+((byteData[13]-0x30)*0.1);
                    m_BC418_BLE_Data.Right_arm_body_fat_percentage = ((byteData[15]-0x30)*10)+(byteData[16]-0x30)+((byteData[18]-0x30)*0.1);
                    break;
                    
                case 0x09:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Right_arm_fat_mass = ((byteData[2]-0x30)*100)+((byteData[3]-0x30)*10)+(byteData[4]-0x30)+((byteData[6]-0x30)*0.1);
                    m_BC418_BLE_Data.Right_arm_fat_free_mass = ((byteData[8]-0x30)*100)+((byteData[9]-0x30)*10)+(byteData[10]-0x30)+((byteData[12]-0x30)*0.1);
                    m_BC418_BLE_Data.Right_arm_predicted_muscle_mass = ((byteData[14]-0x30)*100)+((byteData[15]-0x30)*10)+(byteData[16]-0x30)+((byteData[18]-0x30)*0.1);
                    break;
                    
                case 0x0A:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Left_arm_body_fat_percentage = ((byteData[2]-0x30)*10)+(byteData[3]-0x30)+((byteData[5]-0x30)*0.1);
                    m_BC418_BLE_Data.Left_arm_fat_mass = ((byteData[7]-0x30)*100)+((byteData[8]-0x30)*10)+(byteData[9]-0x30)+((byteData[11]-0x30)*0.1);
                    m_BC418_BLE_Data.Left_arm_fat_free_mass = ((byteData[13]-0x30)*100)+((byteData[14]-0x30)*10)+(byteData[15]-0x30)+((byteData[17]-0x30)*0.1);
                    m_BC418_BLE_Data.Left_arm_predicted_muscle_mass = ((byteData[19]-0x30)*100);
                    break;
                    
                case 0x0B:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Left_arm_predicted_muscle_mass = m_BC418_BLE_Data.Left_arm_predicted_muscle_mass + ((byteData[2]-0x30)*10)+(byteData[3]-0x30)+((byteData[5]-0x30)*0.1);
                    m_BC418_BLE_Data.Trunk_body_fat_percentage = ((byteData[7]-0x30)*10)+(byteData[8]-0x30)+((byteData[10]-0x30)*0.1);
                    m_BC418_BLE_Data.Trunk_fat_mass = ((byteData[12]-0x30)*100)+((byteData[13]-0x30)*10)+(byteData[14]-0x30)+((byteData[16]-0x30)*0.1);
                    m_BC418_BLE_Data.Trunk_fat_free_mass = ((byteData[18]-0x30)*100)+((byteData[19]-0x30)*10);
                    break;
                    
                case 0x0C:
                    intPacket_counter = intPacket_counter+byteData[1];
                    m_BC418_BLE_Data.Trunk_fat_free_mass = m_BC418_BLE_Data.Trunk_fat_free_mass + (byteData[2]-0x30)+((byteData[4]-0x30)*0.1);
                    m_BC418_BLE_Data.Trunk_predicted_muscle_mass = ((byteData[6]-0x30)*100)+((byteData[7]-0x30)*10)+(byteData[8]-0x30)+((byteData[10]-0x30)*0.1);
                    
                    /* 確認每個封包都有收到 = 0x78, 沒收到就要求重送一次 */
                    if(intPacket_counter == 78){
                        //[self show_all_result];
                        intPacket_counter = 0;
                        bwgDeviceData = [[BWBC41DeviceData alloc] init];
                        bwgDeviceData.Year = m_BC418_BLE_Data.Year;
                        bwgDeviceData.Date = m_BC418_BLE_Data.Date;
                        bwgDeviceData.Month = m_BC418_BLE_Data.Month;
                        bwgDeviceData.Hour = m_BC418_BLE_Data.Hour;
                        bwgDeviceData.Minute = m_BC418_BLE_Data.Minute;
                        bwgDeviceData.Body_type = m_BC418_BLE_Data.Body_type;
                        bwgDeviceData.Gender = m_BC418_BLE_Data.Gender;
                        bwgDeviceData.Height = m_BC418_BLE_Data.Height;
                        bwgDeviceData.fltWeight = m_BC418_BLE_Data.Weight;
                        
                        bwgDeviceData.Body_fat_percentage = m_BC418_BLE_Data.Body_fat_percentage;//體脂率
                        bwgDeviceData.Fat_mass = m_BC418_BLE_Data.Fat_mass;//脂肪量
                        bwgDeviceData.Fat_free_mass = m_BC418_BLE_Data.Fat_free_mass;//肌肉量
                        bwgDeviceData.Body_water_mass = m_BC418_BLE_Data.Body_water_mass;//水分率 去除體重 佔的比例
                        bwgDeviceData.Age = m_BC418_BLE_Data.Age;
                        bwgDeviceData.BMI = m_BC418_BLE_Data.BMI;
                        bwgDeviceData.BMR = m_BC418_BLE_Data.BMR;
                        bwgDeviceData.fltResistance = m_BC418_BLE_Data.Whole_body_impedance;
                        
                        if (self.delegate != nil) {
                            [self.delegate baseDevice:self resultData:bwgDeviceData];
                        }
                        CMD = [self stringToByte:GSH_BC418_CMD_Clear_Data];//@"434D34"
                        DLog(@"BLELog:CMD = %@",CMD);
                        [m_peripheral writeValue:CMD
                               forCharacteristic:m_characteristic_Write
                                            type:CBCharacteristicWriteWithResponse];
                        
                    }else{
                        intPacket_counter = 0;
                        /* 一秒後要求重送 */
                        tmrReset = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                                                     selector:@selector(resendTimerHandler:) userInfo:nil repeats:NO];
                    }
                    
                    break;
                default:
                    break;
            }
        }else if(byteData[0] == 0x43 && byteData[1] == 0x4E && byteData[2] == 0x34){
            
            
            
        }
        free(byteData);
    }
}
-(void)resendTimerHandler:(NSTimer *)sender{
    NSData *CMD;
    CMD = [self stringToByte:GSH_BC418_CMD_Read_Data];//@"434D33"
    DLog(@"BLELog:CMD = %@",CMD);
    [m_peripheral writeValue:CMD
           forCharacteristic:m_characteristic_Write
                        type:CBCharacteristicWriteWithResponse];
}
@end
