//
//  BWG950BleDevice.h
//  SoHappy
//
//  Created by Yu Chi on 2017/9/2.
//
//
//透過SERIAL_NUMBER 取得mac
#import "BWBleDevice.h"

@interface BWG950DeviceData : BWDeviceData
/**BMR*/
@property (assign) float fltBasalMetabolism;
/**體脂*/
@property (assign) float fltBodyFat;
/**水分*/
@property (assign) float fltBodyWater;
/**內臟脂肪*/
@property (assign) float fltVisceralFatLevel;
/**肌肉*/
@property (assign) float fltMuscle;
/**骨骼*/
@property (assign) float fltBone;
@end
/**支援G950 設備功能特殊，需設定身高 年齡 性別 故只能自己新增此設備 並透過BLEDeviceManager 來使用*/
@interface BWG950BleDevice : BWBleDevice

/**身高需設定*/
@property (assign) int intHeight;
/**s年紀需設定*/
@property (assign) int intAge;
/**性別需設定*/
@property (assign) int intGender;
/**g950專用的data*/
@property (nonatomic,retain) BWG950DeviceData *bwgDeviceData;

@end
