//
//  BWBleDevice.m
//  SoHappy
//
//  Created by Yu Chi on 2017/9/2.
//
//
#define BW0202B_00000000  @"0202B 00000000" //設備名稱
#define BW0102B_00000000 @"0102B 00000000" //設備名稱 沒有電阻值
#define BW0202B_0001  @"0202B-0001" //設備名稱
#define GSH103_0001 @"GSH103-0001"//星空
#define GSH103_0002 @"GSH103-0002"//蝴蝶藍

#define BW_SERVICE_SCALES @"78A2"//服務的uuid

#define BW_UUID_SCALES_1 @"8A21"//量測完資料透過這裡取得
#define BW_UUID_SCALES_COMMAND @"8A81"//寫入命令用的

#import "BWBleDevice.h"
#import "BleCommand.h"
@interface BWBleDevice()
@end
@implementation BWBleDevice
/**掃到設備，準備與設備連線前可以做的事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{

}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{

}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    return true;
}
/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_SCALES_1]]){//To get Notify value about Scales
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_SCALES_COMMAND]]){//To get Notify value about Scales
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        }
    }
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:BW_SERVICE_SCALES]]){
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_SCALES_1]]){
            if(!characteristic.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
            }else{
                NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
                [self bleDidReceiveData:characteristic.value length:(int)characteristic.value.length withUUID:[CBUUID UUIDWithString:BW_UUID_SCALES_1] withModelName:[aPeripheral.name copy] DeviceID:tmpUUIDstr]; //@2015/09/17
            }
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_SCALES_COMMAND]]){

            if(!characteristic.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
                NSData *d1d=[self stringToByte:@"02"];
                [aPeripheral writeValue:d1d forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
            }else{
            }
        }
    }
    else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:BW_SERVICE_SCALES,SERVICE_INFO_DEVICE,nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = [NSString stringWithFormat:@"%@,%@,%@,%@,%@",BW0102B_00000000,BW0202B_00000000,BW0202B_0001,GSH103_0001,GSH103_0002];
    
}
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    BWDeviceData *bwDeviceData = [BWDeviceData new];
    NSUInteger len = [data length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [data bytes], len);
    NSData *MantissaData = [NSData dataWithBytes:&byteData[1] length:2];
    NSData *Mantissa2Data = [NSData dataWithBytes:&byteData[3] length:1];
    NSData *ExponentData = [NSData dataWithBytes:&byteData[4] length:1];
    int value1 = CFSwapInt16LittleToHost(*(int*)([MantissaData bytes]));
    int value2 = CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes]));
    int value3 = CFSwapInt16LittleToHost(*(int*)([ExponentData bytes]));
    DLog(@"value3 = %d",value3);
    value3 = 256- value3;
    DLog(@"256 - value3 = %d,10^value3 = %f,test = %f",value3,pow(10,value3),pow(10,5));
    DLog(@"Weight :%.2f",(value1+value2*pow(2,16))/pow(10,value3));
    bwDeviceData.fltWeight = (value1+value2*pow(2,16))/pow(10,value3);
    //電阻
    NSData *MantissaData2 = [NSData dataWithBytes:&byteData[13] length:2];
    NSData *Mantissa2Data2 = [NSData dataWithBytes:&byteData[15] length:1];
    NSData *ExponentData2 = [NSData dataWithBytes:&byteData[16] length:1];
    int value4 = CFSwapInt16LittleToHost(*(int*)([MantissaData2 bytes]));
    int value5 = CFSwapInt16LittleToHost(*(int*)([Mantissa2Data2 bytes]));
    int value6 = CFSwapInt16LittleToHost(*(int*)([ExponentData2 bytes]));
    DLog(@"value6 = %d",value6);
    value6 = 256- value6;
    DLog(@"256 - value6 = %d,10^value6 = %f,test = %f",value6,pow(10,value6),pow(10,5));
    DLog(@"Resistance :%.2f",(value4+value5*pow(2,16))/pow(10,value6));
    bwDeviceData.fltResistance = (value4+value5*pow(2,16))/pow(10,value6);
    
    //@2016/10/19
    if([modelName containsString:BW0102B_00000000] || [modelName containsString:GSH103_0001] || [modelName containsString:GSH103_0002]){
        bwDeviceData.fltResistance = -1;
    }
    
    if (self.delegate != nil) {
        [self.delegate baseDevice:self resultData:bwDeviceData];
    }
}
@end
