//
//  BWG950BleDevice.m
//  SoHappy
//
//  Created by Yu Chi on 2017/9/2.
//
//
#define G950   @"G-950"//設備名稱
#define BW_SERVICE_G950 @"78F2"//服務的UUID

#define BW_UUID_G950_APPEND_MEASURE @"8A22"//取得量測 電阻分析後數值的notify
#define BW_UUID_G950_MEASURE @"8A24"//取得體重 電阻的notify
#define BW_UUID_G950_WRITE @"8A81"//設定22命令來Enable或者updateUserInfo或者setUTC 透過這個寫入
#define BW_UUID_G950_READ @"8A82"//連線成功後第一步都會先read這裡得到回傳後再去setUTC 然後取得userID
#define BW_UUID_G950_FEATURE @"8A20"//目前沒有作用


#import "BWG950BleDevice.h"
#import "BleCommand.h"
@implementation BWG950DeviceData
@synthesize fltBone,fltMuscle,fltBodyFat,fltBodyWater,fltBasalMetabolism,fltVisceralFatLevel;
@end
@interface BWG950BleDevice()
{
    CBPeripheral *m_peripheral;
    NSData *strPreWriteCmd;
    NSTimer *nstWaitTimer;
    CBCharacteristic *m_characteristic_write;
    BOOL blnWegihtOnlyFlag;
    int intUserId;
}
@end
@implementation BWG950BleDevice
@synthesize intAge,intGender,intHeight,bwgDeviceData;

/**掃到設備，準備與設備連線前可以做的事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    DLog(@"請實作didDiscoverPeripheralHandler");
    
}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    DLog(@"請實作didConnectPeripheralHandler");
    m_peripheral = aPeripheral;
    blnWegihtOnlyFlag = false;
    intUserId = 0;
    bwgDeviceData = [BWG950DeviceData new];

}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    if (error.code == 10) {
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBConnectPeripheralOptionNotifyOnDisconnectionKey, nil];
        [central connectPeripheral:aPeripheral options:options];
        return false;
    }
    DLog(@"請實作didDisconnectPeripheral");
    return true;
}
/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    DLog(@"請實作didDiscoverCharacteristicsForService");
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_READ]]){
        [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        }
    }
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateValueForCharacteristic");
    [self stopNstWaitTimer];
    strPreWriteCmd = nil;
    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:BW_SERVICE_G950]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_READ]]) {
            if(!characteristic.isNotifying){
                DLog(@"G950 8A82 set setNotifyValue");
                [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
                //G950一定要依照流程圖set nofity和write command
                [self performSelector:@selector(setG950Notify8A24) withObject:nil afterDelay:0.2];
            } else {
                DLog(@"8A82 value=%@ length=%d",characteristic.value, (int)(characteristic.value.length));
                if(characteristic.value.length > 0) {
                    blnWegihtOnlyFlag = NO;
                    NSUInteger len = [characteristic.value length];
                    Byte *byteData = (Byte*)malloc(len);
                    memcpy(byteData, [characteristic.value bytes], len);
                    NSData *cmdData = [NSData dataWithBytes:&byteData[0] length:1];
                    int cmdValue = CFSwapInt16LittleToHost(*(int*)([cmdData bytes]));
                    DLog(@"cmdValue=%@",[NSString stringWithFormat:@"%02X", cmdValue]);
                    if(cmdValue == 192) { //user info 0xc0
                        NSData *idData = [NSData dataWithBytes:&byteData[2] length:1];
                        int idValue = CFSwapInt16LittleToHost(*(int*)([idData bytes]));
                        int index = 3;
                        int genderValue = 0, ageValue = 0, heightValue = 0;
                        int userGender;
                        
                        NSData *flagData = [NSData dataWithBytes:&byteData[1] length:1];
                        int flagValue = CFSwapInt16LittleToHost(*(int*)([flagData bytes]));
                        if(flagValue&0x01) {
                            NSData *genderData = [NSData dataWithBytes:&byteData[index++] length:1];
                            genderValue = CFSwapInt16LittleToHost(*(int*)([genderData bytes]));
                        }
                        if(flagValue&0x02) {
                            NSData *ageData = [NSData dataWithBytes:&byteData[index++] length:1];
                            ageValue = CFSwapInt16LittleToHost(*(int*)([ageData bytes]));
                        }
                        if(flagValue&0x04) {
                            NSData *heightData = [NSData dataWithBytes:&byteData[index++] length:2];
                            heightValue = CFSwapInt32LittleToHost(*(int*)([heightData bytes]));
                            heightValue = heightValue&0x0FFF;
                        }
                        DLog(@"genderValue=%d ageValue=%d heightValue=%d", genderValue, ageValue, heightValue);
                        /*UserData *userData = [UserData getUserDataWithAccount:[AppUtility getAppDelegate].account];
                        DLog(@"userData.Age=%d userData.Height=%f userData.BeMale=%d", userData.Age, userData.Height, userData.BeMale);*/
                        if(self.intGender == 1) {
                            userGender = 1;
                        } else {
                            userGender = 2;
                        }
                        
                        if((ageValue == 0) && (heightValue == 0) && (genderValue == 0)) {
                            blnWegihtOnlyFlag = YES;
                            [self setG950EnableDisconnection];
                        } else {
                            if((intAge != ageValue) || (intHeight != (heightValue/10.0)) || (userGender != genderValue)) {
                                DLog(@"userinfo different");
                                [self setG950UpdateUserInfo:idValue gender:userGender age:intAge height:intHeight];
                            } else {
                                DLog(@"userinfo the same");
                                [self setG950EnableDisconnection];
                            }
                        }
                    }
                } else {
                    [self setG950EnableDisconnection];
                }
            }
        } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_MEASURE]]) {
            if(!characteristic.isNotifying){
            } else {
                DLog(@"8A24 value=%@ length=%d",characteristic.value, (int)(characteristic.value.length));
                if(characteristic.value.length > 0) {
                    NSUInteger len = [characteristic.value length];
                    Byte *byteData = (Byte*)malloc(len);
                    memcpy(byteData, [characteristic.value bytes], len);
                    
                    NSData *flagData = [NSData dataWithBytes:&byteData[0] length:1];
                    int flag = CFSwapInt16LittleToHost(*(int*)([flagData bytes]));
                    NSString *deviceSelectedUnit = @"0";
                    if((flag & 0x60) == 0x00) {
                        DLog(@"Unit is Kg");
                        deviceSelectedUnit = @"0";
                    } else if((flag & 0x60) == 0x20) {
                        DLog(@"Unit is LB");
                        deviceSelectedUnit = @"1";
                    } else if((flag & 0x60) == 0x40) {
                        DLog(@"Unit is St.");
                        deviceSelectedUnit = @"2";
                    }
                    
                    //體重
                    NSData *MantissaData = [NSData dataWithBytes:&byteData[1] length:2];
                    NSData *Mantissa2Data = [NSData dataWithBytes:&byteData[3] length:1];
                    NSData *ExponentData = [NSData dataWithBytes:&byteData[4] length:1];
                    int value1 = CFSwapInt16LittleToHost(*(int*)([MantissaData bytes]));
                    int value2 = CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes]));
                    int value3 = CFSwapInt16LittleToHost(*(int*)([ExponentData bytes]));
                    value3 = 256 - value3;
                    bwgDeviceData.fltWeight = (value1+value2*pow(2,16))/pow(10,value3);
                    DLog(@"Weight :%f",bwgDeviceData.fltWeight);
                    
                    //電阻
                    NSData *MantissaData2 = [NSData dataWithBytes:&byteData[13] length:2];
                    NSData *Mantissa2Data2 = [NSData dataWithBytes:&byteData[15] length:1];
                    NSData *ExponentData2 = [NSData dataWithBytes:&byteData[16] length:1];
                    int value4 = CFSwapInt16LittleToHost(*(int*)([MantissaData2 bytes]));
                    int value5 = CFSwapInt16LittleToHost(*(int*)([Mantissa2Data2 bytes]));
                    int value6 = CFSwapInt16LittleToHost(*(int*)([ExponentData2 bytes]));
                    value6 = 256 - value6;
                    bwgDeviceData.fltResistance = (value4+value5*pow(2,16))/pow(10,value6);
                    DLog(@"Resistance :%f",bwgDeviceData.fltResistance);
                    
                    //NSString *nowDate = @"2017-01-01 00:00:00";//[[AppUtility stringFromInternetDateTime:[NSDate date] withFormat:@"yyyy-MM-dd HH:mm:ss"] stringByReplacingOccurrencesOfString:@" " withString:@"T"];
                    
                    /*[AppUtility getAppDelegate].weightData = [[WeightData alloc] init];
                    [AppUtility getAppDelegate].weightData.Date = nowDate;
                    [AppUtility getAppDelegate].weightData.Other4 = resistance;
                    [AppUtility getAppDelegate].weightData.Unit = deviceSelectedUnit;
                    [AppUtility getAppDelegate].weightData.Weight = weight;*/
                    
                    if(bwgDeviceData.fltResistance == 0 || blnWegihtOnlyFlag) {
                        //只有體重值,直接儲存
                        //[[AppUtility getAppDelegate] saveWeightDataWithWeight:weight Fat:0 BMI:0 Bone:0 Muscle:0 Water:0 Calories:0 VisceralFatLevel:0 Resistance:resistance withDeviceName:@"" date:nowDate];
                        if (self.delegate != nil) {
                            [self.delegate baseDevice:self resultData:bwgDeviceData];
                        }
                    }
                }
            }
        } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_APPEND_MEASURE]])
        {
            if(!characteristic.isNotifying)
            {
            }
            else
            {
                DLog(@"8A22 value=%@ length=%d",characteristic.value, (int)(characteristic.value.length));
                if(characteristic.value.length > 0)
                {
                    NSUInteger len = [characteristic.value length];
                    Byte *byteData = (Byte*)malloc(len);
                    memcpy(byteData, [characteristic.value bytes], len);
                    
                    NSData *flagData = [NSData dataWithBytes:&byteData[0] length:1];
                    int flag = CFSwapInt16LittleToHost(*(int*)([flagData bytes]));
                    int index = 5;
                    if(flag & 0x01) {
                        NSData *idData = [NSData dataWithBytes:&byteData[index++] length:1];
                        intUserId = CFSwapInt16LittleToHost(*(int*)([idData bytes]));
                    }
                    if(flag & 0x02) {
                        NSData *basalMetabolismData = [NSData dataWithBytes:&byteData[index++] length:2];
                        index++;
                        bwgDeviceData.fltBasalMetabolism = CFSwapInt32LittleToHost(*(int*)([basalMetabolismData bytes]));
                        //[AppUtility getAppDelegate].weightData.Other1 = basalMetabolism;
                    }
                    if(flag & 0x04) {
                        NSData *MantissaData = [NSData dataWithBytes:&byteData[index++] length:1];
                        NSData *Mantissa2Data = [NSData dataWithBytes:&byteData[index++] length:1];
                        int value1 = CFSwapInt16LittleToHost(*(int*)([MantissaData bytes]));
                        int value2 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0x0F);
                        int value3 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0xF0);
                        bwgDeviceData.fltBodyFat = (value1+value2*pow(2,8))/pow(10,(16-(value3>>4)));
                        if(bwgDeviceData.fltBodyFat > 100) {
                            bwgDeviceData.fltBodyFat = 100;
                        }
                        //[AppUtility getAppDelegate].weightData.BodyFat = bodyFat;
                    }
                    if(flag & 0x08) {
                        NSData *MantissaData = [NSData dataWithBytes:&byteData[index++] length:1];
                        NSData *Mantissa2Data = [NSData dataWithBytes:&byteData[index++] length:1];
                        int value1 = CFSwapInt16LittleToHost(*(int*)([MantissaData bytes]));
                        int value2 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0x0F);
                        int value3 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0xF0);
                        bwgDeviceData.fltBodyWater = (value1+value2*pow(2,8))/pow(10,(16-(value3>>4)));
                        if(bwgDeviceData.fltBodyWater > 100) {
                            bwgDeviceData.fltBodyWater = 100;
                        }
                        //[AppUtility getAppDelegate].weightData.Water = bodyWater;
                    }
                    if(flag & 0x10) {
                        NSData *MantissaData = [NSData dataWithBytes:&byteData[index++] length:1];
                        NSData *Mantissa2Data = [NSData dataWithBytes:&byteData[index++] length:1];
                        int value1 = CFSwapInt16LittleToHost(*(int*)([MantissaData bytes]));
                        int value2 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0x0F);
                        int value3 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0xF0);
                        bwgDeviceData.fltVisceralFatLevel = (value1+value2*pow(2,8))/pow(10,(16-(value3>>4)));
                        //[AppUtility getAppDelegate].weightData.Other2 = visceralFatLevel;
                    }
                    if(flag & 0x20) {
                        NSData *MantissaData = [NSData dataWithBytes:&byteData[index++] length:1];
                        NSData *Mantissa2Data = [NSData dataWithBytes:&byteData[index++] length:1];
                        int value1 = CFSwapInt16LittleToHost(*(int*)([MantissaData bytes]));
                        int value2 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0x0F);
                        int value3 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0xF0);
                        bwgDeviceData.fltMuscle = (value1+value2*pow(2,8))/pow(10,(16-(value3>>4)));
                        if(bwgDeviceData.fltMuscle > 100) {
                            bwgDeviceData.fltMuscle = 100;
                        }
                        //fltMuscle = (fltMuscle * self.weight) / 100;
                        //[AppUtility getAppDelegate].weightData.Muscle = (muscle * [AppUtility getAppDelegate].weightData.Weight)/100;//@20170117 肌肉率(實際為骨骼肌率)轉成肌肉量儲存及上傳
                    }
                    if(flag & 0x40) {
                        NSData *MantissaData = [NSData dataWithBytes:&byteData[index++] length:1];
                        NSData *Mantissa2Data = [NSData dataWithBytes:&byteData[index++] length:1];
                        int value1 = CFSwapInt16LittleToHost(*(int*)([MantissaData bytes]));
                        int value2 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0x0F);
                        int value3 = (CFSwapInt16LittleToHost(*(int*)([Mantissa2Data bytes])) & 0xF0);
                        bwgDeviceData.fltBone = (value1+value2*pow(2,8))/pow(10,(16-(value3>>4)));
                    }

                    if (self.delegate != nil) {
                        [self.delegate baseDevice:self resultData:bwgDeviceData];
                    }
                }
            }
        } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_WRITE]]) {
        } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_FEATURE]]) {
            DLog(@"G950 8A20 feature value=%@ length=%d",characteristic.value, (int)(characteristic.value.length));
        }
    }
    else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
    [self stopNstWaitTimer];
    m_characteristic_write = characteristic;
    nstWaitTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(fireNstWaitTimer) userInfo:nil repeats:false];
    
}
-(void)fireNstWaitTimer
{
    if (strPreWriteCmd != nil) {
        [m_peripheral writeValue:strPreWriteCmd forCharacteristic:m_characteristic_write type:CBCharacteristicWriteWithResponse];
        m_characteristic_write = nil;
    }
}
-(void)stopNstWaitTimer
{
    if (nstWaitTimer != nil) {
        [nstWaitTimer invalidate];
        nstWaitTimer = nil;
    }
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:BW_SERVICE_G950,SERVICE_INFO_DEVICE,nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = G950;
    
}
-(void)setG950Notify8A24
{
    DLog(@"setG950Notify8A24");
    for(CBService *service in m_peripheral.services)
    {
        if([service.UUID isEqual:[CBUUID UUIDWithString:BW_SERVICE_G950]])
        {
            for(CBCharacteristic *characteristic in service.characteristics)
            {
                if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_MEASURE]])
                {
                    if(!characteristic.isNotifying){
                        DLog(@"G950 8A24 setNotifyValue");
                        [m_peripheral setNotifyValue:YES forCharacteristic:characteristic];
                        [self performSelector:@selector(setG950Notify8A22) withObject:nil afterDelay:0.2];
                        return;
                    }
                }
            }
        }
    }
}
-(void)setG950Notify8A22
{
    DLog(@"setG950Notify8A22");
    for(CBService *service in m_peripheral.services)
    {
        if([service.UUID isEqual:[CBUUID UUIDWithString:BW_SERVICE_G950]])
        {
            for(CBCharacteristic *characteristic in service.characteristics)
            {
                if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_APPEND_MEASURE]])
                {
                    if(!characteristic.isNotifying){
                        DLog(@"G950 8A22 setNotifyValue");
                        [m_peripheral setNotifyValue:YES forCharacteristic:characteristic];
                        [self performSelector:@selector(setG950UTC) withObject:nil afterDelay:0.2];
                        return;
                    }
                }
            }
        }
    }
}
-(void)setG950UTC {
    DLog(@"setG950UTC");
    for(CBService *service in m_peripheral.services)
    {
        if([service.UUID isEqual:[CBUUID UUIDWithString:BW_SERVICE_G950]])
        {
            for(CBCharacteristic *characteristic in service.characteristics)
            {
                if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_WRITE]])
                {
                    NSDate *date = [NSDate date];
                    NSTimeInterval time = [date timeIntervalSince1970];
                    NSString *value = [NSString stringWithFormat:@"%02X", (int)time];
                    DLog(@"UTC value=%@ length=%lu",value,(unsigned long)[value length]);
                    
                    if([value length] > 7) {
                        NSData *utcData = [self stringToByte:[NSString stringWithFormat:@"02%@%@%@%@",[value substringWithRange:NSMakeRange(6, 2)], [value substringWithRange:NSMakeRange(4, 2)], [value substringWithRange:NSMakeRange(2, 2)], [value substringWithRange:NSMakeRange(0, 2)]]];
                        strPreWriteCmd = utcData;
                        [m_peripheral writeValue:utcData forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                    }
                }
            }
        }
    }
}
-(void)setG950EnableDisconnection {
    DLog(@"setG950EnableDisconnection");
    NSData *disCMD = [self stringToByte:@"22"];
    
    [self writeValueToG950Peripheral:m_peripheral value:disCMD];
}
-(void)writeValueToG950Peripheral:(CBPeripheral *)peripheral value:(NSData *)value
{
    DLog(@"writeValueToG950Peripheral");
    for(CBService *service in peripheral.services)
    {
        if([service.UUID isEqual:[CBUUID UUIDWithString:BW_SERVICE_G950]])
        {
            for(CBCharacteristic *characteristic in service.characteristics)
            {
                if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BW_UUID_G950_WRITE]])
                {
                    strPreWriteCmd = value;
                    [m_peripheral writeValue:value forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                }
            }
        }
    }
}
-(void)setG950UpdateUserInfo:(int)userId gender:(int)gender age:(int)age height:(float)height {
    NSString *userIdHexString = [NSString stringWithFormat:@"%02X", userId];
    NSString *genderHexString = [NSString stringWithFormat:@"%02X", gender];
    NSString *ageHexString = [NSString stringWithFormat:@"%02X", age];
    NSString *heightHexString = [NSString stringWithFormat:@"%04X", ((int)height*10) | 0xD000];
    
    DLog(@"setG950UpdateUserInfo %@", [NSString stringWithFormat:@"5107%@%@%@%@%@", userIdHexString, genderHexString, ageHexString, [heightHexString substringWithRange:NSMakeRange(2, 2)] , [heightHexString substringWithRange:NSMakeRange(0, 2)]]);
    NSData *updateUserInfoCMD = [self stringToByte:[NSString stringWithFormat:@"5107%@%@%@%@%@", userIdHexString, genderHexString, ageHexString, [heightHexString substringWithRange:NSMakeRange(2, 2)] , [heightHexString substringWithRange:NSMakeRange(0, 2)]]];
    [self writeValueToG950Peripheral:m_peripheral value:updateUserInfoCMD];
    //update userInfo後會主動收到8A82 notify
}
@end

