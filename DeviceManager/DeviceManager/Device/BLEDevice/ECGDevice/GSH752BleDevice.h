//
//  GSH752BleDevice.h
//  DeviceManager
//
//  Created by gsh_mac_2018 on 2019/12/11.
//  Copyright © 2019 GSH. All rights reserved.
//

#import "BaseBLEDevice.h"
#import "DeviceDatas.h"
#define ECGDATATYPE512B16 @"fcff10"
#define ECGDATATYPE256B8 @"f18008"
typedef enum
{
    GSH_752_DATA_RATE_300Hz_16bit = 0x01,
    GSH_752_DATA_RATE_600Hz_16bit = 0x03,
}GSH_752_ECG_Data_Rate;
@interface GSH_752_ECG_BLE_Data : NSObject {
    
}
@property(nonatomic, assign) uint8_t packet_number;             /* 0~31 from device. +1 every packet */
@property(strong, nonatomic) NSData* raw_data;                  /* 18 byte ecg raw data */
@property(nonatomic, assign) bool lead_on;                      /* true = LEAD on, false = LEAD off. */

@end
@protocol GSH_752_ECG_BLE_DevicesDelegate
@required

/* Notification when receive data from ecg device. */
-(void)GSH_752_ECG_BLE_ReceiveData:(GSH_752_ECG_BLE_Data*)data;

/* Notification when get error message. */
//-(void)GSH_752_ECG_BLE_ErrorMessage:(NSString *)msg Error_msg:(GSH_752_ECG_BLE_ERROR_MSG)Error_msg;

/* Notification when ECG device connected. */
//-(void)GSH_752_ECG_BLE_DidConnect;

/* Notification when ECG device disconnected */
//-(void)GSH_752_ECG_BLE_DidDisconnect;

/* Notification when get device information*/
//-(void)GSH_752_ECG_BLE_DeviceInfo_Data:(GSH_752_ECG_BLE_Device_info*)deviceinfo_data;

/* Notification when get HR and R2R information*/
-(void)GSH_752_ECG_BLE_HR_Data:(int)heartRate r2rInterval:(int)r2rInterval;

/* Notification when get SQ and overall_SQ information*/
-(void)GSH_752_ECG_BLE_SQ_Data:(int)SQ overallSQ:(float)overallSQ;
@optional

@end
@interface GSH752BleDevice: BaseBLEDevice
@property (retain,nonatomic) NSString * ecgDataType;
@end


