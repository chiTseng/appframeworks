#import "GSH752BleDevice.h"
#import "BleCommand.h"

#define DEVICE_NAME_ECGGSH @"GSH_752"//設備的名稱

#define GSH_752_ECG_SERVICE_UUID @"FFE0"
#define GSH_752_ECG_NOTIFY_UUID @"FFE1"
#define GSH_752_ECG_WRITE_UUID @"FFE2"
#define GSH_752_ECG_RESEND_UUID @"FFE3"

#define GSH_752_BATTERY_SERVICE_UUID @"180F"
#define GSH_752_BATTERY_NOTIFY_BATTERY_UUID @"2A19"

#define GSH_752_DIS_UUID @"180A"
#define GSH_752_DIS_READ_FW_UUID @"2A26"
#define GSH_752_DIS_READ_SN_UUID @"2A25"

#define GSH_752_POWER_OFF_CMD @"A1C0"
#define GSH_752_PASSWORD_CMD @"CC"

#define FRAMEWORK_VERSION @"1.0(1)"
#define GSH_752_ACCEPTABLE_FW_VERSION 100
@implementation GSH_752_ECG_BLE_Data
@synthesize packet_number;
@synthesize raw_data;
@synthesize lead_on;
@end
@interface GSH752BleDevice()
{
    /**
     寫入密碼用的 characterisitc
     */
    CBCharacteristic *m_characteristicFFF4;
    /**
     判斷密碼有無寫入成功
     */
    BOOL blnPasswordIsOk;
    NSString            *m_passwordString;
    CBPeripheral        *m_peripheral;
    NSTimer             *passwordTimer;
    uint8_t             m_resend_password_counter;
    uint8_t             m_resend_datarate_counter;
    CBCharacteristic    *m_characteristic_ecg_write_FFE2;
    CBCharacteristic    *m_characteristic_ecg_notify_FFE1;
    
    uint8_t             m_Data_Rate_set;
    
    bool password_reply;
    bool datarate_reply;
    bool password_timer_running;
    
}
@end
@implementation GSH752BleDevice
@synthesize ecgDataType;
/**掃到設備，準備與設備連線前可以做的事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setObject:@"try" forKey:@"try"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ECG_NOTY_BLETRYTOCONNECT" object:nil userInfo:dic];
    NSData *manufacturerData = NULL;
    manufacturerData = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
    
    if(manufacturerData == NULL)
        return;
    
    //NSData *manufacturerData = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
    //m_macAddress = [[manufacturerData description] stringByReplacingOccurrencesOfString:@" " withString:@""];
    //m_macAddress = [[m_macAddress substringWithRange:NSMakeRange(1, [m_macAddress length] - 2)] substringFromIndex:4];
    
    NSUInteger len = [manufacturerData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [manufacturerData bytes], len);
    
    int total = 0;
    for(int i=0;i<6;i++) {
        total += CFSwapInt16LittleToHost(*(int*)([[NSData dataWithBytes:&byteData[i+2] length:1] bytes]));
    }
    m_passwordString = [NSString stringWithFormat:@"CC%04x",total];
    
    //m_BLE_Device_info.ecg_ble_MAC_address = [NSData dataWithBytes:&byteData[2] length:6];
    
}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    DLog(@"請實作didConnectPeripheralHandler");
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:@"1" forKey:@"result"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ECG_NOTY_BLECONNECT_SUCCESS" object:nil userInfo:dic];
    m_peripheral = aPeripheral;
}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DISCONNECT_DEVICE" object:nil];
    DLog(@"請實作didDisconnectPeripheral");
    [passwordTimer invalidate];
    passwordTimer = nil;
    return true;
}
/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
            DLog(@"SYSTEMID ,%d",self.readSystemInfoStatus);
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
            DLog(@"SERIAL_NUMBER ,%d",self.readSystemInfoStatus);
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
            DLog(@"HARDWARE_VERSION ,%d",self.readSystemInfoStatus);
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
            DLog(@"FIRMWARE_VERSION ,%d",self.readSystemInfoStatus);
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
            DLog(@"SOFTWARE_VERSION ,%d",self.readSystemInfoStatus);
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
            DLog(@"MANUFACTURER_NAME ,%d",self.readSystemInfoStatus);
            [aPeripheral readValueForCharacteristic:aChar];
        }
        
    }
    DLog(@"請實作didDiscoverCharacteristicsForService");
    for (CBCharacteristic *aChar in service.characteristics){
        if([service.UUID isEqual:[CBUUID UUIDWithString:GSH_752_ECG_SERVICE_UUID]]){
            //NSLog(@"Discover SERVICE_ECG Found");
            for (CBCharacteristic *aChar in service.characteristics)
            {
                if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_752_ECG_NOTIFY_UUID]]){
                    [aPeripheral readValueForCharacteristic:aChar];
                } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_752_ECG_RESEND_UUID]]){
                    if(!aChar.isNotifying){
                        [aPeripheral setNotifyValue:YES forCharacteristic:aChar];
                    }
                } else  if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_752_ECG_WRITE_UUID]]){
                    [aPeripheral readValueForCharacteristic:aChar];
                }
            }
        }else if([service.UUID isEqual:[CBUUID UUIDWithString:GSH_752_BATTERY_SERVICE_UUID]]){
            //NSLog(@"Discover SERVICE_BAS Found");
            for (CBCharacteristic *aChar in service.characteristics)
            {
                if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_752_BATTERY_NOTIFY_BATTERY_UUID]]){
                    if(!aChar.isNotifying){
                        [aPeripheral setNotifyValue:YES forCharacteristic:aChar];
                    }
                    [aPeripheral readValueForCharacteristic:aChar];
                }
            }
        }else if([service.UUID isEqual:[CBUUID UUIDWithString:GSH_752_DIS_UUID]]){
            //NSLog(@"Discover SERVICE_DIS Found");
            for (CBCharacteristic *aChar in service.characteristics)
            {
                if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_752_DIS_READ_FW_UUID]]){
                    [aPeripheral readValueForCharacteristic:aChar];
                }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_752_DIS_READ_SN_UUID]]){
                    [aPeripheral readValueForCharacteristic:aChar];
                }
            }
        }
    }
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateValueForCharacteristic");
    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:GSH_752_ECG_SERVICE_UUID]]){
        /* UUID : RESENT */
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_752_ECG_RESEND_UUID]]){
            if(!characteristic.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
            }else{
                [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:GSH_752_ECG_RESEND_UUID]];
            }
            
            /* UUID : Notify */
        } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_752_ECG_NOTIFY_UUID]]){
            m_characteristic_ecg_notify_FFE1 = characteristic;
            if(password_reply && datarate_reply){
                if(!characteristic.isNotifying){
                    [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
                }else{
                    [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:GSH_752_ECG_NOTIFY_UUID]];
                }
            }
            
            /* UUID : Write */
        } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_752_ECG_WRITE_UUID]]) {
            if(!password_reply){
                if(!password_timer_running){
                    passwordTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                                                   selector:@selector(passwordTimerHandler:) userInfo:nil repeats:YES];
                    password_timer_running = true;
                    m_characteristic_ecg_write_FFE2 = characteristic;
                    NSData *password = [self stringToByte:m_passwordString];
                    [aPeripheral writeValue:password forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                    
                }else{
                    return;
                }
            }
        }
    }else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:GSH_752_BATTERY_SERVICE_UUID]]){
        //NSLog(@"BAS Found");
        /* UUID : BAS Read */
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_752_BATTERY_NOTIFY_BATTERY_UUID]]){
            int batteryvalue = CFSwapInt16LittleToHost(*(int*)([characteristic.value bytes]));
            [[NSUserDefaults standardUserDefaults] setInteger:batteryvalue forKey:@"breathTempBattery"];
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[NSString stringWithFormat:@"%d",batteryvalue] forKey:@"battery"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ECG_NOTY_BLECONNECT_SUCCESS" object:nil userInfo:dic];
        }
        
        /* UUID : DIS*/
    }else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:GSH_752_DIS_UUID]]){
        //NSLog(@"DIS Found");
        /* UUID : DIS Read FW*/
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_752_DIS_READ_FW_UUID]]){
            /* Check fw version is match SDK or not */
            NSUInteger len = [characteristic.value length];
            Byte *byteData = (Byte*)malloc(len);
            memcpy(byteData, [characteristic.value bytes], len);
            uint8_t H_byte,M_byte,L_byte;
            H_byte = byteData[len-3]-0x30;
            M_byte = byteData[len-2]-0x30;
            L_byte = byteData[len-1]-0x30;
            
            uint16_t fw_version_read_from_ble = 0;
            fw_version_read_from_ble = H_byte*100 + M_byte*10 + L_byte;
            NSLog(@"%hu", fw_version_read_from_ble);
            free(byteData);
            
        }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_752_DIS_READ_SN_UUID]]){
            /* UUID : DIS Read Serial number and check. */
            //m_macAddress = NULL;
            //m_macAddress = characteristic.value;
            
           // NSString* m_macAddress_Str = [[NSString alloc] initWithData:m_macAddress encoding:NSASCIIStringEncoding];
            
            /* Convert Mac Data to String */
            /*uint8_t *bytes = (uint8_t*)m_BLE_Device_info.ecg_ble_MAC_address.bytes;
            NSMutableString *ecg_ble_MAC_address_resultString= [NSMutableString stringWithCapacity:sizeof(bytes)*2];
            for(int i=0;i<6;i++){
                NSString *resultString =[NSString stringWithFormat:@"%02lx",(unsigned long)bytes[i]];
                [ecg_ble_MAC_address_resultString appendString:[resultString uppercaseString]];
            }
            
            NSLog(ecg_ble_MAC_address_resultString);*/
            
        }
    }

    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
    }
    else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            NSString *value = [self NSDataToHexString:characteristic.value];
            value = [self StringConvert:value];
            DLog(@"mac is %@",value);
            self.macAddress = value;
    }
    else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
    }
    else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
    }
    else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
    }
    else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
    }
    
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:GSH_752_ECG_SERVICE_UUID,GSH_752_DIS_UUID,GSH_752_BATTERY_SERVICE_UUID,nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = DEVICE_NAME_ECGGSH;
    
}
-(void)passwordTimerHandler:(NSTimer *)sender{
    
    if(!password_reply){
        
        NSData *password = [self stringToByte:m_passwordString];
        [m_peripheral writeValue:password forCharacteristic:m_characteristic_ecg_write_FFE2 type:CBCharacteristicWriteWithResponse];
        m_resend_password_counter++;
        if(m_resend_password_counter >= 10){
            NSString * state = nil;
            state = NSLocalizedString(@"BLE Password response timeout.",nil);
            NSLog(@"ECG_BLELog:BLE Password response timeout.");
            //[delegate GSH_752_ECG_BLE_ErrorMessage:state Error_msg:GSH_752_BLE_PASSWORD_TIMEOUT];
            //[self GSH_752_ecg_ble_CMD_Disconnect];
        }
        
    }else{
        
        if(!datarate_reply){
            NSData *datarateset ;
            
            switch(m_Data_Rate_set){
                case GSH_752_DATA_RATE_300Hz_16bit:
                    datarateset = [self stringToByte:@"F18008"];
                    [m_peripheral writeValue:datarateset forCharacteristic:m_characteristic_ecg_write_FFE2 type:CBCharacteristicWriteWithResponse];
                    break;
                    
                case GSH_752_DATA_RATE_600Hz_16bit:
                    datarateset = [self stringToByte:@"F38010"];
                    [m_peripheral writeValue:datarateset forCharacteristic:m_characteristic_ecg_write_FFE2 type:CBCharacteristicWriteWithResponse];
                    break;
                    
                    
                default:
                    datarateset = [self stringToByte:@"F18008"];
                    [m_peripheral writeValue:datarateset forCharacteristic:m_characteristic_ecg_write_FFE2 type:CBCharacteristicWriteWithResponse];
                    break;
                    
            }
            
            m_resend_datarate_counter++;
            if(m_resend_datarate_counter >= 10){
                NSString * state = nil;
                state = NSLocalizedString(@"BLE Datarate response timeout.",nil);
                NSLog(@"ECG_BLELog:BLE Datarate response timeout.");
                //[delegate GSH_752_ECG_BLE_ErrorMessage:state Error_msg:GSH_752_BLE_DATARATE_TIMEOUT];
                //[self GSH_752_ecg_ble_CMD_Disconnect];
            }
        }else{
            
            if(!m_characteristic_ecg_notify_FFE1.isNotifying){
                NSLog(@"ECG_BLELog:Enable Notify");
                [m_peripheral setNotifyValue:YES forCharacteristic:m_characteristic_ecg_notify_FFE1];
            }
            [passwordTimer invalidate];
            passwordTimer = nil;
        }
    }
}
#pragma mark - Ble DidReceiveData
-(void)bleDidReceiveData:(NSData *) data length:(int) length withUUID:(CBUUID *)uuid {
    //NSLog(@"bleDidReceiveData");
    
    NSData *tmpData = [[NSData alloc] initWithData:data];
    
    if([((CBUUID *)uuid) isEqual:[CBUUID UUIDWithString:GSH_752_ECG_RESEND_UUID]]) {
        
        NSUInteger len = [tmpData length];
        if(len == 3){
            Byte *byteData = (Byte*)malloc(len);
            memcpy(byteData, [tmpData bytes], len);
            NSData *cmd = [NSData dataWithBytes:&byteData[0] length:1];
            NSData *reply_H = [NSData dataWithBytes:&byteData[1] length:1];
            NSData *reply_L = [NSData dataWithBytes:&byteData[2] length:1];
            
            int intcmd = CFSwapInt16LittleToHost(*(int*)([cmd bytes]));
            int intreply_H = CFSwapInt16LittleToHost(*(int*)([reply_H bytes]));
            int intreply_L = CFSwapInt16LittleToHost(*(int*)([reply_L bytes]));
            
            if(intcmd == 0xCC){
                if(intreply_H == 0x4F && intreply_L == 0x4B){
                    password_reply = true;
                    //NSLog(@"Got Password Reply");
                    if(!datarate_reply){
                        NSData *datarateset ;
                        
                        switch(m_Data_Rate_set){
                            case GSH_752_DATA_RATE_300Hz_16bit:
                                datarateset = [self stringToByte:@"F18008"];
                                [m_peripheral writeValue:datarateset forCharacteristic:m_characteristic_ecg_write_FFE2 type:CBCharacteristicWriteWithResponse];
                                break;
                                
                            case GSH_752_DATA_RATE_600Hz_16bit:
                                datarateset = [self stringToByte:@"F38010"];
                                [m_peripheral writeValue:datarateset forCharacteristic:m_characteristic_ecg_write_FFE2 type:CBCharacteristicWriteWithResponse];
                                break;
                                
                            default:
                                datarateset = [self stringToByte:@"F18008"];
                                [m_peripheral writeValue:datarateset forCharacteristic:m_characteristic_ecg_write_FFE2 type:CBCharacteristicWriteWithResponse];
                                break;
                                
                        }
                    }
                }
            }else if(intcmd == 0xF1 || intcmd == 0xF3){
                if(intreply_H == 0x4F && intreply_L == 0x4B){
                    datarate_reply = true;
                    //NSLog(@"Got Data rate setting Reply");
                    if(password_reply && datarate_reply){
                        if(!m_characteristic_ecg_notify_FFE1.isNotifying){
                            NSLog(@"ECG_BLELog:Enable Notify");
                            [m_peripheral setNotifyValue:YES forCharacteristic:m_characteristic_ecg_notify_FFE1];
                            
                        }
                        [passwordTimer invalidate];
                        passwordTimer = nil;
                    }
                }
            }
            free(byteData);
        }else{ //len == 3
            if(len == 6){
                Byte *byteData = (Byte*)malloc(len);
                memcpy(byteData, [tmpData bytes], len);
                NSData *countData = [NSData dataWithBytes:&byteData[0] length:1];
                int CMDData = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
                
                if(CMDData == 0x10){
                    countData = [NSData dataWithBytes:&byteData[3] length:1];
                    int HR_Data = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
                    countData = [NSData dataWithBytes:&byteData[4] length:1];
                    int tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
                    int RR_Data = tempdatabyte;
                    RR_Data = RR_Data << 8;
                    countData = [NSData dataWithBytes:&byteData[5] length:1];
                    tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
                    RR_Data = RR_Data + tempdatabyte;
                    //[delegate GSH_752_ECG_BLE_HR_Data:HR_Data r2rInterval:RR_Data];
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                    [dic setObject:[NSString stringWithFormat:@"%d",RR_Data] forKey:@"r2r"];
                    [dic setObject:[NSString stringWithFormat:@"%d",HR_Data] forKey:@"hr"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ECG_R2R_RETURN" object:nil userInfo:dic];
                }else if(CMDData == 0x11){
                    countData = [NSData dataWithBytes:&byteData[3] length:1];
                    int SQ_Data = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
                    countData = [NSData dataWithBytes:&byteData[4] length:1];
                    float tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
                    float OSQ_Data = tempdatabyte;
                    //OSQ_Data = OSQ_Data << 8;
                    countData = [NSData dataWithBytes:&byteData[5] length:1];
                    tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
                    tempdatabyte = tempdatabyte/256;
                    OSQ_Data = OSQ_Data + tempdatabyte;
                    if(SQ_Data > 5){
                        SQ_Data = 0;
                    }
                    //[delegate GSH_752_ECG_BLE_SQ_Data:SQ_Data overallSQ:OSQ_Data];
                }
                
                free(byteData);
            }
            return;
        }
        
    } else if([((CBUUID *)uuid) isEqual:[CBUUID UUIDWithString:GSH_752_ECG_NOTIFY_UUID]]){
        //畫圖用的資料
        GSH_752_ECG_BLE_Data *m_Data = [[GSH_752_ECG_BLE_Data alloc] init];
        NSUInteger len = [tmpData length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [tmpData bytes], len);
        NSData *countData = [NSData dataWithBytes:&byteData[0] length:1];
        int intCountData = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
        int sensorBit = intCountData & 0x80;
        if (sensorBit == 0x80){
            m_Data.lead_on = true;
        } else {
            m_Data.lead_on = false;
        }
        m_Data.packet_number = intCountData & 0x1F;
        m_Data.raw_data = [NSData dataWithBytes:&byteData[2] length:18];
        //[[self delegate] GSH_752_ECG_BLE_ReceiveData:m_Data];
        
        NSData *tmpData = [[NSData alloc] initWithData:m_Data.raw_data];
        for(int i=2; i<len; i=i+2) {
            
            NSData *ecgData = [NSData dataWithBytes:&byteData[i] length:1];
            NSData *ecgData_L = [NSData dataWithBytes:&byteData[i+1] length:1];
            
            int intEcgData = 0;
            intEcgData = CFSwapInt16LittleToHost(*(int*)([ecgData bytes]));
            intEcgData = intEcgData << 8;
            intEcgData = intEcgData + CFSwapInt16LittleToHost(*(int*)([ecgData_L bytes]));
            
            if (intEcgData >= 32768) {
                intEcgData = intEcgData-65536;
            }
            NSMutableDictionary *dic = [NSMutableDictionary new];
            [dic setObject:[[NSNumber alloc] initWithDouble:(intEcgData * -1)] forKey:@"value"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"noControlCountdown" object:nil userInfo:dic];
            
        }
        free(byteData);
    }
}
/*-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    ECGDeviceData *ecgData = [ECGDeviceData new];
    ecgData.recData = data;
    ecgData.recUUID = uuid;
    ecgData.intLength = length;
    DLog(@"%@", uuid);
    if (self.delegate != nil) {
        [self.delegate baseDevice:self resultData:ecgData];
    }
}*/
@end
