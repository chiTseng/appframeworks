//
//  ECGBleDevice.m
//  SoHappy
//
//  Created by elaine on 2017/9/20.
//
//
#define DEVICE_NAME_ECGGSH @"GSH_ECG"//設備的名稱
#define ECG_SERVICE_INFO_180A @"180A"//設備可搜尋服務的uuid
#define SERVICE_ECG_FFF0 @"FFF0"//設備回傳數值時服務的uuid
#define ECG_SERVICE_INFO_BATTERY_180F @"180F"//取得電量的服務uuid


#define ECG_UUID_NOTIFY_FFF3 @"FFF3"//收ECG傳過來的數值會在這裡收到
#define ECG_INFO_NOTIFY_BATTERY_2A19 @"2A19"//電量服務中的notify可取得電量的值


#define ECG_UUID_WRITE_FFF4 @"FFF4"//寫入密碼時會用到這個命令
#define ECG_UUID_RESEND_FFF5 @"FFF5"//寫入密碼會透過這裡回傳是否成功
#define ECG_POWER_OFF_CMD_A1C0 @"A1C0"//關閉ECG電源的命令，此處沒用到

#define ECG_PASSWORD_CMD_CC @"CC"//寫入密碼的字串頭
#import "ECGBleDevice.h"
#import "BleCommand.h"
@interface ECGBleDevice()
{
    /**
     寫入密碼用的 characterisitc
     */
    CBCharacteristic *m_characteristicFFF4;
    /**
     判斷密碼有無寫入成功
     */
    BOOL blnPasswordIsOk;
    
}
@end
@implementation ECGBleDevice
@synthesize ecgDataType;
/**掃到設備，準備與設備連線前可以做的事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (ecgDataType == nil)
    {
        ecgDataType = ECGDATATYPE256B8;
    }
    m_characteristicFFF4 = nil;
    blnPasswordIsOk = false;
    DLog(@"請實作didDiscoverPeripheralHandler");
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setObject:@"try" forKey:@"try"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ECG_NOTY_BLETRYTOCONNECT" object:nil userInfo:dic];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"passwordString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //for ECG V105 password
    NSData *manufacturerData = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
    NSUInteger len = [manufacturerData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [manufacturerData bytes], len);
    int total = 0;
    for(int i=0;i<6;i++) {
        total += CFSwapInt16LittleToHost(*(int*)([[NSData dataWithBytes:&byteData[i+2] length:1] bytes]));
    }
    DLog(@"passwordString=%@",[NSString stringWithFormat:@"%@%04x",ECG_PASSWORD_CMD_CC,total]);
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@%04x",ECG_PASSWORD_CMD_CC,total] forKey:@"passwordString"];
    [[NSUserDefaults standardUserDefaults] setObject:self.macAddress forKey:@"ecgMacAddress"];
    [[NSUserDefaults standardUserDefaults] synchronize];
        
}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    DLog(@"請實作didConnectPeripheralHandler");
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:@"1" forKey:@"result"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ECG_NOTY_BLECONNECT_SUCCESS" object:nil userInfo:dic];
}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DISCONNECT_DEVICE" object:nil];
    DLog(@"請實作didDisconnectPeripheral");
    m_characteristicFFF4 = nil;
    blnPasswordIsOk = false;
    return true;
}
/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
        
    }
    DLog(@"請實作didDiscoverCharacteristicsForService");
    for (CBCharacteristic *aChar in service.characteristics){
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:ECG_UUID_NOTIFY_FFF3]]){
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:ECG_UUID_RESEND_FFF5]]){
            [aPeripheral setNotifyValue:YES forCharacteristic:aChar];
        } else  if([aChar.UUID isEqual:[CBUUID UUIDWithString:ECG_UUID_WRITE_FFF4]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:ECG_INFO_NOTIFY_BATTERY_2A19]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        }
    }
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateValueForCharacteristic");
    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_ECG_FFF0]]){
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:ECG_UUID_NOTIFY_FFF3]]){//fff3才是接收ecg rawData的地方
            if(!characteristic.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
                DLog(@"ECG_UUID_NOTIFY setNotifyValue");
            }else{
                if (blnPasswordIsOk == false) {
                    if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"passwordString"] isEqualToString:@""]) {
                        NSData *password = [self stringToByte:[[NSUserDefaults standardUserDefaults] valueForKey:@"passwordString"]];
                        [aPeripheral writeValue:password forCharacteristic:m_characteristicFFF4 type:CBCharacteristicWriteWithResponse];
                    }
                }
                else
                {
                    DLog(@"ECG_UUID_NOTIFY characteristic.value=%@ length=%d",characteristic.value, (int)(characteristic.value.length));
                    NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
                    [self bleDidReceiveData:characteristic.value length:(int)characteristic.value.length withUUID:[CBUUID UUIDWithString:ECG_UUID_NOTIFY_FFF3] withModelName:[aPeripheral.name copy] DeviceID:tmpUUIDstr];
                }
            }
        } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:ECG_UUID_WRITE_FFF4]]) {//設定ecg sample rate
            DLog(@"ECG_UUID_WRITE PASSWORD=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"passwordString"]);
            m_characteristicFFF4 = characteristic;
            if(![[[NSUserDefaults standardUserDefaults]
                valueForKey:@"passwordString"] isEqualToString:@""]) {
                DLog(@"a======>256/8<=======");
                NSData *sampleRate;
                if ([ecgDataType isEqualToString:ECGDATATYPE256B8]) {
                    sampleRate = [self stringToByte:@"f18008"];//256/8
                }
                else
                {
                    sampleRate = [self stringToByte:@"fcff10"];//512/16
                }
//#else
                //
//#endif
                //NSData *sampleRate = [self stringToByte:@"faff08"];//512/8
                //NSData *sampleRate = [self stringToByte:@"f18008"];//256/8
                [aPeripheral writeValue:sampleRate forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:ECG_UUID_RESEND_FFF5]])
        {//取得設備回傳的命令這裡主要是針對密碼有沒有設定成功
            DLog(@"ECG_UUID_NOTIFY characteristic.value=%@ length=%d",characteristic.value, (int)(characteristic.value.length));
            NSString *hexString = @"";
            UInt8 byte = 0;
            for (int i = 0; i < characteristic.value.length; i++) {
                [characteristic.value getBytes:&byte range:NSMakeRange(i, 1)];
                NSString *_cha = [NSString stringWithFormat:@"%02x",byte];
                hexString = [hexString stringByAppendingString:_cha];
            }
            DLog(@"hexString is %@",hexString);
            if ([hexString isEqualToString:@"cc4f4b"]) {
                blnPasswordIsOk = true;
            }
            else
            {
                DLog(@"ECG_UUID_WRITE PASSWORD=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"passwordString"]);
                if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"passwordString"] isEqualToString:@""]) {
                    NSData *password = [self stringToByte:[[NSUserDefaults standardUserDefaults] valueForKey:@"passwordString"]];
                    [aPeripheral writeValue:password forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                    
                }
            }
        }
    }
    else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:ECG_SERVICE_INFO_BATTERY_180F]]){//取得電量
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:ECG_INFO_NOTIFY_BATTERY_2A19]]){
            DLog(@"UUID_NOTIFY_BATTERY");
            NSUInteger len = [characteristic.value length];
            Byte *byteData = (Byte*)malloc(len);
            memcpy(byteData, [characteristic.value bytes], len);
            NSData *batteryData = [NSData dataWithBytes:&byteData[0] length:2];
            int value1 = CFSwapInt16LittleToHost(*(int*)([batteryData bytes]));
            DLog(@"battery:%d %%",value1);
            //[[NSUserDefaults standardUserDefaults] setInteger:value1 forKey:ECG_NOTY_BATTERY_VALUE];
            [[NSUserDefaults standardUserDefaults] setInteger:value1 forKey:@"breathTempBattery"];
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[NSString stringWithFormat:@"%d",value1] forKey:@"battery"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ECG_NOTY_BLECONNECT_SUCCESS" object:nil userInfo:dic];
        }
    }
    else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            NSString *value = [self NSDataToHexString:characteristic.value];
            value = [self StringConvert:value];
            DLog(@"mac is %@",value);
            self.macAddress = value;
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:ECG_SERVICE_INFO_180A,SERVICE_ECG_FFF0,ECG_SERVICE_INFO_BATTERY_180F,nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = DEVICE_NAME_ECGGSH;
    
}
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    ECGDeviceData *ecgData = [ECGDeviceData new];
    ecgData.recData = data;
    ecgData.recUUID = uuid;
    ecgData.intLength = length;
    DLog(@"%@", uuid);
    if (self.delegate != nil) {
        [self.delegate baseDevice:self resultData:ecgData];
    }
}
@end
