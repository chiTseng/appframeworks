//
//  GSH711BleDevice.h
//  DeviceManager
//
//  Created by elaine on 2018/9/20.
//  Copyright © 2018年 GSH. All rights reserved.
//

//#define ECG512BIT16//這個如果移除則預設會是256BIT8
#import "BaseBLEDevice.h"
#import "DeviceDatas.h"
#define ECGDATATYPE512B16 @"fcff10"
#define ECGDATATYPE256B8 @"f18008"
/**支援ECG設備*/
@interface GSH711BleDevice : BaseBLEDevice
@property (retain,nonatomic) NSString * ecgDataType;
@end
