//
//  BPGSHBPBleDevice.m
//  SoHappy
//
//  Created by elaine on 2017/9/6.
//
//
#define SUPPORVERSION "e0 69"
#define GSH_BP   @"GSH-BP"//設備名稱 有兩種版本
#define GSHBP   @"GSH BP"//設備名稱  有兩種版本

#define GSHBP_SERVICE @"FFF0"//GSHBP 服務的uuid

#define GSHBP_UUID_NOTIFY @"FFF4"//量測中使用notify 來取得數值
#define GSHBP_UUID_GSH_VALUE @"FFF3"//量測完已經有數值，透過這個命令來取得數值

#import "BPGSHBPBleDevice.h"
#import "BleCommand.h"
@interface BPGSHBPBleDevice()
{
    /**
     如果藍牙設備一直沒有傳送notyfy資料上來 這透過這個timer判斷時間到就送出GSHBP_UUID_GSH_VALUE
     來取得是否已經有結果在設備上
     */
    NSTimer *gshBtValueWriteTimer;
    /**
     暫存已連結得藍牙設備，以利gshBtValueWriteTimer時間到時可以透過mPeripheral 傳送命令
     */
    CBPeripheral *mPeripheral;
    
    NSString *strVersion;
}
@end
@implementation BPGSHBPBleDevice
/**
 這裡目前不做任何事情
 */
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    DLog(@"請實作didDiscoverPeripheralHandler");
    strVersion = @"";
}
/**
 將連結的設備 assign到mPeripheral 並建立gshBtValueWriteTimer
 */
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    DLog(@"請實作didConnectPeripheralHandler");
    mPeripheral = aPeripheral;
    gshBtValueWriteTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(writeBPValue) userInfo:nil repeats:NO];
}
/**
 因為斷線了記得把timer 清掉
 */
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    DLog(@"請實作didDisconnectPeripheral");
    if (gshBtValueWriteTimer != nil) {
        [gshBtValueWriteTimer invalidate];
        gshBtValueWriteTimer = nil;
    }
    return true;
}
/**read 該設備上所有需要用到的服務*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    DLog(@"請實作didDiscoverCharacteristicsForService");
    //NSData *valueA0 = [self stringToByte:@"A0"];
    if([service.UUID isEqual:[CBUUID UUIDWithString:GSHBP_SERVICE]])
    {
        for(CBCharacteristic *characteristic in service.characteristics)
        {
            if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSHBP_UUID_GSH_VALUE]])
            {
                //[aPeripheral writeValue:valueA0 forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                [self writeBPGetVersion];
            }
            else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSHBP_UUID_NOTIFY]])
            {
                
                [aPeripheral readValueForCharacteristic:characteristic];
            }
        }
    }
    else
    {
        for(CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }
            else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            }
        }
    }
}
/**
 收到設備回傳的資料
 GSHBP_UUID_NOTIFY 量測中到量測完 會透過這個回傳數值
 GSHBP_UUID_GSH_VALUE 設備早已有量測完的資料才連線 則會透過這個取得設備上的數值
 */
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateValueForCharacteristic");
    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:GSHBP_SERVICE]]){
        
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSHBP_UUID_NOTIFY]]){
            if(!characteristic.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
            }else{
                if (gshBtValueWriteTimer != nil) {
                    [gshBtValueWriteTimer invalidate];
                    gshBtValueWriteTimer = nil;
                }
                NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
                [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:GSHBP_UUID_NOTIFY] withModelName:[aPeripheral.name copy] DeviceID:tmpUUIDstr];
            }
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSHBP_UUID_GSH_VALUE]]) { //@2015/05/22
            DLog(@"UUID_BP_VALUE read value = %@ length = %lu", characteristic.value,characteristic.value.length);
            //@2015/8/13 支援單ㄧ類型多藍芽裝置
            NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
            [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:GSHBP_UUID_GSH_VALUE] withModelName:[aPeripheral.name copy] DeviceID:tmpUUIDstr];
            
        }

    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            /*NSString *value = [self NSDataToHexString:characteristic.value];
             value = [self StringConvert:value];
             DLog(@"mac is %@",value);
             self.macAddress = value;*/
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
/**
 write GSHBP_UUID_GSH_VALUE 後 透過這裡讀數值
 */
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
    NSData *data = characteristic.value;
    DLog(@"%@",data);
    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSHBP_UUID_GSH_VALUE]]) {
        [aPeripheral readValueForCharacteristic:characteristic];
    }
}
/**
 Timer 到了會執行這裡
 */
-(void)writeBPValue {
    if(mPeripheral) {
        NSData *valueA0 = [self stringToByte:@"A0"];
        [self writeValueToPeripheral:mPeripheral value:valueA0];
    }
}
-(void)writeBPDisconnect
{
    if(mPeripheral) {
        NSData *valueA0 = [self stringToByte:@"C0"];
        [self writeValueToPeripheral:mPeripheral value:valueA0];
    }
}
-(void)writeBPGetVersion
{
    if (mPeripheral) {
        NSData *valueE0 = [self stringToByte:@"E0"];
        [self writeValueToPeripheral:mPeripheral value:valueE0];
    }
}
/**
 取得已存在設備上數值的命令
 */
-(void)writeValueToPeripheral:(CBPeripheral *)peripheral value:(NSData *)value
{
    
    
    for(CBService *service in peripheral.services)
    {
        if([service.UUID isEqual:[CBUUID UUIDWithString:GSHBP_SERVICE]])
        {
            for(CBCharacteristic *characteristic in service.characteristics)
            {
                if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSHBP_UUID_GSH_VALUE]])
                {
                    [peripheral writeValue:value forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                }
            }
        }
    }
}
/**初始化*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = @[GSHBP_SERVICE,SERVICE_INFO_DEVICE];
        strVersion = @"";
        [self initDeviceValue];
    }
    return self;
}
/**設定設備名稱*/
-(void)initDeviceValue
{
    self.deviceName = [NSString stringWithFormat:@"%@,%@",GSHBP,GSH_BP];
    
}
/**藍牙資料透過這裡處理*/
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    if (length == 2) {
        NSString* newStr = [self NSDataToHexString:data];
        DLog(@"version = %@",newStr);
        if ([newStr containsString:@"e0 69"]) {
            strVersion = newStr;
        }
        
      
        
    }
    if(length ==10 || length == 12 || length == 14){//BP final value
        if (gshBtValueWriteTimer != nil) {
            [gshBtValueWriteTimer invalidate];
            gshBtValueWriteTimer = nil;
        }
        NSUInteger len = [data length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [data bytes], len);
        NSData *SystolicData = [NSData dataWithBytes:&byteData[1] length:2];
        NSData *DiastolicData = [NSData dataWithBytes:&byteData[3] length:2];
        NSData *PulseData = [NSData dataWithBytes:&byteData[7] length:2];
        
        int value1 = CFSwapInt16BigToHost(*(int*)([SystolicData bytes]));
        int value2 = CFSwapInt16BigToHost(*(int*)([DiastolicData bytes]));
        int value3 = CFSwapInt16BigToHost(*(int*)([PulseData bytes]));
        free(byteData);
        DLog(@"BP final value : \nSystolic: %d mmHg, Diastolic: %d mmHg, Pulse : %d Pulse/min",value1,value2,value3);
        BPDeviceData *bpDevicData = [BPDeviceData new];
        bpDevicData.intSys = value1;
        bpDevicData.intDia = value2;
        bpDevicData.intPulse = value3;
        bpDevicData.intAPI = -1;
        bpDevicData.intAVI = -1;
        bpDevicData.strVersion = strVersion;
        if ([strVersion containsString:@"e0 69"]) {
            [self writeBPDisconnect];
        }
        if (self.delegate != nil) {
            [self.delegate baseDevice:self resultData:bpDevicData];
        }
    }
    else
    {
        DLog(@"length error is %d",length);
    }
}
@end
