//
//  BPGSH823B.m
//  DeviceManager
//
//  Created by gsh_mac_2018 on 2019/11/27.
//  Copyright © 2019 GSH. All rights reserved.
//
//#define DeviceName_GSH_822B             @"GSH_822B"
#define DeviceName_GSH_823B             @"GSH_823B"

#define GSH_822B_BP_SERVICE_UUID        @"1810"
#define GSH_822B_BP_INDICATE_UUID       @"2A35"     //最後量測結果.
#define GSH_822B_BP_COMMAND_UUID        @"2A49"     //指令.
#define GSH_822B_BP_CUFF_UUID           @"2A36"     //袖套即時壓力.

#import "BPGSH823B.h"
#import "BleCommand.h"
@implementation BP_BLE_Data
@synthesize Time_Total_Sec;         /* Device's time total sec */
@synthesize Year;                   /* Device's time Year */
@synthesize Month;                  /* Device's time Month */
@synthesize Date;                   /* Device's time Date */
@synthesize Hour;                   /* Device's time Hour */
@synthesize Minute;                 /* Device's time Minute */
@synthesize Second;                 /* Device's time Second */
@synthesize SYS;                    /* User SYS data 收縮壓 mmHg */
@synthesize DIA;                    /* User DIA data 舒張壓 mmHg */
@synthesize HR;                     /* User Heart rate data */
@synthesize UserID;                 /* User ID */
@end
@interface BPGSH823B()
{
    CBPeripheral        *m_peripheral;
    CBCharacteristic    *m_characteristic_Write;         //for password Write
    CBCharacteristic    *m_characteristic_Notify;        //for password Notify
    CBCharacteristic    *m_characteristic_BP_Indicate;   //for BP Indicate
    CBCharacteristic    *m_characteristic_BP_Notify;     //for BP Notify
    
    NSString            *passwordString;
        
    NSTimer             *passwordTimer;
    
    uint8_t             m_resend_password_counter;
    
    uint16_t            m_ui16t_cuff_data;
    
    BOOL                password_timer_running;
    BOOL                password_reply;
}
@end
@implementation BPGSH823B
/**
 這裡目前不做任何事情
 */
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    DLog(@"請實作didDiscoverPeripheralHandler");
    m_peripheral = nil;
    m_characteristic_Write = nil;
    m_characteristic_Notify = nil;
    m_characteristic_BP_Indicate = nil;
    m_characteristic_BP_Notify = nil;
    passwordString = @"";
    m_ui16t_cuff_data = 0;
    NSData *manufacturerData = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
    NSString *macAddress = [[manufacturerData description] stringByReplacingOccurrencesOfString:@" " withString:@""];
    macAddress = [[macAddress substringWithRange:NSMakeRange(1, [macAddress length] - 2)] substringFromIndex:4];
    
    NSUInteger len = [manufacturerData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [manufacturerData bytes], len);
    int total = 0;
    for(int i=0;i<6;i++) {
        total += CFSwapInt16LittleToHost(*(int*)([[NSData dataWithBytes:&byteData[i+2] length:1] bytes]));
    }
    passwordString = [NSString stringWithFormat:@"CC%04X",total];
    NSLog(@"passwordString=%@",passwordString);
}
/**
 將連結的設備 assign到mPeripheral 並建立gshBtValueWriteTimer
 */
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    DLog(@"請實作didConnectPeripheralHandler");
    m_peripheral = aPeripheral;
}
/**
 因為斷線了記得把timer 清掉
 */
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    DLog(@"請實作didDisconnectPeripheral");
    
    if (passwordTimer != nil) {
        [passwordTimer invalidate];
        passwordTimer = nil;
    }
    
    
    return true;
}
/**read 該設備上所有需要用到的服務*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    if([service.UUID isEqual:[CBUUID UUIDWithString:GSH_822B_BP_SERVICE_UUID]]){
        NSLog(@"Discover BP SERVICE Found");
        for (CBCharacteristic *characteristic in service.characteristics)
        {
            if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_822B_BP_INDICATE_UUID]]){
                if(!characteristic.isNotifying){
                    [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
                    NSLog(@"Enable BP_INDICATE");
                    
                }
                m_characteristic_BP_Indicate = characteristic;
                
            }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_822B_BP_CUFF_UUID]]){
                if(!characteristic.isNotifying){
                    [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
                    NSLog(@"Enable BP_NOTIFY");
                    
                }
                m_characteristic_BP_Notify = characteristic;
                
            }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_822B_BP_COMMAND_UUID]]){
                NSLog(@"Command UUID found");
                
                if(!characteristic.isNotifying){
                    [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
                    NSLog(@"Enable Command");
                }
                
                m_characteristic_Write = characteristic;
                m_characteristic_Notify = characteristic;
                
                NSLog(@"Write password");
                
                NSData *password = [self stringToByte:passwordString];
                [aPeripheral writeValue:password forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
               
                
                NSLog(@"Enable password timer");
                if(!password_timer_running){
                    passwordTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                                                       selector:@selector(passwordTimerHandler:) userInfo:nil repeats:YES];
                    password_timer_running = true;
                    NSData *password = [self stringToByte:passwordString];
                    [aPeripheral writeValue:password forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                    m_resend_password_counter = 0;
                }else{
                    return;
                }
                
            }
        }
    }else if([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]]){
        NSLog(@"Discover DIS Found");
        for (CBCharacteristic *characteristic in service.characteristics)
        {
            if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
                [aPeripheral readValueForCharacteristic:characteristic];
            }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]){
                [aPeripheral readValueForCharacteristic:characteristic];
            }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
                [aPeripheral readValueForCharacteristic:characteristic];
            }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]){
                [aPeripheral readValueForCharacteristic:characteristic];
            }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]){
                [aPeripheral readValueForCharacteristic:characteristic];
            }
        }
    }
}
/**
 收到設備回傳的資料
 GSHBP_UUID_NOTIFY 量測中到量測完 會透過這個回傳數值
 GSHBP_UUID_GSH_VALUE 設備早已有量測完的資料才連線 則會透過這個取得設備上的數值
 */
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateValueForCharacteristic");
    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:GSH_822B_BP_SERVICE_UUID]]){
           if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_822B_BP_INDICATE_UUID]]){
              
               [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:GSH_822B_BP_INDICATE_UUID]];
               
           }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_822B_BP_CUFF_UUID]]){
               
               [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:GSH_822B_BP_CUFF_UUID]];
               
           }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_822B_BP_COMMAND_UUID]]){
               NSLog(@"didUpdateValueForCharacteristic:BP Command notify");
               
               if(!characteristic.isNotifying){
                   [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
                   NSLog(@"didUpdateValueForCharacteristic Enable BP Command notify");
                   
               }
               [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:GSH_822B_BP_COMMAND_UUID]];
               
           }
           
       }else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
       {
           if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
               self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
               self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
           }
           else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
               self.readSystemInfoComplete += SUPPORT_SYSTEMID;
               NSString *value = [self NSDataToHexString:characteristic.value];
               value = [self StringConvert:value];
               DLog(@"mac is %@",value);
               self.macAddress = value;
           }
           else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
               self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
               self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
               DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
           }
           else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
               self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
               self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
               DLog(@"firmwareVersion is %@",self.firmwareVersion);
           }
           else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
               self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
               self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
               DLog(@"softwareVersion is %@",self.softwareVersion);
           }
           else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
               self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
               self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
               DLog(@"hardwareVersion is %@",self.hardwareVersion);
           }
       }
       if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
           if (self.delegate != nil) {
               [self.delegate baseDevice:self];
           }
           self.readSystemInfoStatus = -1;
           self.readSystemInfoComplete = -1;
       }
}
/**
 write GSHBP_UUID_GSH_VALUE 後 透過這裡讀數值
 */
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
    
}
-(void)passwordTimerHandler:(NSTimer *)sender{
    if(!password_reply){
        NSLog(@"Retry send Write password");
        NSData *password = [self stringToByte:passwordString];
        [m_peripheral writeValue:password forCharacteristic:m_characteristic_Write type:CBCharacteristicWriteWithoutResponse];
        m_resend_password_counter++;
        if(m_resend_password_counter >= 10){
            NSString * state = nil;
            state = NSLocalizedString(@"BLE Password response timeout.",nil);
            NSLog(@"BLE Password response timeout.");
        }
    }
}
-(void)bleDidReceiveData:(NSData *) data length:(int) length withUUID:(CBUUID *)uuid {
    NSData *tmpData = [[NSData alloc] initWithData:data];
    NSUInteger len = [tmpData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [tmpData bytes], len);
    
    NSLog(@"bleDidReceiveData %@ UUID:%@", tmpData, uuid);
    
    if([((CBUUID *)uuid) isEqual:[CBUUID UUIDWithString:GSH_822B_BP_COMMAND_UUID]]){
        if(len == 3){
            Byte *byteData = (Byte*)malloc(len);
            memcpy(byteData, [tmpData bytes], len);
            NSData *cmd = [NSData dataWithBytes:&byteData[0] length:1];
            NSData *reply_H = [NSData dataWithBytes:&byteData[1] length:1];
            NSData *reply_L = [NSData dataWithBytes:&byteData[2] length:1];
            
            int intcmd = CFSwapInt16LittleToHost(*(int*)([cmd bytes]));
            int intreply_H = CFSwapInt16LittleToHost(*(int*)([reply_H bytes]));
            int intreply_L = CFSwapInt16LittleToHost(*(int*)([reply_L bytes]));
            
            if(intcmd == 0xCC){
                if(intreply_H == 0x4F && intreply_L == 0x4B){
                    password_reply = true;
                    NSLog(@"Got Password OK Reply");
                    [passwordTimer invalidate];
                    passwordTimer = nil;
                }else{
                    NSLog(@"Got Password Fail Reply");
                }
            }
        }
    }else if([((CBUUID *)uuid) isEqual:[CBUUID UUIDWithString:GSH_822B_BP_INDICATE_UUID]]){
        //NSLog(@"bleDidReceiveData GSH_862B_BP_INDICATE_UUID");
        //最終量測結果, 封包0x1C00FF00C800000050000000.
        if(len == 12){
            uint16_t u16t_SYS = 0;
            uint16_t u16t_DIA = 0;
            uint16_t u16t_HR = 0;
            
            
            
            //SYS
            NSData *countData = [NSData dataWithBytes:&byteData[1] length:1];
            int tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
            
            u16t_SYS = tempdatabyte;
            u16t_SYS = u16t_SYS << 8;
            
            countData = 0;
            tempdatabyte = 0;
            countData = [NSData dataWithBytes:&byteData[2] length:1];
            tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
            u16t_SYS += tempdatabyte;
            
            //DIA
            countData = 0;
            tempdatabyte = 0;
            countData = [NSData dataWithBytes:&byteData[3] length:1];
            tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
            
            u16t_DIA = tempdatabyte;
            u16t_DIA = u16t_DIA << 8;
            
            countData = 0;
            tempdatabyte = 0;
            countData = [NSData dataWithBytes:&byteData[4] length:1];
            tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
            u16t_DIA += tempdatabyte;
            
            //HR
            countData = 0;
            tempdatabyte = 0;
            countData = [NSData dataWithBytes:&byteData[7] length:1];
            tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
            
            u16t_HR = tempdatabyte;
            u16t_HR = u16t_HR << 8;
            
            countData = 0;
            tempdatabyte = 0;
            countData = [NSData dataWithBytes:&byteData[8] length:1];
            tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
            u16t_HR += tempdatabyte;
            
            //_Label_SYS.text = [NSString stringWithFormat:@"%i", u16t_SYS];
            //_Label_DIA.text = [NSString stringWithFormat:@"%i", u16t_DIA];
            //_Label_HR.text = [NSString stringWithFormat:@"%i", u16t_HR];
            
            countData = 0;
            tempdatabyte = 0;
            countData = [NSData dataWithBytes:&byteData[10] length:1];
            tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
            tempdatabyte = tempdatabyte & 0x04;
            if(tempdatabyte == 0x04){
                //UIColor *color = [UIColor redColor];
                //[_Label_Irregular_Heartbeat setTextColor:color];
                //_Label_Irregular_Heartbeat.text = [NSString stringWithFormat:@"Irregular Heartbeat!"];
            }else{
                //UIColor *color = [UIColor blackColor];
                //[_Label_Irregular_Heartbeat setTextColor:color];
                //_Label_Irregular_Heartbeat.text = [NSString stringWithFormat:@"Normal"];
            }
            BPDeviceData *bpDeviceData = [BPDeviceData new];
            bpDeviceData.intSys = [NSString stringWithFormat:@"%i", u16t_SYS].intValue;
            bpDeviceData.intDia = [NSString stringWithFormat:@"%i", u16t_DIA].intValue;
            bpDeviceData.intPulse = [NSString stringWithFormat:@"%i", u16t_HR].intValue;
            bpDeviceData.intAVI = -1;
            bpDeviceData.intAPI = -1;
            if (self.delegate != nil) {
                [self.delegate baseDevice:self resultData:bpDeviceData];
            }
            
        }
    }else if([((CBUUID *)uuid) isEqual:[CBUUID UUIDWithString:GSH_822B_BP_CUFF_UUID]]){
        
        //袖套即時壓力, 封包0x20AABB AA = L byte, BB = H byte.
        NSData *countData = [NSData dataWithBytes:&byteData[1] length:1];
        int tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
        
        uint16_t u16t_SYS = 0;
        u16t_SYS = tempdatabyte;
        u16t_SYS = u16t_SYS << 8;
        
        countData = [NSData dataWithBytes:&byteData[2] length:1];
        tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
        u16t_SYS += tempdatabyte;
        m_ui16t_cuff_data = u16t_SYS;
        

    }
    free(byteData);
}
/**初始化*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = @[GSH_822B_BP_SERVICE_UUID];
        [self initDeviceValue];
    }
    return self;
}
/**設定設備名稱*/
-(void)initDeviceValue
{
    self.deviceName = [NSString stringWithFormat:@"%@",DeviceName_GSH_823B];
    
}

@end
