//
//  BPGSH823B.h
//  DeviceManager
//
//  Created by gsh_mac_2018 on 2019/11/27.
//  Copyright © 2019 GSH. All rights reserved.
//

#import "BPBleDevice.h"

NS_ASSUME_NONNULL_BEGIN
@interface BP_BLE_Data : NSObject {
    
}
@property(nonatomic, assign) uint32_t Time_Total_Sec;    /* Device's time total sec */
@property(nonatomic, assign) int Year;                   /* Device's time Year */
@property(nonatomic, assign) int Month;                  /* Device's time Month */
@property(nonatomic, assign) int Date;                   /* Device's time Date */
@property(nonatomic, assign) int Hour;                   /* Device's time Hour */
@property(nonatomic, assign) int Minute;                 /* Device's time Minute */
@property(nonatomic, assign) int Second;                 /* Device's time Second */
@property(nonatomic, assign) int SYS;                    /* User SYS data 收縮壓 mmHg */
@property(nonatomic, assign) int DIA;                    /* User DIA data 舒張壓 mmHg */
@property(nonatomic, assign) int HR;                     /* User Heart rate data */
@property(nonatomic, assign) int UserID;                 /* User ID */

@end
@interface BPGSH823B : BPBleDevice

@end

NS_ASSUME_NONNULL_END
