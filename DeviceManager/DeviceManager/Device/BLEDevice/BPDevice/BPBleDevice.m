//
//  GSHBPBleDevice.m
//  SoHappy
//
//  Created by elaine on 2017/8/30.
//
//

#import "BPBleDevice.h"

@implementation BPBleDevice
/**繼承後請覆寫*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [NSArray new];
        [self initDeviceValue];
    }
    return self;
}
/**繼承後請覆寫*/
-(void)initDeviceValue
{
    self.deviceName = @"GSHBPBleDevice";
    
}
@end
