//
//  GSHBP822BleDevice.m
//  SoHappy
//
//  Created by elaine on 2017/8/29.
//
//
#import <UIKit/UIKit.h>
#import "BP822BleDevice.h"
#import "BleCommand.h"
#define BP822  @"GSH-BP822"//藍牙設備對應名稱
#define BP822_SERVICE_UUID          @"FFF0"//藍牙設備服務uuid
#define BP822_NOTIFY_UUID           @"FFF4"//回傳的資料透過這個notify
//#define BP822_WRITE_UUID            @"FFF4"沒用到

#define BP822_DIS_UUID              @"180A"//藍牙設備服務uuid
#define BP822_MODEL_UUID            @"2A24"//取得設備基本資訊
#define BP822_SERIAL_NUMBER_UUID    @"2A25"//取得設備serialNumber
#define BP822_MANUFACTURER_ID_UUID  @"2A29"//取得設備manufacturer

@interface BP822BleDevice()
{
    /**
     此設備收到BP822_NOTIFY_UUID 傳入資料後，根據情況需要寫入命令，通知設備進行其他行為，故將相關服務控制暫存已便需要處可以直接呼叫
     */
    CBCharacteristic    *m_characteristic_Write;
}
@end
@implementation BP822BleDevice
/**
 找到設備進行連結並初始化 hr sys dia
 */
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    
}
/**這裡沒有事情需要處理*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    
}
/**這裡沒事情需要處理*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    return true;
}
/**
 根據service 是BP822_SERVICE_UUID or BP822_DIS_UUID 分別做不同處理
 BP822_SERVICE_UUID 跟血壓數值回傳相關
 BP822_DIS_UUID 跟設備基本訊息相關
 */
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    if([service.UUID isEqual:[CBUUID UUIDWithString:BP822_SERVICE_UUID]]){
        DLog(@"Discover GSH_BP822 SERVICE Found");
        for (CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:BP822_NOTIFY_UUID]]){
                if(!aChar.isNotifying){
                    [aPeripheral setNotifyValue:YES forCharacteristic:aChar];
                    DLog(@"Enable 0xFFF4 notify");
                }
                
                m_characteristic_Write = aChar;
            }
            
        }
    }else if([service.UUID isEqual:[CBUUID UUIDWithString:BP822_DIS_UUID]]){
        DLog(@"Discover GSH_BP822 DIS Found");
        for (CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:BP822_MODEL_UUID]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:BP822_SERIAL_NUMBER_UUID]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:BP822_MANUFACTURER_ID_UUID]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }
        }
    }
    else
    {
        for (CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }
            else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            }
        }
    }
}
/**
 收到41 or 200 or 42 or 49 則需要透過m_characteristic_Write響應
 告知設備已經收到這個數值了，不需要再傳送，不然會一直傳
 並在送出響應命令後將收到的數值送到bleDidReceiveData 進一步處理
 */
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:BP822_SERVICE_UUID]]){
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BP822_NOTIFY_UUID]]){
            if(!characteristic.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
            }else{
                NSData *tmpData = [[NSData alloc] initWithData:characteristic.value];
                DLog(@"bleDidReceiveData %@", tmpData);
                NSUInteger len = [tmpData length];
                Byte *byteData = (Byte*)malloc(len);
                memcpy(byteData, [tmpData bytes], len);
                NSData *countData = [NSData dataWithBytes:&byteData[2] length:1];
                int intID = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
                NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
                NSData *ACK;
                switch(intID){
                    case 0x28://ID:40  送袖帶壓力數據
                        break;
                    case 0x29://ID:41  血壓/脈搏數據（需要響應）
                        ACK = [self stringToByte:@"AA0303292F"];
                        [aPeripheral writeValue:ACK
                               forCharacteristic:m_characteristic_Write
                                            type:CBCharacteristicWriteWithResponse];
                        break;
                    case 0xC8://ID:200 血壓/脈搏數據（需要響應）
                        ACK = [self stringToByte:@"AA0303C8CE"];
                        [aPeripheral writeValue:ACK
                               forCharacteristic:m_characteristic_Write
                                            type:CBCharacteristicWriteWithResponse];
                        break;
                    case 0x2A://ID:42  血壓計狀態
                        ACK = [self stringToByte:@"AA03032A30"];
                        [aPeripheral writeValue:ACK
                               forCharacteristic:m_characteristic_Write
                                            type:CBCharacteristicWriteWithResponse];
                        break;
                        
                    case 0x31://ID:49  用戶訊息（需要響應）
                        ACK = [self stringToByte:@"AA03033137"];
                        [aPeripheral writeValue:ACK
                               forCharacteristic:m_characteristic_Write
                                            type:CBCharacteristicWriteWithResponse];
                        break;
                    default:
                        break;
                }
                free(byteData);
                [self bleDidReceiveData:characteristic.value length:(int)characteristic.value.length withUUID:[CBUUID UUIDWithString:BP822_NOTIFY_UUID] withModelName:[aPeripheral.name copy] DeviceID:tmpUUIDstr];
            }
        }
    }else if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:BP822_DIS_UUID]]){
        //DLog(@"DIS Found");
        /* UUID : DIS Read*/
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BP822_MODEL_UUID]]){
            NSString* newStr = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
            DLog(@"Model is %@",newStr);
            //_Label_Model_name.text = [NSString stringWithFormat:@"Model Name:%@", newStr];
            
        }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BP822_SERIAL_NUMBER_UUID]]){
            NSString* newStr = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
            //_Label_MAC.text = [NSString stringWithFormat:@"MAC:%@", newStr];
            DLog(@"MAC is %@",newStr);
            
        }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BP822_MANUFACTURER_ID_UUID]]){
            NSString* newStr = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
            //_Label_Manufacturer_name.text = [NSString stringWithFormat:@"Manufacturer Name:%@", newStr];
            DLog(@"Manufacturer_name is %@",newStr);
        }
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            /*NSString *value = [self NSDataToHexString:characteristic.value];
             value = [self StringConvert:value];
             DLog(@"mac is %@",value);
             self.macAddress = value;*/
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
/**初始化設備*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:BP822_SERVICE_UUID,SERVICE_INFO_DEVICE,nil];
        [self initDeviceValue];
    }
    return self;
}
/**初始化的時候設定名字*/
-(void)initDeviceValue
{
    self.deviceName = BP822;
    
}
/**
 收到得藍牙data 統一在這裡處理
 0x28 送袖帶壓力數據 這裡未處理
 0x29 血壓/脈搏數據（需要響應）連結中量測
 0xC8 血壓/脈搏數據（需要響應）量測完才連結
 0x2A 血壓計狀態
 */
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    NSData *tmpData = [[NSData alloc] initWithData:data];
    NSUInteger len = [tmpData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [tmpData bytes], len);
    NSData *countData = [NSData dataWithBytes:&byteData[2] length:1];
    int intID = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
    
    
    switch(intID){
        case 0x28:      //ID:40  送袖帶壓力數據
            //[self GSH_BP822_ID_0x28_Data:data length:length];
            break;
            
        case 0x29:      //ID:41  血壓/脈搏數據（需要響應）
            //[m_ble disconnectDeviceWithModelName:nil];
            [self GSH_BP822_ID_0x29_Data:data length:length];
            
            break;
            
        case 0xC8:      //ID:200 血壓/脈搏數據（需要響應）
            //[m_ble disconnectDeviceWithModelName:nil];
            [self GSH_BP822_ID_0x29_Data:data length:length];
            
            break;
            
        case 0x2A:      //ID:42  血壓計狀態
            [self GSH_BP822_ID_0x2A_Data:data length:length];
            
            break;
            
        case 0x31:      //ID:49  用戶訊息（需要響應）
            break;
            
        default:
            break;
    }
    free(byteData);
}
//MARK: - 寶萊特 -
/**袖帶壓力數據處理*/
-(void)GSH_BP822_ID_0x28_Data:(NSData *) data length:(int) length{
    
    NSData *tmpData = [[NSData alloc] initWithData:data];
    DLog(@"GSH_BP822_ID_0x28_Data %@", tmpData);
    
    NSUInteger len = [tmpData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [tmpData bytes], len);
    NSData *countData = [NSData dataWithBytes:&byteData[5] length:1];
    int tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
    
    uint16_t u16t_SYS = 0;
    uint8_t u8t_SYS_HBit = tempdatabyte & 0x80;
    
    countData = [NSData dataWithBytes:&byteData[3] length:1];
    tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
    u16t_SYS = tempdatabyte;
    
    if (u8t_SYS_HBit == 0x80){
        u16t_SYS += 0x0100;
    }
    free(byteData);
    
}
/**血壓數據處理*/
-(void)GSH_BP822_ID_0x29_Data:(NSData *) data length:(int) length{
    
    NSData *tmpData = [[NSData alloc] initWithData:data];
    DLog(@"GSH_BP822_ID_0x29_Data %@", tmpData);
    
    NSUInteger len = [tmpData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [tmpData bytes], len);
    NSData *countData = [NSData dataWithBytes:&byteData[3] length:1];
    int tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
    
    uint16_t u16t_SYS = 0;
    uint16_t u16t_DIA = 0;
    uint16_t u16t_PR = 0;
    
    uint8_t u8t_SYS_HBit = tempdatabyte & 0x01;
    uint8_t u8t_DIA_HBit = tempdatabyte & 0x02;
    uint8_t u8t_PR_HBit = tempdatabyte & 0x08;
    
    /* Get SYS */
    countData = [NSData dataWithBytes:&byteData[5] length:1];
    tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
    u16t_SYS = tempdatabyte;
    
    if (u8t_SYS_HBit == 0x01){
        u16t_SYS += 0x0100;
    }
    //_Label_SYS.text = [NSString stringWithFormat:@"%i", u16t_SYS];
    
    /* Get DIA */
    countData = [NSData dataWithBytes:&byteData[6] length:1];
    tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
    u16t_DIA = tempdatabyte;
    
    if (u8t_DIA_HBit == 0x02){
        u16t_DIA += 0x0100;
    }
    //_Label_DIA.text = [NSString stringWithFormat:@"%i", u16t_DIA];
    
    /* Get PR */
    countData = [NSData dataWithBytes:&byteData[8] length:1];
    tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
    u16t_PR = tempdatabyte;
    
    if (u8t_PR_HBit == 0x08){
        u16t_PR += 0x0100;
    }
    //_Label_Pulse.text = [NSString stringWithFormat:@"%i", u16t_PR];
    
    
    /* Check error message */
    countData = [NSData dataWithBytes:&byteData[4] length:1];
    tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
    NSString *errStr;
    
    if(tempdatabyte != 0){
        switch(tempdatabyte){
            case 0x07: // 漏氣
            case 0x0E: // 氣路漏氣
                errStr = [NSString stringWithFormat:@"E01(0x%02x)",tempdatabyte];
                
                break;
                
            case 0x06: // 袖帶鬆
            case 0x14: // 袖帶類型錯
                errStr = [NSString stringWithFormat:@"E02(0x%02x)",tempdatabyte];
                break;
                
            case 0x02: // 自檢失敗
            case 0x08: // 氣壓錯誤
            case 0x0A: // 測量超限
            case 0x0C: // 檢測到過壓
            case 0x0F: // 系統失敗
                errStr = [NSString stringWithFormat:@"E03(0x%02x)",tempdatabyte];
                break;
                
            case 0x0B: // 過多運動
            case 0x0D: // 信號飽和
                errStr = [NSString stringWithFormat:@"E04(0x%02x)",tempdatabyte];
                break;
                
            case 0x13: // 超時
            case 0x09: // 微弱信號
                errStr = [NSString stringWithFormat:@"E05(0x%02x)",tempdatabyte];
                break;
                
            default:
                errStr = [NSString stringWithFormat:@"ERR(0x%02x)",tempdatabyte];
                break;
                
        }
        UIAlertView *errAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"量測錯誤", nil) message:errStr delegate:nil cancelButtonTitle:NSLocalizedString(@"確認",nil) otherButtonTitles:nil, nil];
        [errAlert show];
    }
    else {
        //[self saveBloodPressureDataWithSystolic:u16t_SYS Diastolic:u16t_DIA PluseRate:u16t_PR];
        BPDeviceData *bpDeviceData = [BPDeviceData new];
        bpDeviceData.intPulse = u16t_PR;
        bpDeviceData.intSys = u16t_SYS;
        bpDeviceData.intDia = u16t_DIA;
        bpDeviceData.intAVI = -1;
        bpDeviceData.intAPI = -1;
        if (self.delegate != nil) {
            [self.delegate baseDevice:self resultData:bpDeviceData];
        }
    }
    
    free(byteData);
}
/**血壓計狀態處理*/
-(void)GSH_BP822_ID_0x2A_Data:(NSData *) data length:(int) length{
    
    NSData *tmpData = [[NSData alloc] initWithData:data];
    DLog(@"GSH_BP822_ID_0x2A_Data %@", tmpData);
    
    NSUInteger len = [tmpData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [tmpData bytes], len);
    NSData *countData = [NSData dataWithBytes:&byteData[3] length:1];
    int tempdatabyte = CFSwapInt16LittleToHost(*(int*)([countData bytes]));
    int battery = tempdatabyte & 0x0F;
    DLog(@"batter is %d",battery);
    
    free(byteData);
    
}
@end
