//
//  GSHBPBleDevice.m
//  SoHappy
//
//  Created by elaine on 2017/8/29.
//
//
#define DeviceName_ZoeS2            @"ZoeS2"//設備對應的名稱

#define ZoeS2_SERVICE_UUID          @"6E400001-B5A3-F393-E0A9-E50E24DCCA9E"//設備服務的UUID
#define ZoeS2_BP822_NOTIFY_UUID     @"6E400003-B5A3-F393-E0A9-E50E24DCCA9E"//設備取得數值的notify
#define ZoeS2_BP822_WRITE_UUID      @"6E400002-B5A3-F393-E0A9-E50E24DCCA9E"//目前沒用

#import "BPZOES2BleDevice.h"
#import "BleCommand.h"
@interface BPZOES2BleDevice()
{
    /**ZoeS2_BP822_WRITE_UUID 對應的characteristic 目前無確定作用*/
    CBCharacteristic    *m_characteristic_Write;
    /**i_Hr i_Sys i_Dia 三個都有數值 才會做後續處理*/
    int i_Hr;
    /**i_Hr i_Sys i_Dia 三個都有數值 才會做後續處理*/
    int i_Sys;
    /**i_Hr i_Sys i_Dia 三個都有數值 才會做後續處理*/
    int i_Dia;
}
@end
@implementation BPZOES2BleDevice
/**初始化相關數值*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    i_Hr = -1;
    i_Sys = -1;
    i_Dia = -1;

}
/**這裡沒做任何處理*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    
}
/**這裡沒做任何處理*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    return true;
}
/**針對設備上有提供的服務做相對應的處理 read write or setNotyfy*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    if([service.UUID isEqual:[CBUUID UUIDWithString:ZoeS2_SERVICE_UUID]]){
        DLog(@"Discover ZoeS2 SERVICE Found");
        for (CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:ZoeS2_BP822_NOTIFY_UUID]]){
                if(!aChar.isNotifying){
                    [aPeripheral setNotifyValue:YES forCharacteristic:aChar];
                    DLog(@"Enable notify");
                }
                
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:ZoeS2_BP822_WRITE_UUID]]){
                
                m_characteristic_Write = aChar;
            }
        }
    }
    else
    {
        for(CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }
            else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
                [aPeripheral readValueForCharacteristic:aChar];
            }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
                [aPeripheral readValueForCharacteristic:aChar];
            }
        }
    }
}
/**收到數值後丟到bleDidReceiveData 進行後續處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:ZoeS2_BP822_NOTIFY_UUID]]){
        if(!characteristic.isNotifying){
            [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
        }else{
            [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length) withUUID:[CBUUID UUIDWithString:ZoeS2_BP822_NOTIFY_UUID]];
        }
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            /*NSString *value = [self NSDataToHexString:characteristic.value];
             value = [self StringConvert:value];
             DLog(@"mac is %@",value);
             self.macAddress = value;*/
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
//MARK: - 子物件 自己的功能 -
/**初始化*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:ZoeS2_SERVICE_UUID,SERVICE_INFO_DEVICE,nil];
        [self initDeviceValue];
    }
    return self;
}
/**設定設備名稱*/
-(void)initDeviceValue
{
    self.deviceName = DeviceName_ZoeS2;

}
/**藍牙回傳的資料處理*/
-(void)bleDidReceiveData:(NSData *) data length:(int) length withUUID:(CBUUID *)uuid {
    
    NSData *tmpData = [[NSData alloc] initWithData:data];
    //DLog(@"bleDidReceiveData %@", tmpData);
    
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    NSUInteger len = [tmpData length];
    uint8_t *bytes = (uint8_t *)[tmpData bytes];
    for (NSUInteger i = 0; i < len; i++) {
        [temp addObject:[NSString stringWithFormat:@"%d",bytes[i]]];
    }
    
    if(len == 12){
        // 0d81455f 48523d30 38352cc5  HR        12byte
        if([[temp objectAtIndex:2] intValue] == 0x45 &&
           [[temp objectAtIndex:3] intValue] == 0x5F &&
           [[temp objectAtIndex:4] intValue] == 0x48 &&
           [[temp objectAtIndex:5] intValue] == 0x52){
            
            int HRData = 0;
            HRData += ([[temp objectAtIndex:7] intValue]-0x30) *100;
            HRData += ([[temp objectAtIndex:8] intValue]-0x30) *10;
            HRData += [[temp objectAtIndex:9] intValue]-0x30;
            
            i_Hr = HRData;
            //_Label_Pulse.text = [NSString stringWithFormat:@"%d",HRData];
            //DLog(@"bleDidReceiveData %d", HRData);
        }
        
    }else if(len == 11){
        // 01815359 533d3133 312c7e    SBP(SYS)  11byte
        if([[temp objectAtIndex:2] intValue] == 0x53 &&
           [[temp objectAtIndex:3] intValue] == 0x59 &&
           [[temp objectAtIndex:4] intValue] == 0x53){
            
            int SYSData = 0;
            SYSData += ([[temp objectAtIndex:6] intValue]-0x30) *100;
            SYSData += ([[temp objectAtIndex:7] intValue]-0x30) *10;
            SYSData += [[temp objectAtIndex:8] intValue]-0x30;
            i_Sys = SYSData;
            //_Label_SYS.text = [NSString stringWithFormat:@"%d",SYSData];
            //DLog(@"bleDidReceiveData %d", HRData);
            
            // 01814449 413d3037 382c57    DBP(DIA)  11byte
        }else if([[temp objectAtIndex:2] intValue] == 0x44 &&
                 [[temp objectAtIndex:3] intValue] == 0x49 &&
                 [[temp objectAtIndex:4] intValue] == 0x41){
            
            int DIAData = 0;
            DIAData += ([[temp objectAtIndex:6] intValue]-0x30) *100;
            DIAData += ([[temp objectAtIndex:7] intValue]-0x30) *10;
            DIAData += [[temp objectAtIndex:8] intValue]-0x30;
            i_Dia = DIAData;
            //_Label_DIA.text = [NSString stringWithFormat:@"%d",DIAData];
            //DLog(@"bleDidReceiveData %d", HRData);
        }
        
    }
    if (i_Hr != -1 && i_Sys != -1 && i_Dia != -1) {
        BPDeviceData *bpDeviceData = [BPDeviceData new];
        bpDeviceData.intPulse = i_Hr;
        bpDeviceData.intSys = i_Sys;
        bpDeviceData.intDia = i_Dia;
        bpDeviceData.intAVI = -1;
        bpDeviceData.intAPI = -1;
        if (self.delegate != nil) {
            [self.delegate baseDevice:self resultData:bpDeviceData];
        }
        i_Hr = -1;
        i_Dia = -1;
        i_Sys = -1;
    }
}

@end
