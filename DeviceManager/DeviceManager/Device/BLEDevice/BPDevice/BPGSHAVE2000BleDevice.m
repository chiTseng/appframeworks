//
//  BPGSHAVE2000BleDevice.m
//  DeviceManager
//
//  Created by Yu Chi on 2018/5/7.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "BPGSHAVE2000BleDevice.h"
#import "BleCommand.h"
#define DeviceName_GSH_AVE2000            @"AVE-2000"
#define GSH_AVE2000_SERVICE_UUID          @"2E514660-30F4-11E5-9320-0002A5D5C51B"

#define GSH_AVE2000_DATA_Indicate_UUID    @"49A66D80-30F6-11E5-8BA8-0002A5D5C51B"
#define GSH_AVE2000_Current_Time_UUID     @"6E817700-30F4-11E5-9EEC-0002A5D5C51B"
#define GSH_AVE2000_Filter_Time_UUID      @"CCC9B940-E6A6-11E5-9EFD-0002A5D5C51B"
@interface BPGSHAVE2000BleDevice()
{
    CBPeripheral*m_peripheral;
    CBCharacteristic * m_characteristic_Data_Notify;
    CBCharacteristic * m_characteristic_filter_Write;
    CBCharacteristic * m_characteristic_Time_WriteRead;
    NSTimer * enable_indicate_Timer;
    
    NSString *_strDeviceDate;
    NSString *_strOSDate;
}
@end
@implementation BPGSHAVE2000BleDevice
/**掃到設備，準備與設備連線前可以做的事情*/
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    DLog(@"請實作didDiscoverPeripheralHandler");
    m_peripheral = nil;
    if (enable_indicate_Timer != nil) {
        [enable_indicate_Timer invalidate];
        enable_indicate_Timer = nil;
    }
    m_characteristic_Data_Notify = nil;
    m_characteristic_filter_Write = nil;
}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
   
    m_peripheral = aPeripheral;
    _strDeviceDate = @"";
    _strOSDate = @"";
}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    m_peripheral = nil;
    if (enable_indicate_Timer != nil) {
        [enable_indicate_Timer invalidate];
        enable_indicate_Timer = nil;
    }
    m_characteristic_Data_Notify = nil;
    m_characteristic_filter_Write = nil;
    return true;
}
/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {
        
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    for (CBCharacteristic *aChar in service.characteristics)
    {
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_AVE2000_DATA_Indicate_UUID]]){
            m_characteristic_Data_Notify = aChar;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_AVE2000_Current_Time_UUID]]){
            m_characteristic_Time_WriteRead = aChar;
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:GSH_AVE2000_Filter_Time_UUID]]){
            m_characteristic_filter_Write = aChar;
        }
        else  if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        }
        
        DLog(@"BLELog:UUID found %@", aChar.UUID);
    }
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_AVE2000_DATA_Indicate_UUID]]){
        NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
        [self bleDidReceiveData:characteristic.value length:(int)(characteristic.value.length)
                       withUUID:[CBUUID UUIDWithString:GSH_AVE2000_DATA_Indicate_UUID] withModelName:tmpUUIDstr DeviceID:[aPeripheral.name copy]];
    }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_AVE2000_Current_Time_UUID]]){
        //DLog(@"BLELog:bleDidReceiveData %@", characteristic.value);
        NSData *tmpData = [[NSData alloc] initWithData:characteristic.value];
        
        NSUInteger len = [tmpData length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [tmpData bytes], len);
        
        //取得當前時間
        NSDate *now = [NSDate date];
        uint16_t year = byteData[1];
        year = year << 8;
        year = year + byteData[0];
        NSString *datefromble = [NSString stringWithFormat:@"%04d-%02d-%02d %02d:%02d:%02d", year,byteData[2],byteData[3],byteData[4],byteData[5],byteData[6]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSCalendar* _cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        [dateFormatter setCalendar:_cal];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        _strDeviceDate = datefromble;
        _strOSDate = [dateFormatter stringFromDate:now];
        
        NSDate *deviceDate = [dateFormatter dateFromString:datefromble];
        //DLog(@"Time from BLE:%@", now);
        //判斷是否要校正時間
        double checkTime = now.timeIntervalSince1970 - deviceDate.timeIntervalSince1970;
        NSLog(@"%f",now.timeIntervalSince1970);
        NSLog(@"%f",deviceDate.timeIntervalSince1970);
        NSLog(@"%f",checkTime);
        if (checkTime > 180 || checkTime < 0) {
            NSLog(@"時間不對需要校正時間");
            [self setTime];
            return;
        }
        
        //取得日曆元件
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        //設定NSDateComponents 用於增減日期
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        
        //扣3min, 只取最後三分鐘內量測到的資料
        [dateComponents setMinute:-3];
        
        //將當前時間透過NSDateComponents來做增減
        deviceDate = [calendar dateByAddingComponents:dateComponents toDate:deviceDate options:0];
        
        //設定NSDateComponents 要取出Year Month Day Weekday，從now之中取出
        NSDateComponents *components = [calendar components:
                                        NSCalendarUnitYear  |
                                        NSCalendarUnitMonth |
                                        NSCalendarUnitDay   |
                                        NSCalendarUnitWeekday |
                                        NSCalendarUnitHour    |
                                        NSCalendarUnitMinute  |
                                        NSCalendarUnitSecond
                                                   fromDate:deviceDate];
        
        //DLog(@"Time -5 :%@", now);
        year = [components year];
        //DLog(@"Time Year :%hu", year);
        byteData[1] = year >> 8;
        byteData[0] = year & 0x00FF;
        byteData[2] = [components month];
        byteData[3] = [components day];
        byteData[4] = [components hour];
        byteData[5] = [components minute];
        byteData[6] = [components second];
        
        //寫入指令時間的filter.
        NSData *writedata = [NSData dataWithBytes:byteData length:sizeof(byteData)];
        [aPeripheral writeValue:writedata forCharacteristic:m_characteristic_filter_Write type:CBCharacteristicWriteWithResponse];
        
        free(byteData);
        
        //等0.5秒後開啟接收資料的Indicate.
        enable_indicate_Timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self
                                                               selector:@selector(enableIndicateTimerHandler:) userInfo:nil repeats:NO];
        
        
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            /*NSString *value = [self NSDataToHexString:characteristic.value];
             value = [self StringConvert:value];
             DLog(@"mac is %@",value);
             self.macAddress = value;*/
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
/**對藍牙設備寫入一個命令成功後會回傳到這裡，並讓個別設備決定要如何處理*/
-(void)didWriteValueForCharacteristic:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didWriteValueForCharacteristic");
    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GSH_AVE2000_Current_Time_UUID]]){
        BPDeviceData *bpDeviceData = [BPDeviceData new];
        bpDeviceData.strError = @"TimeError";
        bpDeviceData.strOSDate = _strOSDate;
        bpDeviceData.strDeviceDate = _strDeviceDate;
        if (self.delegate != nil) {
            [self.delegate baseDevice:self resultData:bpDeviceData];
        }
    }
    
}
-(void)didUpdateNotificationStateForCharacteristic:(CBPeripheral *)aPeripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"請實作didUpdateNotificationStateForCharacteristic");
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:GSH_AVE2000_SERVICE_UUID,SERVICE_INFO_DEVICE,nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = [NSString stringWithFormat:@"%@",DeviceName_GSH_AVE2000];
    
}
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    NSData *tmpData = [[NSData alloc] initWithData:data];
    DLog(@"BLELog:bleDidReceiveData %@", tmpData);
    
    if([((CBUUID *)uuid) isEqual:[CBUUID UUIDWithString:GSH_AVE2000_DATA_Indicate_UUID]]){
        int UserID =0;
        int Flag = 0;
        int SYS = 0;
        int DIA = 0;
        int HR = 0;
        
        int AVI = 0;
        int API = 0 ;
        
        NSUInteger len = [tmpData length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [tmpData bytes], len);
        
        UserID = byteData[0];
        Flag = byteData[1];
        
        uint16_t mixdata = 0;
        mixdata = byteData[3];
        mixdata = mixdata << 8;
        mixdata = mixdata + byteData[2];
        SYS = mixdata;
        
        mixdata = 0;
        mixdata = byteData[5];
        mixdata = mixdata << 8;
        mixdata = mixdata + byteData[4];
        DIA = mixdata;
        
        HR = byteData[6];
        
        mixdata = 0;
        mixdata = byteData[8];
        mixdata = mixdata << 8;
        mixdata = mixdata + byteData[7];
        AVI = mixdata/10;
        
        mixdata = 0;
        mixdata = byteData[10];
        mixdata = mixdata << 8;
        mixdata = mixdata + byteData[9];
        API = mixdata/10;
        
        /* 取得量測的時間 */
        uint32_t time4byte = 0;
        time4byte = byteData[14] << 24;
        time4byte = time4byte + (byteData[13] << 16);
        time4byte = time4byte + (byteData[12] << 8);
        time4byte = time4byte + byteData[11];
        
        //儀器的量測時間是4byte的秒數, 從2000-1-1 0:0:0開始算.
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDate *now = [NSDate date];
        NSString *datefromble = [NSString stringWithFormat:@"2000-01-01 00:00:00"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [dateFormatter setCalendar:calendar];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        now = [dateFormatter dateFromString:datefromble];
        
        //取得日曆元件
        
        //設定NSDateComponents 用於增減日期
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        
        //加上量測的秒數.
        [dateComponents setSecond:time4byte];
        
        //將當前時間透過NSDateComponents來做增減
        now = [calendar dateByAddingComponents:dateComponents toDate:now options:0];
        
        //設定NSDateComponents 要取出Year Month Day Weekday，從now之中取出
        NSDateComponents *components = [calendar components:
                                        NSCalendarUnitYear  |
                                        NSCalendarUnitMonth |
                                        NSCalendarUnitDay   |
                                        NSCalendarUnitWeekday |
                                        NSCalendarUnitHour    |
                                        NSCalendarUnitMinute  |
                                        NSCalendarUnitSecond
                                                   fromDate:now];
        
        DLog(@"Measurement Data time :%@", now);
        int Year = 0,Month=0,Date=0,Hour=0,Minute=0,Second=0,Temp=0;
        Year = (int)[components year];
        Month = (int)[components month];
        Date = (int)[components day];
        Hour = (int)[components hour];
        Minute = (int)[components minute];
        Second = (int)[components second];
        Temp = byteData[15];
        NSDate *osDate = [dateFormatter dateFromString:_strOSDate];
        double checkTime = osDate.timeIntervalSince1970 - now.timeIntervalSince1970;

        if (checkTime < 180 && checkTime > 0) {
            BPDeviceData *bpDeviceData = [BPDeviceData new];
            bpDeviceData.intPulse = HR;
            bpDeviceData.intSys = SYS;
            bpDeviceData.intDia = DIA;
            bpDeviceData.intAPI = API;
            bpDeviceData.intAVI = AVI;
            if (self.delegate != nil) {
                [self.delegate baseDevice:self resultData:bpDeviceData];
            }
        }
        else
        {//上傳的資料的時間 比平板時間快 or 上傳資料的時間與平板時間比超過三分鐘
            
        }
        
        
        
        free(byteData);
    }

}
-(void)enableIndicateTimerHandler:(NSTimer *)sender{
    DLog(@"enableIndicateTimerHandler");
    if(!m_characteristic_Data_Notify.isNotifying){
        [m_peripheral setNotifyValue:YES forCharacteristic:m_characteristic_Data_Notify];
        DLog(@"BLELog:Enable notify %@", m_characteristic_Data_Notify.UUID);
    }
}
-(void)setTime
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [outputFormatter setCalendar:calendar];
    [outputFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    NSLog(@"newDateString %@", newDateString);
    //取得日曆元件
    
    //設定NSDateComponents 要取出Year Month Day Weekday，從now之中取出
    NSDateComponents *components = [calendar components:
                                    NSCalendarUnitYear  |
                                    NSCalendarUnitMonth |
                                    NSCalendarUnitDay   |
                                    NSCalendarUnitWeekday |
                                    NSCalendarUnitHour    |
                                    NSCalendarUnitMinute  |
                                    NSCalendarUnitSecond
                                               fromDate:now];
    NSLog(@"Measurement Data time :%@", now);
    Byte *byteData = (Byte*)malloc(7);
    uint16_t n16year = (int)[components year];
    byteData[0] = n16year & 0xFF;
    byteData[1] = n16year >> 8;
    byteData[2] = (int)[components month];
    byteData[3] = (int)[components day];
    byteData[4] = (int)[components hour];
    byteData[5] = (int)[components minute];
    byteData[6] = (int)[components second];
    NSData *writedata = [NSData dataWithBytes:byteData length:sizeof(byteData)];
    [m_peripheral writeValue:writedata forCharacteristic:m_characteristic_Time_WriteRead type:CBCharacteristicWriteWithResponse];


    free(byteData);
}
@end
