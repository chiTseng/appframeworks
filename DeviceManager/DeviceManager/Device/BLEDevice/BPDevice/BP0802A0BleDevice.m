//
//  GSHBP0802A0BleDevice.m
//  SoHappy
//
//  Created by elaine on 2017/8/29.
//
//

#import "BP0802A0BleDevice.h"
#import "BleCommand.h"
#define BP_0802A0 @"0802A0"//血壓設備的名稱
#define BP_SERVICE_0802A0 @"1810"//設備的服務uuid
#define BP_0802A0UUID_NOTIFY @"2A35"//主要是設定notify 來去得藍牙設備回傳的血壓數值 length 10 是量完後的最後數值，length 19帶有量測中的量測帶壓力
#define BP_0802A0UUID_NOTIFY2 @"2A36"//因為是直接複製過來，所以不清楚這個命令是做什麼的//應該也是沒用到
//#define BP_UUID_SCALES_NOTIFY @"2A35"//沒用到

@interface BP0802A0BleDevice()
@end

@implementation BP0802A0BleDevice
/**
 初始化 hr sys dia 數值
 
 */
-(void)didDiscoverPeripheralHandler:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    
}
/**藍牙管理器跟設備已連線，準備尋找設備提供服務前，可以做的事情*/
-(void)didConnectPeripheralHandler:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    
}
/**藍牙管理器跟設備已斷線，可以做的事情*/
-(BOOL)didDisconnectPeripheral:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    return true;
}
/**藍牙管理器回覆設備上可以使用的服務的命令，並讓個別設備決定如何處理*/
-(void)didDiscoverCharacteristicsForService:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *aChar in service.characteristics)
    {

        if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            self.readSystemInfoStatus += SUPPORT_SYSTEMID;
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            self.readSystemInfoStatus += SUPPORT_SERIALNUMBER;
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_HARDWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_FIRMWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoStatus += SUPPORT_SOFTWAREVERSION;
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
           self.readSystemInfoStatus += SUPPORT_MANUFACTURENAME;
        }
    }
    for (CBCharacteristic *aChar in service.characteristics)
    {
        DLog(@"readValueForCharacteristic: %@", [aChar.UUID data]);
        if([aChar.UUID isEqual:[CBUUID UUIDWithString:BP_0802A0UUID_NOTIFY]]){//To get Notify value
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:BP_0802A0UUID_NOTIFY2]]){//To open BP indicator
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }
        else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]){
            [aPeripheral readValueForCharacteristic:aChar];
        }else if([aChar.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        } else if([aChar.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            [aPeripheral readValueForCharacteristic:aChar];
        }
    }
}
/**藍牙管理器收到藍牙設備的資料回復，並讓個別設備決定如何處理*/
-(void)didUpdateValueForCharacteristic:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:BP_SERVICE_0802A0]]){
        
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BP_0802A0UUID_NOTIFY]]){
            if(!characteristic.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
            }else{
                NSString *tmpUUIDstr = [NSString stringWithFormat:@"%@",aPeripheral.identifier.UUIDString];
                [self bleDidReceiveData:characteristic.value length:(int)characteristic.value.length withUUID:[CBUUID UUIDWithString:BP_0802A0UUID_NOTIFY] withModelName:[aPeripheral.name copy] DeviceID:tmpUUIDstr]; //@2015/09/17
            }
        }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:BP_0802A0UUID_NOTIFY2]]){
            if(!characteristic.isNotifying){
                [aPeripheral setNotifyValue:YES forCharacteristic:characteristic];
            }
        }
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_INFO_DEVICE]])
    {
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SYSTEMID]]) {
            self.readSystemInfoComplete += SUPPORT_SYSTEMID;
            /*NSString *value = [self NSDataToHexString:characteristic.value];
            value = [self StringConvert:value];
            DLog(@"mac is %@",value);
            self.macAddress = value;*/
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SERIAL_NUMBER]]) {
            self.readSystemInfoComplete += SUPPORT_SERIALNUMBER;
            self.macAddress = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:MANUFACTURER_NAME]]) {
            self.readSystemInfoComplete += SUPPORT_MANUFACTURENAME;
            self.manufacturerName = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"MANUFACTURER_NAME is %@",self.manufacturerName);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:FIRMWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_FIRMWAREVERSION;
            self.firmwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"firmwareVersion is %@",self.firmwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:SOFTWARE_VERSION]]) {
            self.readSystemInfoComplete += SUPPORT_SOFTWAREVERSION;
            self.softwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"softwareVersion is %@",self.softwareVersion);
        }
        else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:HARDWARE_VERSION]]){
            self.readSystemInfoComplete += SUPPORT_HARDWAREVERSION;
            self.hardwareVersion = [[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
            DLog(@"hardwareVersion is %@",self.hardwareVersion);
        }
    }
    if (self.readSystemInfoComplete != -1 && (self.readSystemInfoComplete == self.readSystemInfoStatus)) {
        if (self.delegate != nil) {
            [self.delegate baseDevice:self];
        }
        self.readSystemInfoStatus = -1;
        self.readSystemInfoComplete = -1;
    }
}
-(instancetype)init
{
    self = [super init];
    if (self) {
        self.uuids = [[NSArray alloc] initWithObjects:BP_SERVICE_0802A0,SERVICE_INFO_DEVICE,nil];
        [self initDeviceValue];
    }
    return self;
}
-(void)initDeviceValue
{
    self.deviceName = BP_0802A0;
    
}
-(void)bleDidReceiveData:(NSData *)data length:(int)length withUUID:(CBUUID *)uuid withModelName:(NSString *)modelName DeviceID:(NSString *)deviceID
{
    if(length==10){//BP final value
        NSUInteger len = [data length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [data bytes], len);
        NSData *SystolicData = [NSData dataWithBytes:&byteData[1] length:2];
        NSData *DiastolicData = [NSData dataWithBytes:&byteData[3] length:2];
        NSData *PulseData = [NSData dataWithBytes:&byteData[7] length:2];
        
        int value1 = CFSwapInt16BigToHost(*(int*)([SystolicData bytes]));
        int value2 = CFSwapInt16BigToHost(*(int*)([DiastolicData bytes]));
        int value3 = CFSwapInt16BigToHost(*(int*)([PulseData bytes]));
        free(byteData);
        DLog(@"BP final value : \nSystolic: %d mmHg, Diastolic: %d mmHg, Pulse : %d Pulse/min",value1,value2,value3);
        BPDeviceData *bpDeviceData = [BPDeviceData new];
        bpDeviceData.intSys = value1;
        bpDeviceData.intDia = value2;
        bpDeviceData.intPulse = value3;
        bpDeviceData.intAVI = -1;
        bpDeviceData.intAPI = -1;
        if (self.delegate != nil) {
            [self.delegate baseDevice:self resultData:bpDeviceData];
        }
    }
    else if(length==19){//BP Transtek
        NSUInteger len = [data length];
        Byte *byteData = (Byte*)malloc(len);
        memcpy(byteData, [data bytes], len);
        NSData *SystolicData = [NSData dataWithBytes:&byteData[1] length:2];
        NSData *DiastolicData = [NSData dataWithBytes:&byteData[3] length:2];
        NSData *PulseData = [NSData dataWithBytes:&byteData[14] length:2];
        
        int value1 = CFSwapInt16LittleToHost(*(int*)([SystolicData bytes]));
        int value2 = CFSwapInt16LittleToHost(*(int*)([DiastolicData bytes]));
        int value3 = CFSwapInt16LittleToHost(*(int*)([PulseData bytes]));
        free(byteData);
        DLog(@"BP final value : \nSystolic: %d mmHg, Diastolic: %d mmHg, Pulse : %d Pulse/min",value1,value2,value3);
        //save to tmp
        BPDeviceData *bpDeviceData = [BPDeviceData new];
        bpDeviceData.intSys = value1;
        bpDeviceData.intDia = value2;
        bpDeviceData.intPulse = value3;
        if (self.delegate != nil) {
            [self.delegate baseDevice:self resultData:bpDeviceData];
        }
    }
}
@end
