//
//  BaseDevice.m
//  SoHappy
//
//  Created by elaine on 2017/8/24.
//
//

#import "BaseDevice.h"
@interface BaseDevice()
{
    __unsafe_unretained id delegate;
    NSString *deviceName;
    NSString *macAddress;
}
@end
@implementation BaseDevice
@synthesize deviceName,macAddress,delegate;
@end
