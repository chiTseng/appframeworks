//
//  GSHBP822BleDevice.h
//  SoHappy
//
//  Created by elaine on 2017/8/29.
//
//

#import "BPBleDevice.h"
/**
 支援GSH-BP822 寶萊特血壓計
 連線到設備量測完後
 會將相關血壓數值寫入 self.hr  self.sys  self.dia
 */
@interface BP822BleDevice : BPBleDevice


@end
