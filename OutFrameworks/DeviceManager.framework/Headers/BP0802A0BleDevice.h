//
//  GSHBP0802A0BleDevice.h
//  SoHappy
//
//  Created by elaine on 2017/8/29.
//
//

#import "BPBleDevice.h"

/**
 支援0802A0 血壓計
 連線到設備量測完後
 會將相關血壓數值寫入 self.hr  self.sys  self.dia 
 */
@interface BP0802A0BleDevice : BPBleDevice

@end
