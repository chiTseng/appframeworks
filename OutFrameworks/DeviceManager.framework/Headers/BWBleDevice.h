//
//  BWBleDevice.h
//  SoHappy
//
//  Created by Yu Chi on 2017/9/2.
//
//
#import "BaseBLEDevice.h"
#import "DeviceDatas.h"
//透過SERIAL_NUMBER 取得mac
/**
 支援體重機的基本類型  預設 0202B 00000000 or 0102B 00000000 or 0202B-0001 三款
 */
@interface BWBleDevice : BaseBLEDevice
@end

