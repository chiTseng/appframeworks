//
//  BWBC418Device.h
//  DeviceManager
//
//  Created by Yu Chi on 2018/5/2.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "BaseBLEDevice.h"
#import "DeviceDatas.h"
@interface GSH_BC418_BLE_Data : NSObject {
    
}
@property(nonatomic, assign) int Year;                   /* Device's time Year */
@property(nonatomic, assign) int Month;                  /* Device's time Month */
@property(nonatomic, assign) int Date;                   /* Device's time Date */
@property(nonatomic, assign) int Hour;                   /* Device's time Hour */
@property(nonatomic, assign) int Minute;                 /* Device's time Minute */
@property(nonatomic, assign) int Body_type;              /* User body type 0 = standard / 2 = athlete */
@property(nonatomic, assign) int Gender;                 /* User gender 1 = male / 2 = female */
@property(nonatomic, assign) int Height;                 /* User height xxxxx(cm) */
@property(nonatomic, assign) float Weight;               /* User weight xxx.x(kg) */
@property(nonatomic, assign) float Body_fat_percentage;  /* User body fat percentage xx.x(%) */
@property(nonatomic, assign) float Fat_mass;             /* User fat mass xxx.x(kg) */
@property(nonatomic, assign) float Fat_free_mass;        /* User fat free mass xxx.x(kg) */
@property(nonatomic, assign) float Body_water_mass;      /* User body water mass xxx.x(kg) */
@property(nonatomic, assign) int Age;                    /* User Age 0~99 */
@property(nonatomic, assign) float BMI;                  /* User BMI xxx.x */
@property(nonatomic, assign) int BMR;                    /* User BMR xxxxx(kJ) */
@property(nonatomic, assign) int Whole_body_impedance;   /* User whole body impedance xxx(Ω) */
@property(nonatomic, assign) int Right_leg_impedance;    /* User right leg impedance xxx(Ω) */
@property(nonatomic, assign) int Left_leg_impedance;     /* User left leg impedance xxx(Ω) */
@property(nonatomic, assign) int Right_arm_impedance;    /* User right leg impedance xxx(Ω) */
@property(nonatomic, assign) int Left_arm_impedance;     /* User left leg impedance xxx(Ω) */
@property(nonatomic, assign) float Right_leg_body_fat_percentage;     /* User right leg body fat percentage xx.x(%) */
@property(nonatomic, assign) float Right_leg_fat_mass;                /* User right leg fat mass xxx.x(kg) */
@property(nonatomic, assign) float Right_leg_fat_free_mass;           /* User right leg fat free mass xxx.x(kg) */
@property(nonatomic, assign) float Right_leg_predicted_muscle_mass;   /* User right leg predicted muscle mass xxx.x(kg) */
@property(nonatomic, assign) float Left_leg_body_fat_percentage;      /* User left leg body fat percentage xx.x(%) */
@property(nonatomic, assign) float Left_leg_fat_mass;                 /* User left leg fat mass xxx.x(kg) */
@property(nonatomic, assign) float Left_leg_fat_free_mass;            /* User left leg fat free mass xxx.x(kg) */
@property(nonatomic, assign) float Left_leg_predicted_muscle_mass;    /* User left leg predicted muscle mass xxx.x(kg) */
@property(nonatomic, assign) float Right_arm_body_fat_percentage;     /* User right arm body fat percentage xx.x(%) */
@property(nonatomic, assign) float Right_arm_fat_mass;                /* User right arm fat mass xxx.x(kg) */
@property(nonatomic, assign) float Right_arm_fat_free_mass;           /* User right arm fat free mass xxx.x(kg) */
@property(nonatomic, assign) float Right_arm_predicted_muscle_mass;   /* User right arm predicted muscle mass xxx.x(kg) */
@property(nonatomic, assign) float Left_arm_body_fat_percentage;      /* User left arm body fat percentage xx.x(%) */
@property(nonatomic, assign) float Left_arm_fat_mass;                 /* User left arm fat mass xxx.x(kg) */
@property(nonatomic, assign) float Left_arm_fat_free_mass;            /* User left arm fat free mass xxx.x(kg) */
@property(nonatomic, assign) float Left_arm_predicted_muscle_mass;    /* User left arm predicted muscle mass xxx.x(kg) */
@property(nonatomic, assign) float Trunk_body_fat_percentage;         /* User trunk body fat percentage xx.x(%) */
@property(nonatomic, assign) float Trunk_fat_mass;                    /* User trunk fat mass xxx.x(kg) */
@property(nonatomic, assign) float Trunk_fat_free_mass;               /* User trunk fat free mass xxx.x(kg) */
@property(nonatomic, assign) float Trunk_predicted_muscle_mass;       /* User trunk predicted muscle mass xxx.x(kg) */

@end
/**
 隧道式血壓機整合
 */
@interface BWBC418Device : BaseBLEDevice
@property (nonatomic,retain) BWBC41DeviceData *bwgDeviceData;
@end
