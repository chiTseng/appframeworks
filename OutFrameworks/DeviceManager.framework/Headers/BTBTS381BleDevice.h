//
//  BTBTS381BleDevice.h
//  DeviceManager
//
//  Created by elaine on 2018/12/11.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import <DeviceManager/DeviceManager.h>

@interface BTBTS381BleDevice : BaseBLEDevice
/**
 體溫量測的單位
 */
@property (assign) int measureUnit;
@end
