//
//  ECGBleDevice.h
//  SoHappy
//
//  Created by elaine on 2017/9/20.
//
//
//#define ECG512BIT16//這個如果移除則預設會是256BIT8
#import "BaseBLEDevice.h"
#import "DeviceDatas.h"
#define ECGDATATYPE512B16 @"fcff10"
#define ECGDATATYPE256B8 @"f18008"
/**支援ECG設備*/
@interface ECGBleDevice : BaseBLEDevice
@property (retain,nonatomic) NSString * ecgDataType;
@end
