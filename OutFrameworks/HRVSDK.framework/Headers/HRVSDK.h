//
//  HRVSDK.h
//  HRVSDK
//
//  Created by Smart Wang on 10/01/2018.
//  Copyright © 2018 Unisage. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HRVSDK.
FOUNDATION_EXPORT double HRVSDKVersionNumber;

//! Project version string for HRVSDK.
FOUNDATION_EXPORT const unsigned char HRVSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HRVSDK/PublicHeader.h>

#import <HRVSDK/NSData+MyExtensions.h>
