#import <Foundation/Foundation.h>

#define kAES256CryptoErrorDomain @"AES256CryptoErrorDomain"
#define kAES256CryptoUserInfoErrorStatus @"CCCryptorStatus"

@interface NSData (MyExtensions)

/**
 * Method to AES256 encrypt the data with the specified key and initialization vector.
 *
 * @param key The key to use to encrypt the data - only up to the first kCCKeySizeAES256 characters are used.
 * @param iv The initialization vector, if any, to use for randomizing the encrypted result so no repeatable patterns can be detected.  The initialization vector should be up to kCCKeySizeAES128 in length.  If nil, no initialization vector is used.
 *
 * @return The encrypted data if successful, otherwise nil.
 *
 * @note This method is intended for use with small NSData (less than tens of MBs).
 */
- (NSData *)aes256EncryptWithKey:(NSString*)key initializationVector: (NSData *)iv error: (NSError **)error;

/**
 * Method to AES256 decrypt the encrypted data with the specified key and initialization vector.
 *
 * @param key The key to use to decrypt the data - only up to the first kCCKeySizeAES256 characters are used.
 * @param iv The initialization vector, if any, should be up to kCCKeySizeAES128 in length and match that which was specified for encryption.  If nil, no initialization vector is used.
 *
 * @return The decrypted data if successful, otherwise nil.
 *
 * @note This method is intended for use with small NSData (less than tens of MBs).
 */
- (NSData *)aes256DecryptWithKey:(NSString*)key initializationVector: (NSData *)iv error: (NSError **)error;

@end
