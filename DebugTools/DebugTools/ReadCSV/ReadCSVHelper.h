//
//  ReadCSVHelper.h
//  DebugTools
//
//  Created by gsh_mac_2018 on 2019/7/2.
//  Copyright © 2019 gsh_mac_2018. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReadCSVHelper : NSObject
+(NSArray*)readCSV:(NSString*)filePath;
@end

NS_ASSUME_NONNULL_END
