//
//  SleepDataHelper.m
//  DebugTools
//
//  Created by gsh_mac_2018 on 2019/7/2.
//  Copyright © 2019 gsh_mac_2018. All rights reserved.
//

#import "SleepDataHelper.h"

@implementation SleepDataHelper
+(NSMutableDictionary*)getAllSleepDataFromCSV:(NSArray*)arrData
{
    NSMutableArray *_arrData = [NSMutableArray arrayWithArray:arrData];
    NSMutableDictionary *dicResult = [NSMutableDictionary new];
    NSMutableArray *arrSleepDt = [NSMutableArray new];
    double firstTime = 0;
    double endTime = 0;
    NSDictionary *item = arrData[0];
    firstTime = [SleepDataHelper getFirstTimeInterval:item[@"Utc"]];
    endTime = [SleepDataHelper getEndTimeInterval:firstTime];
    NSLog(@"firstTIme %f",firstTime);
    NSLog(@"endTime %f",endTime);
    while (_arrData.count > 0) {
        NSDictionary *itemFirst = [_arrData firstObject];
        NSDictionary *itemSec;
        NSLog(@"%@",itemFirst);
        if (_arrData.count > 1) {
            itemSec = [_arrData objectAtIndex:1];
        }
        NSString *strUTC = itemFirst[@"Utc"];
        NSLog(@"%@",strUTC);
        if (strUTC.doubleValue >= firstTime && strUTC.doubleValue <= endTime ) {
            //先計算該資料是否跨UTC
            int overTime = [SleepDataHelper checkDateIsOverEndTime:itemFirst endTime:endTime];
            NSLog(@"overTime is %d",overTime);
            if (overTime > 0) {
                NSLog(@"%@",itemFirst);
                NSString *sleeps = itemFirst[@"Sleeps"];
                NSString *sleep1 = [sleeps substringToIndex:sleeps.length - (overTime / 150)];
                NSString *sleep2 = [sleeps substringFromIndex:sleeps.length - (overTime / 150)];
                NSString *item2UTC = [NSString stringWithFormat:@"%.0f",endTime + 300];
                
                NSMutableDictionary *itemFirst1 = [NSMutableDictionary new];
                [itemFirst1 setValue:itemFirst[@"Analysis"] forKey:@"Analysis"];
                [itemFirst1 setValue:itemFirst[@"OriginalSleepID"] forKey:@"OriginalSleepID"];
                [itemFirst1 setValue:sleep1 forKey:@"Sleeps"];
                [itemFirst1 setValue:itemFirst[@"UserId"] forKey:@"UserId"];
                [itemFirst1 setValue:itemFirst[@"Utc"] forKey:@"Utc"];
                [itemFirst1 setValue:itemFirst[@"CreateDate"] forKey:@"CreateDate"];
                [itemFirst1 setValue:itemFirst[@"LastUpdate"] forKey:@"LastUpdate"];
                
                [arrSleepDt addObject:itemFirst1];
                
                NSMutableDictionary *itemFirst2 = [NSMutableDictionary new];
                [itemFirst2 setValue:itemFirst[@"Analysis"] forKey:@"Analysis"];
                [itemFirst2 setValue:itemFirst[@"OriginalSleepID"] forKey:@"OriginalSleepID"];
                [itemFirst2 setValue:sleep2 forKey:@"Sleeps"];
                [itemFirst2 setValue:itemFirst[@"UserId"] forKey:@"UserId"];
                [itemFirst2 setValue:item2UTC forKey:@"Utc"];
                [itemFirst2 setValue:itemFirst[@"CreateDate"] forKey:@"CreateDate"];
                [itemFirst2 setValue:itemFirst[@"LastUpdate"] forKey:@"LastUpdate"];
                
                [_arrData removeObject:itemFirst];
                NSMutableArray *tempArray = [NSMutableArray new];
                [tempArray addObject:itemFirst2];
                [tempArray addObjectsFromArray:_arrData];
                _arrData = tempArray;
            }
            else
            {
                if (itemSec != nil) {
                    NSString *sleeps = itemFirst[@"Sleeps"];
                    NSString *strNextUTC = itemSec[@"Utc"];
                    double nextUTC = strNextUTC.doubleValue;
                    if (nextUTC > endTime) {
                        nextUTC = endTime;
                    }
                    double checkUTC = nextUTC - (strUTC.doubleValue + sleeps.length * 150.0);
                    if (checkUTC > 0) {
                        NSLog(@"checkUTC is %f",checkUTC);
                        int diff = checkUTC / 300;
                        NSLog(@"diff %d",diff);
                        for (int j = 0; j < diff; j++) {
                            sleeps = [sleeps stringByAppendingFormat:@"FF"];
                        }
                    }
                    NSMutableDictionary *itemFirst1 = [NSMutableDictionary new];
                    [itemFirst1 setValue:itemFirst[@"Analysis"] forKey:@"Analysis"];
                    [itemFirst1 setValue:itemFirst[@"OriginalSleepID"] forKey:@"OriginalSleepID"];
                    [itemFirst1 setValue:sleeps forKey:@"Sleeps"];
                    [itemFirst1 setValue:itemFirst[@"UserId"] forKey:@"UserId"];
                    [itemFirst1 setValue:itemFirst[@"Utc"] forKey:@"Utc"];
                    [itemFirst1 setValue:itemFirst[@"CreateDate"] forKey:@"CreateDate"];
                    [itemFirst1 setValue:itemFirst[@"LastUpdate"] forKey:@"LastUpdate"];
                    [arrSleepDt addObject:itemFirst1];
                    [_arrData removeObject:itemFirst];
                }
                else
                {
                    [arrSleepDt addObject:itemFirst];
                    [_arrData removeObject:itemFirst];
                }
            }
        }
        else
        {
            if (strUTC.doubleValue < firstTime)
            {
                [_arrData removeObject:itemFirst];
            }
            else
            {
                [dicResult setValue:[arrSleepDt mutableCopy] forKey:[NSString stringWithFormat:@"%.0f",firstTime]];
                firstTime = endTime + 1;
                endTime = [SleepDataHelper getEndTimeInterval:firstTime];
                [arrSleepDt removeAllObjects];
            }
        }
    }
    
    return dicResult;
}
+(NSMutableDictionary*)getAllSleepDataFromSQLDB:(NSArray*)arrData
{
    NSMutableArray *_arrData = [NSMutableArray arrayWithArray:arrData];
    NSMutableDictionary *dicResult = [NSMutableDictionary new];
    NSMutableArray *arrSleepDt = [NSMutableArray new];
    double firstTime = 0;
    double endTime = 0;
    NSDictionary *item = arrData[0];
    firstTime = [SleepDataHelper getFirstTimeInterval:item[@"Utc"]];
    endTime = [SleepDataHelper getEndTimeInterval:firstTime];
    
    while (_arrData.count > 0) {
        NSDictionary *itemFirst = [_arrData firstObject];
        NSDictionary *itemSec;
        if (_arrData.count > 1) {
            itemSec = [_arrData objectAtIndex:1];
        }
        NSString *strUTC = itemFirst[@"Utc"];
        if (strUTC.doubleValue >= firstTime && strUTC.doubleValue <= endTime ) {
            //先計算該資料是否跨UTC
            int overTime = [SleepDataHelper checkDateIsOverEndTime:itemFirst endTime:endTime];
            if (overTime > 0) {
                //NSLog(@"%@",itemFirst);
                NSString *sleeps = itemFirst[@"Sleeps"];
                NSString *sleep1 = [sleeps substringToIndex:sleeps.length - (overTime / 150)];
                NSString *sleep2 = [sleeps substringFromIndex:sleeps.length - (overTime / 150)];
                NSString *item2UTC = [NSString stringWithFormat:@"%.0f",endTime + 300];
                
                NSMutableDictionary *itemFirst1 = [NSMutableDictionary new];
                [itemFirst1 setValue:itemFirst[@"SleepId"] forKey:@"SleepId"];
                [itemFirst1 setValue:itemFirst[@"ServerId"] forKey:@"ServerId"];
                [itemFirst1 setValue:sleep1 forKey:@"Sleeps"];
                [itemFirst1 setValue:itemFirst[@"Utc"] forKey:@"Utc"];
                
                [arrSleepDt addObject:itemFirst1];
                
                NSMutableDictionary *itemFirst2 = [NSMutableDictionary new];
                [itemFirst2 setValue:itemFirst[@"SleepId"] forKey:@"SleepId"];
                [itemFirst2 setValue:itemFirst[@"ServerId"] forKey:@"ServerId"];
                [itemFirst2 setValue:sleep2 forKey:@"Sleeps"];
                [itemFirst2 setValue:item2UTC forKey:@"Utc"];
                
                [_arrData removeObject:itemFirst];
                NSMutableArray *tempArray = [NSMutableArray new];
                [tempArray addObject:itemFirst2];
                [tempArray addObjectsFromArray:_arrData];
                _arrData = tempArray;
            }
            else
            {
                if (itemSec != nil) {
                    NSString *sleeps = itemFirst[@"Sleeps"];
                    NSString *strNextUTC = itemSec[@"Utc"];
                    double nextUTC = strNextUTC.doubleValue;
                    if (nextUTC > endTime) {
                        nextUTC = endTime;
                    }
                    double checkUTC = nextUTC - (strUTC.doubleValue + sleeps.length * 150.0);
                    if (checkUTC > 0) {
                        NSLog(@"checkUTC is %f",checkUTC);
                        int diff = checkUTC / 300;
                        NSLog(@"diff %d",diff);
                        for (int j = 0; j < diff; j++) {
                            sleeps = [sleeps stringByAppendingFormat:@"FF"];
                        }
                    }
                    NSMutableDictionary *itemFirst1 = [NSMutableDictionary new];
                    [itemFirst1 setValue:itemFirst[@"SleepId"] forKey:@"SleepId"];
                    [itemFirst1 setValue:itemFirst[@"ServerId"] forKey:@"ServerId"];
                    [itemFirst1 setValue:sleeps forKey:@"Sleeps"];
                    [itemFirst1 setValue:itemFirst[@"Utc"] forKey:@"Utc"];
                    
                    [arrSleepDt addObject:itemFirst1];
                    [_arrData removeObject:itemFirst];
                }
                else
                {
                    [arrSleepDt addObject:itemFirst];
                    [_arrData removeObject:itemFirst];
                }
            }
        }
        else
        {
            if (strUTC.doubleValue < firstTime)
            {
                [_arrData removeObject:itemFirst];
            }
            else
            {
                [dicResult setValue:[arrSleepDt mutableCopy] forKey:[NSString stringWithFormat:@"%.0f",firstTime]];
                firstTime = endTime + 1;
                endTime = [SleepDataHelper getEndTimeInterval:firstTime];
                [arrSleepDt removeAllObjects];
            }
        }
    }
    
    return dicResult;
}
+(double)getFirstTimeInterval:(NSString*)time
{
    //NSLog(@"firstTime is %.0f",time.doubleValue);
    NSDate *firstDate = [NSDate dateWithTimeIntervalSince1970:time.doubleValue];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy/MM/dd 00:00:00"];
    NSString *tempDate = [formatter stringFromDate:firstDate];
    NSDate *newFirstDate = [formatter dateFromString:tempDate];
    return newFirstDate.timeIntervalSince1970 + 64800;
}
+(double)getEndTimeInterval:(double)time
{
    return time + 86399;
}
+(int)checkDateIsOverEndTime:(NSDictionary*)dicData endTime:(double)endTime
{
    int ret = 0;
    NSString *sleeps = dicData[@"Sleeps"];
    NSString *strUTC = dicData[@"Utc"];
    NSInteger sleepsCount = strUTC.doubleValue + ((sleeps.length - 2) * 150);
    if (sleepsCount > endTime) {
        ret = sleepsCount - endTime;
    }
    return ret;
}

@end
