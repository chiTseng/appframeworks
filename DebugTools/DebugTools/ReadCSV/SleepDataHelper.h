//
//  SleepDataHelper.h
//  DebugTools
//
//  Created by gsh_mac_2018 on 2019/7/2.
//  Copyright © 2019 gsh_mac_2018. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SleepDataHelper : NSObject
+(NSMutableDictionary*)getAllSleepDataFromCSV:(NSArray*)arrData;
+(NSMutableDictionary*)getAllSleepDataFromSQLDB:(NSArray*)arrData;
@end

NS_ASSUME_NONNULL_END
