//
//  ReadCSVHelper.m
//  DebugTools
//
//  Created by gsh_mac_2018 on 2019/7/2.
//  Copyright © 2019 gsh_mac_2018. All rights reserved.
//

#import "ReadCSVHelper.h"

@implementation ReadCSVHelper
+(NSArray*)readCSV:(NSString*)filePath
{
    NSMutableArray *arrResult = [NSMutableArray new];
    NSMutableArray *arrTitle = [NSMutableArray new];
    
    NSString *strPath = [[NSBundle mainBundle] pathForResource:filePath ofType:@"csv"];
    NSString *fileContents = [NSString stringWithContentsOfFile:strPath encoding:NSUTF8StringEncoding error:nil];
    NSArray* rows = [fileContents componentsSeparatedByString:@"\n"];
    for (int j = 0; j < rows.count; j++)
    {
        NSString *row = rows[j];
        if ([row isEqualToString:@""])
        {
            continue;
        }
        NSMutableDictionary *dicRow = [NSMutableDictionary new];
        NSArray* columns = [row componentsSeparatedByString:@","];
        if (j == 0)
        {
            for (NSString * title in columns) {
                [arrTitle addObject:title];
            }
        }
        else
        {
            for (int i = 0; i < columns.count; i++) {
                NSString *title = arrTitle[i];
                [dicRow setValue:columns[i] forKey:title];
            }
            [arrResult addObject:dicRow];
        }
        
    }
    return arrResult;
}
@end
