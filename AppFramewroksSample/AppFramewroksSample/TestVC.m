//
//  TestVC.m
//  AppFramewroksSample
//
//  Created by gsh_mac_2018 on 2019/9/17.
//  Copyright © 2019 GSH. All rights reserved.
//

#import "TestVC.h"

@interface TestVC ()

@end

@implementation TestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSError *error;
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *jsonString = @"{\"MaxVersion\":\"2017-11-30T10:01:46\",\"JsonMessage\":[{\"ServerId\":156854,\"Title\":\"hello im curtis\",\"Content\":\"Hi welcome\",\"RecordDate\":\"2017-11-30T10:01:46\",\"ReadCount\":0,\"UpdateStatus\":\"A\"}]}";
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        NSLog(@"%@",dic);
        if (error) {
            NSLog(@"%@",error.debugDescription);
        }
    });
    
    
   
}


@end
