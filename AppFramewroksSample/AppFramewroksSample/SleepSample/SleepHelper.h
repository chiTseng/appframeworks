//
//  SleepHelper.h
//  AppFramewroksSample
//
//  Created by gsh_mac_2018 on 2019/10/2.
//  Copyright © 2019 GSH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GUtility/GUtility.h>
#import "FMNEWDatabaseQueue.h"
#import "FMNEWDatabase.h"
#import "FMNEWDatabaseAdditions.h"
NS_ASSUME_NONNULL_BEGIN

@interface SleepHelper : UIViewController
+ (void)                checkDBfileExists;
+ (FMNEWDatabase *)     openDB;
+ (void)                closeDB: (FMNEWDatabase *) db;
+ (NSArray *)            GetTmpSleepDataRecordsFilterWaitDelete;
@end

NS_ASSUME_NONNULL_END
