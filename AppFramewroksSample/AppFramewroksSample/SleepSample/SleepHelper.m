//
//  SleepHelper.m
//  AppFramewroksSample
//
//  Created by gsh_mac_2018 on 2019/10/2.
//  Copyright © 2019 GSH. All rights reserved.
//

#import "SleepHelper.h"

@interface SleepHelper ()

@end

@implementation SleepHelper

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
+ (void)checkDBfileExists
{
    DLog(@"=======>%s:%d",__FUNCTION__,__LINE__);
    NSDate* tmpStartData = [NSDate date];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [paths objectAtIndex:0];
    NSString *dbPath = [documentDir stringByAppendingPathComponent:@"iHealthDB"];
    NSString *sourceDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"iHealthDB"];
    if (![fileManager fileExistsAtPath:dbPath])
    {
        NSError *error;
        [fileManager copyItemAtPath:sourceDBPath toPath:dbPath error:&error];
    }
    double deltaTime = [[NSDate date] timeIntervalSinceDate:tmpStartData];
    NSLog(@"cost time = %f", deltaTime);
    DLog(@"=======>%s:%d",__FUNCTION__,__LINE__);
}
+ (FMNEWDatabase *) openDB
{
    [SleepHelper checkDBfileExists];
    FMNEWDatabase *db = [FMNEWDatabase databaseWithPath:[[GAppUtility getDocumentFolderPath] stringByAppendingPathComponent:@"iHealthDB"]];
    if ([db open])
        return db;
    else
        return nil;
}
+ (void) closeDB: (FMNEWDatabase *) db
{
    [db close];
}
+ (NSArray*) GetTmpSleepDataRecordsFilterWaitDelete //@2017/03/29 過濾待刪除的原始睡眠資料給睡眠分析用
{
    NSMutableArray *tmplSleepDataArray = [[NSMutableArray alloc] init];
    
    FMNEWDatabase* db = [SleepHelper openDB];

    NSString *sqlcmd = [NSString stringWithFormat:@"SELECT * FROM A5SleepTemp ORDER BY SleepUTC ASC"];
    FMNEWResultSet* rs = [db executeQuery:sqlcmd, nil];
    
    while ([rs next])
    {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        NSString *strUTC = [rs stringForColumn:@"SleepUTC"];
        NSString *strsleepArray = [rs stringForColumn:@"SleepArray"];
        NSString *strSleepID = [rs stringForColumn:@"SleepId"];
        NSString *strServerID = [rs stringForColumn:@"ServerId"];
        
        [dic setValue:strUTC forKey:@"Utc"];
        [dic setValue:strSleepID forKey:@"SleepId"];
        [dic setValue:strServerID forKey:@"ServerId"];
        [dic setValue:strsleepArray forKey:@"Sleeps"];

        [tmplSleepDataArray addObject:dic];
    }
    [SleepHelper closeDB:db];
    NSArray *sleepRawDataArray = [NSArray arrayWithArray:tmplSleepDataArray];
    return sleepRawDataArray;
}

@end
