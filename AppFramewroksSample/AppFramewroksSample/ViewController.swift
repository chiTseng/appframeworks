//
//  ViewController.swift
//  AppFramewroksSample
//
//  Created by elaine on 2018/5/29.
//  Copyright © 2018年 GSH. All rights reserved.
//

import UIKit
import DeviceManager
import NetworkExtension
import Crashlytics
import DebugTools
import GAPIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,BLEDeviceManagerDelegate,WebServiceAPIDelegate
{
    func webServiceCallFinished(_ returnData: Data!, soapAction: String!) {
        //Crashlytics.sharedInstance().crash()
        /*NSString *xmlString = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];*/
        let str = NSString(bytes: (returnData! as NSData).bytes, length: (returnData! as NSData).length, encoding: String.Encoding.utf8.rawValue)
        print(str!)
        let alertVC = UIAlertController(title: "server result", message: str! as String, preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok", style: .cancel, handler: nil)
        alertVC.addAction(ok)
        self.present(alertVC, animated: true, completion: nil)
        
    }
    
    func webServiceCallError(_ error: Error!, soapAction: String!) {
        
    }
    
    @IBOutlet weak var btnBW: UIButton!
    @IBOutlet weak var btnBT: UIButton!
    @IBOutlet weak var btnBP: UIButton!
    @IBOutlet weak var btnBS: UIButton!
    @IBOutlet weak var btnECG: UIButton!
    
    @IBOutlet weak var btnTestServer: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var arrDeviceInfo : NSMutableDictionary!
    
    var manager : BLESeriesManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrDeviceInfo = NSMutableDictionary();
        manager = BLESeriesManager(delegate: self, seriesType: Int32(BLEDEVICE_BP_SERIES))
        manager.startScanList()
        btnBP.addTarget(self, action: #selector(clickBtn(sender:)), for: .touchUpInside)
        btnBT.addTarget(self, action: #selector(clickBtn(sender:)), for: .touchUpInside)
        btnBW.addTarget(self, action: #selector(clickBtn(sender:)), for: .touchUpInside)
        btnBS.addTarget(self, action: #selector(clickBtn(sender:)), for: .touchUpInside)
        btnECG.addTarget(self, action: #selector(clickBtn(sender:)), for: .touchUpInside)
        btnBP.isSelected = true
        btnTestServer.addTarget(self, action: #selector(clickTestServer), for: .touchUpInside)
        var i = self.firstMissingPositive([1,2,0])
        print("i is \(i)")
        i = self.firstMissingPositive([7,8,9])
        print("i is \(i)")
        i = self.firstMissingPositive([-1,1,3,4])
        print("i is \(i)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func clickTestServer()
    {
        /*let arr = ReadCSVHelper.readCSV("Results")
        //let arr = SleepHelper.getTmpSleepDataRecordsFilterWaitDelete()
        let dic = SleepDataHelper.getAllSleepData(fromCSV: arr)
        var keys = dic.allKeys
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd hh:mm:ss"
        keys.sort { (a, b) -> Bool in let _a = a as! String;let _b = b as! String;if _a < _b {return true} else {return false}}
        for k in keys {
            print("===================")
            print("key is ",k)
            let d = dic[k] as! NSArray
            var 睡眠資料 = ""
            var 入睡時間 : NSString = ""
            for  v  in d
            {
                let _v = v as! NSMutableDictionary
                let _utc = _v["Utc"] as! NSString
                if 入睡時間 == "" { 入睡時間 = _utc}
                let _sleeps = _v["Sleeps"] as! String
                睡眠資料 = 睡眠資料 + _sleeps
            }
            print(睡眠資料)
 
            let results = LSSleepAnalyze.dataAnalysis(睡眠資料, time: 入睡時間.longLongValue, interval: 300, gmtOffset: Int32(TimeZone.current.secondsFromGMT()), version: .V1)
            for result:LSSleepAnalyzeResult in results!
            {
                var date = Date(timeIntervalSince1970: TimeInterval(result.sleepTimeUTC))
                let startTime = dateFormatter.string(from: date)
                //print("startDate is ",date)
                date = Date(timeIntervalSince1970: TimeInterval(result.getupTimeUTC))
                let endTime = dateFormatter.string(from: date)
                //print("EndDate is ",date)
                print("SleepTime is ",startTime)
                print("WakeupTime is ",endTime)
                print("睡眠狀態",result.sleepStatus!)
            }
            print("===================")
        }*/
        //let service = SleepService()
        //service.service_GetOriginalSleepRecords("nccam2017", password: "533145017", strID: "", with: self)
        
        //let service = BaseSOAPService()
        //service.testServer()
        
        let service = AuthenticateService()
        service.service_UserLogin(byName: "nccam2017", password: "533145017", username: "ch19095000", with: self)
        /*if #available(iOS 11.0, *) {
            let hotspotConfig = NEHotspotConfiguration(ssid: "GSHAdmin_5G", passphrase: "gshadmin", isWEP: false)
            NEHotspotConfigurationManager.shared.apply(hotspotConfig) { (error) in
                
            }
        } else {
            // Fallback on earlier versions
        }*/
        
       // let vc = LineChart1ViewController()
        //self.present(vc, animated: false, completion: nil)
        
    }
    func firstMissingPositive(_ nums: [Int]) -> Int {

        let length = nums.count
        if length == 0 {return 1}
        let _nums  = nums.sorted()
        var k = 1//k 是用来作为对比的下标
        for i in 0...(length - 1)
        {
            if _nums[i] <= 0 || (i > 0 && _nums[i] == _nums[i-1])
            {
                continue
            }
            else if _nums[i] != k
            {
                return k;
            }
            k = k + 1
        }
        if _nums[length - 1] <= 0
        {
            return 1
        }
        return _nums[length-1] + 1
        
    }
    @objc func clickBtn(sender:UIButton)
    {
        if sender.isSelected == true
        {
            return
        }
        btnBW.isSelected = false
        btnBT.isSelected = false
        btnBP.isSelected = false
        btnBS.isSelected = false
        btnECG.isSelected = false
        sender.isSelected = true
        arrDeviceInfo.removeAllObjects()
        tableView.reloadData()
        switch sender {
        case btnBW:
            manager.changeSeriesTypeAndRescan(byScanList:Int32(BLEDEVICE_BW_SERIES))
            break
        case btnBT:
            manager.changeSeriesTypeAndRescan(byScanList:Int32(BLEDEVICE_BT_SERIES))
            break
        case btnECG:
            manager.changeSeriesTypeAndRescan(byScanList:Int32(BLEDEVICE_ECG_SERIES))
            break
        case btnBS:
            manager.changeSeriesTypeAndRescan(byScanList:Int32(BLEDEVICE_BS_SERIES))
            break
        default:
            manager.changeSeriesTypeAndRescan(byScanList:Int32(BLEDEVICE_BP_SERIES))
            break
        }
    }
//MARK: - tableview delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeviceInfo.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height / 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let arrKeys = arrDeviceInfo.allKeys
        let key = arrKeys[indexPath.row]
        let deviceInfo = arrDeviceInfo[key] as? NSMutableDictionary
        let name : String = deviceInfo!["name"] as! String
        let uuid : String = deviceInfo!["uuid"] as! String
        let RSSI : NSNumber = deviceInfo!["rssi"] as! NSNumber
        let date = Date()
        let nowNumber = Int64(date.timeIntervalSince1970)
        let lastUpdatedate : NSNumber = deviceInfo!["timeIntervalSince1970"] as! NSNumber
        let diffNumber = nowNumber - lastUpdatedate.int64Value
        
        let cell = UITableViewCell()
        cell.textLabel?.text = "name:\(name)\nuuid:\(uuid)\nrssi:\(RSSI.stringValue)\nlastUpdateDate:\(diffNumber)"
        cell.textLabel?.numberOfLines = -1
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let arrKeys = arrDeviceInfo.allKeys
        let key : String = arrKeys[indexPath.row] as! String
        manager.connectDevice(byUUID: key)
    }
//MARK: - blemanager delegate -
    func bleManager(_ manager: Any!, deviceList list: NSMutableDictionary!) {
        
        arrDeviceInfo = list
        tableView.reloadData()
    }
    func bleManager(_ manager: Any!, device resultDevice: Any!) {
        let device = resultDevice as? BaseBLEDevice
        
        let alertVC = UIAlertController(title: "設備資訊", message: " firmwareVersion is \((device?.firmwareVersion)!)\n hardwareVersion is \((device?.hardwareVersion)!)\n softwareVersion is \((device?.softwareVersion)!)\n manufacturerName is \((device?.manufacturerName)!)\n macAddress is \((device?.macAddress)!)", preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok", style: .cancel, handler: nil)
        let reconnect = UIAlertAction(title: "reconnect", style: .default) { (action) in
            self.manager.disconnectBT()
            self.manager.startScan()
        }
        alertVC.addAction(ok)
        alertVC.addAction(reconnect)
        self.present(alertVC, animated: false, completion: nil)
    }
    
    func bleManager(_ manager: Any!, updateStatus status: Int32) {
        
        switch status{
        case 0:
            print("G_BLE_STATUS_IDLE")
        case 1:
            print("G_BLE_STATUS_SCANNING")
        case 2:
            print("G_BLE_STATUS_CONNECTING")
        case 3:
            print("G_BLE_STATUS_CONNECTED")
        case 4:
            print("G_BLE_STATUS_DISCONNECTED")
        case 5:
            print("G_BLE_ERROR")
        case 6:
            print("G_BLE_STATUS_DISCONNECTED_BYUSER")
        default:
            print("unknown_G_BLE_ERROR")
        }
        
    }
    func bleManager(_ manager: Any!, device resultDevice: Any!, resultData data: NSObject!) {
        
    }
}

