//
//  FMNEWDatabasePool.h
//  FMNEWDb
//
//  Created by August Mueller on 6/22/11.
//  Copyright 2011 Flying Meat Inc. All rights reserved.
//
#ifdef DEBUGMODE
#    define DLog(...) NSLog(__VA_ARGS__)
#else
#    define DLog(...) /* */
#endif
#import <Foundation/Foundation.h>
#import "sqlite3.h"

@class FMNEWDatabase;

@interface FMNEWDatabaseQueue : NSObject {
    NSString            *_path;
    dispatch_queue_t    _queue;
    FMNEWDatabase          *_db;
}

@property (retain) NSString *path;

+ (id)databaseQueueWithPath:(NSString*)aPath;
- (id)initWithPath:(NSString*)aPath;
- (void)close;

- (void)inDatabase:(void (^)(FMNEWDatabase *db))block;

- (void)inTransaction:(void (^)(FMNEWDatabase *db, BOOL *rollback))block;
- (void)inDeferredTransaction:(void (^)(FMNEWDatabase *db, BOOL *rollback))block;

#if SQLITE_VERSION_NUMBER >= 3007000
// NOTE: you can not nest these, since calling it will pull another database out of the pool and you'll get a deadlock.
// If you need to nest, use FMNEWDatabase's startSavePointWithName:error: instead.
- (NSError*)inSavePoint:(void (^)(FMNEWDatabase *db, BOOL *rollback))block;
#endif

@end

