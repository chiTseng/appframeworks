//
//  FMNEWDatabasePool.m
//  FMNEWDb
//
//  Created by August Mueller on 6/22/11.
//  Copyright 2011 Flying Meat Inc. All rights reserved.
//

#import "FMNEWDatabaseQueue.h"
#import "FMNEWDatabase.h"

/*
 
 Note: we call [self retain]; before using dispatch_sync, just incase 
 FMNEWDatabaseQueue is released on another thread and we're in the middle of doing
 something in dispatch_sync
 
 */
 
@implementation FMNEWDatabaseQueue

@synthesize path = _path;

+ (id)databaseQueueWithPath:(NSString*)aPath {
    
    FMNEWDatabaseQueue *q = [[self alloc] initWithPath:aPath];
    
    FMNEWDBAutorelease(q);
    
    return q;
}

- (id)initWithPath:(NSString*)aPath {
    
    self = [super init];
    
    if (self != nil) {
        
        _db = [FMNEWDatabase databaseWithPath:aPath];
        FMNEWDBRetain(_db);
        
        if (![_db open]) {
            DLog(@"Could not create database queue for path %@", aPath);
            FMNEWDBRelease(self);
            return 0x00;
        }
        
        _path = FMNEWDBReturnRetained(aPath);
        
        _queue = dispatch_queue_create([[NSString stringWithFormat:@"FMNEWDb.%@", self] UTF8String], NULL);
    }
    
    return self;
}

- (void)dealloc {
    
    FMNEWDBRelease(_db);
    FMNEWDBRelease(_path);
    
    if (_queue) {
        dispatch_release(_queue);
        _queue = 0x00;
    }
#if ! __has_feature(objc_arc)
    [super dealloc];
#endif
}

- (void)close {
    FMNEWDBRetain(self);
    dispatch_sync(_queue, ^() { 
        [_db close];
        FMNEWDBRelease(_db);
        _db = 0x00;
    });
    FMNEWDBRelease(self);
}

- (FMNEWDatabase*)database {
    if (!_db) {
        _db = FMNEWDBReturnRetained([FMNEWDatabase databaseWithPath:_path]);
        
        if (![_db open]) {
            DLog(@"FMNEWDatabaseQueue could not reopen database for path %@", _path);
            FMNEWDBRelease(_db);
            _db  = 0x00;
            return 0x00;
        }
    }
    
    return _db;
}

- (void)inDatabase:(void (^)(FMNEWDatabase *db))block {
    FMNEWDBRetain(self);
    
    dispatch_sync(_queue, ^() {
        
        FMNEWDatabase *db = [self database];
        block(db);
        
        if ([db hasOpenResultSets]) {
            DLog(@"Warning: there is at least one open result set around after performing [FMNEWDatabaseQueue inDatabase:]");
        }
    });
    
    FMNEWDBRelease(self);
}


- (void)beginTransaction:(BOOL)useDeferred withBlock:(void (^)(FMNEWDatabase *db, BOOL *rollback))block {
    FMNEWDBRetain(self);
    dispatch_sync(_queue, ^() { 
        
        BOOL shouldRollback = NO;
        
        if (useDeferred) {
            [[self database] beginDeferredTransaction];
        }
        else {
            [[self database] beginTransaction];
        }
        
        block([self database], &shouldRollback);
        
        if (shouldRollback) {
            [[self database] rollback];
        }
        else {
            [[self database] commit];
        }
    });
    
    FMNEWDBRelease(self);
}

- (void)inDeferredTransaction:(void (^)(FMNEWDatabase *db, BOOL *rollback))block {
    [self beginTransaction:YES withBlock:block];
}

- (void)inTransaction:(void (^)(FMNEWDatabase *db, BOOL *rollback))block {
    [self beginTransaction:NO withBlock:block];
}

#if SQLITE_VERSION_NUMBER >= 3007000
- (NSError*)inSavePoint:(void (^)(FMNEWDatabase *db, BOOL *rollback))block {
    
    static unsigned long savePointIdx = 0;
    __block NSError *err = 0x00;
    FMNEWDBRetain(self);
    dispatch_sync(_queue, ^() { 
        
        NSString *name = [NSString stringWithFormat:@"savePoint%ld", savePointIdx++];
        
        BOOL shouldRollback = NO;
        
        if ([[self database] startSavePointWithName:name error:&err]) {
            
            block([self database], &shouldRollback);
            
            if (shouldRollback) {
                [[self database] rollbackToSavePointWithName:name error:&err];
            }
            else {
                [[self database] releaseSavePointWithName:name error:&err];
            }
            
        }
    });
    FMNEWDBRelease(self);
    return err;
}
#endif

@end
