//
//  FMNEWDatabasePool.h
//  FMNEWDb
//
//  Created by August Mueller on 6/22/11.
//  Copyright 2011 Flying Meat Inc. All rights reserved.
//
#ifdef DEBUGMODE
#    define DLog(...) NSLog(__VA_ARGS__)
#else
#    define DLog(...) /* */
#endif
#import <Foundation/Foundation.h>
#import "sqlite3.h"

/*

                         ***README OR SUFFER***
Before using FMNEWDatabasePool, please consider using FMNEWDatabaseQueue instead.

If you really really really know what you're doing and FMNEWDatabasePool is what
you really really need (ie, you're using a read only database), OK you can use
it.  But just be careful not to deadlock!

For an example on deadlocking, search for:
ONLY_USE_THE_POOL_IF_YOU_ARE_DOING_READS_OTHERWISE_YOULL_DEADLOCK_USE_FMNEWDATABASEQUEUE_INSTEAD
in the main.m file.

*/



@class FMNEWDatabase;

@interface FMNEWDatabasePool : NSObject {
    NSString            *_path;
    
    dispatch_queue_t    _lockQueue;
    
    NSMutableArray      *_databaseInPool;
    NSMutableArray      *_databaseOutPool;
    
    __unsafe_unretained id _delegate;
    
    NSUInteger          _maximumNumberOfDatabasesToCreate;
}

@property (retain) NSString *path;
@property (assign) id delegate;
@property (assign) NSUInteger maximumNumberOfDatabasesToCreate;

+ (id)databasePoolWithPath:(NSString*)aPath;
- (id)initWithPath:(NSString*)aPath;

- (NSUInteger)countOfCheckedInDatabases;
- (NSUInteger)countOfCheckedOutDatabases;
- (NSUInteger)countOfOpenDatabases;
- (void)releaseAllDatabases;

- (void)inDatabase:(void (^)(FMNEWDatabase *db))block;

- (void)inTransaction:(void (^)(FMNEWDatabase *db, BOOL *rollback))block;
- (void)inDeferredTransaction:(void (^)(FMNEWDatabase *db, BOOL *rollback))block;

#if SQLITE_VERSION_NUMBER >= 3007000
// NOTE: you can not nest these, since calling it will pull another database out of the pool and you'll get a deadlock.
// If you need to nest, use FMNEWDatabase's startSavePointWithName:error: instead.
- (NSError*)inSavePoint:(void (^)(FMNEWDatabase *db, BOOL *rollback))block;
#endif

@end


@interface NSObject (FMNEWDatabasePoolDelegate)

- (BOOL)databasePool:(FMNEWDatabasePool*)pool shouldAddDatabaseToPool:(FMNEWDatabase*)database;

@end

