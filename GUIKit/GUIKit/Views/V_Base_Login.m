//
//  V_Base_Login.m
//  GUIKit
//
//  Created by Yu Chi on 2020/1/17.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

#import "V_Base_Login.h"

@implementation V_Base_Login
@synthesize txtAccount,txtPassword,delegate;
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor grayColor];
    self.frame = frame;
    if (self) {
        [self initView];
    }
    return self;
}
- (void) initView
{
    UIImageView *logoView = [UIImageView new];
   
    CGFloat bW = self.frame.size.width * 0.5;
    CGFloat bX = (self.frame.size.width - bW ) / 2;
   
    CGFloat bH = 60;
    logoView.frame = CGRectMake(bX, 30, bW, bH);
    [self addSubview:logoView];
    
    UILabel *ubl_dt = [UILabel new];
    ubl_dt.frame = CGRectMake(0, logoView.frame.origin.y + logoView.frame.size.height + 10, self.frame.size.width, 30);
    ubl_dt.text = NSLocalizedString(@"請登入您的帳號", nil);
    ubl_dt.textAlignment = NSTextAlignmentCenter;
    [self addSubview:ubl_dt];
    txtAccount = [[UITextField alloc] initWithFrame:CGRectMake(60, ubl_dt.frame.origin.y + ubl_dt.frame.size.height + 10, self.frame.size.width - 90, 40)];
    
    //2017/07/11   調整  帳號label 位置 by barret
    [self addSubview:txtAccount];
    [txtAccount setBorderStyle:UITextBorderStyleNone];
    [txtAccount setPlaceholder:NSLocalizedString(@"請輸入健康管理帳號",nil)];//Jora
    [txtAccount setReturnKeyType:UIReturnKeyDone];
    txtAccount.keyboardType = UIKeyboardTypeASCIICapable;
    txtAccount.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    txtAccount.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [txtAccount setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [txtAccount setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtAccount.clearButtonMode = UITextFieldViewModeWhileEditing;
    [txtAccount setDelegate:self];
    UIView *accountLine = [UIView new];
    accountLine.frame = CGRectMake(txtAccount.frame.origin.x - 30, txtAccount.frame.origin.y + txtAccount.frame.size.height, txtAccount.frame.size.width + 30, 1);
    accountLine.backgroundColor = [UIColor blackColor];
    [self addSubview:accountLine];
    
    txtPassword = [[UITextField alloc] initWithFrame:CGRectMake(60, txtAccount.frame.origin.y + txtAccount.frame.size.height + 20, self.frame.size.width - 90, 40)];
    [self addSubview:txtPassword];
    [txtPassword setPlaceholder:NSLocalizedString(@"請輸入密碼", nil)];
    [txtPassword setBorderStyle:UITextBorderStyleNone];
    [txtPassword setSecureTextEntry:YES];
    [txtPassword setReturnKeyType:UIReturnKeyDone];
    txtPassword.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    txtPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
    [txtPassword setDelegate:self];
    UIView *passwordLine = [UIView new];
    passwordLine.frame = CGRectMake(txtPassword.frame.origin.x - 30, txtPassword.frame.origin.y + txtPassword.frame.size.height, txtPassword.frame.size.width + 30, 1);
    passwordLine.backgroundColor = [UIColor blackColor];
    [self addSubview:passwordLine];
    
    UIButton *btnlogin = [[UIButton alloc] init];
    //UIImage *btnLoginImg = [UIImage imageNamed:@"btn_login.png"];
    btnlogin.frame = CGRectMake(30, txtPassword.frame.origin.y + txtPassword.frame.size.height + 20, self.frame.size.width - 60, 40);
    //[btnlogin setBackgroundImage:btnLoginImg forState:UIControlStateNormal];
    [btnlogin addTarget:self action:@selector(buttonLoginClickHandler:) forControlEvents:UIControlEventTouchUpInside];
    [btnlogin setTitle:NSLocalizedString(@"登入", nil) forState:UIControlStateNormal];
    [btnlogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnlogin.titleLabel.font = [UIFont systemFontOfSize:20];
    btnlogin.layer.cornerRadius = btnlogin.frame.size.height / 2;
    btnlogin.layer.borderColor = [[UIColor blackColor] CGColor];
    btnlogin.layer.borderWidth = 2;
    [self addSubview:btnlogin];
    
}
-(void) buttonLoginClickHandler:(UIButton*)sender
{
    [txtAccount resignFirstResponder];
    [txtPassword resignFirstResponder];
    if (delegate != nil) {
        [delegate V_Base_Login_CallBack:sender];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}
@end
