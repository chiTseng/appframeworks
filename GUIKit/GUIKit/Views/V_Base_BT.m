//
//  V_Base_BT.m
//  GUIKit
//
//  Created by Yu Chi on 2020/1/17.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

#import "V_Base_BT.h"

@implementation V_Base_BT
@synthesize titleLabel,valueLabel,delegate;
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor grayColor];
    self.frame = frame;
    if (self) {
        [self initView];
    }
    return self;
}
- (void) initView
{
    UIImageView *logoView = [UIImageView new];
   
    CGFloat bW = self.frame.size.width * 0.5;
    CGFloat bX = (self.frame.size.width - bW ) / 2;
   
    CGFloat bH = 60;
    logoView.frame = CGRectMake(bX, 30, bW, bH);
    [self addSubview:logoView];
    
    UILabel *ubl_dt = [UILabel new];
    ubl_dt.frame = CGRectMake(0, logoView.frame.origin.y + logoView.frame.size.height + 10, self.frame.size.width, 30);
    ubl_dt.text = NSLocalizedString(@"請登入您的帳號", nil);
    ubl_dt.textAlignment = NSTextAlignmentCenter;
    [self addSubview:ubl_dt];
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, ubl_dt.frame.origin.y + ubl_dt.frame.size.height + 10, self.frame.size.width - 90, 40)];
    //2017/07/11   調整  帳號label 位置 by barret
    [self addSubview:titleLabel];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    titleLabel.text = NSLocalizedString(@"體溫", "");
    
    valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, titleLabel.frame.origin.y + titleLabel.frame.size.height + 20, self.frame.size.width - 90, 40)];
    [self addSubview:valueLabel];
    valueLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    UIView *passwordLine = [UIView new];
    passwordLine.frame = CGRectMake(valueLabel.frame.origin.x - 30, valueLabel.frame.origin.y + valueLabel.frame.size.height, valueLabel.frame.size.width + 30, 1);
    passwordLine.backgroundColor = [UIColor blackColor];
    [self addSubview:passwordLine];
    
    UIButton *btnlogin = [[UIButton alloc] init];
    //UIImage *btnLoginImg = [UIImage imageNamed:@"btn_login.png"];
    btnlogin.frame = CGRectMake(30, valueLabel.frame.origin.y + valueLabel.frame.size.height + 20, self.frame.size.width - 60, 40);
    //[btnlogin setBackgroundImage:btnLoginImg forState:UIControlStateNormal];
    [btnlogin addTarget:self action:@selector(buttonLoginClickHandler:) forControlEvents:UIControlEventTouchUpInside];
    [btnlogin setTitle:NSLocalizedString(@"登入", nil) forState:UIControlStateNormal];
    [btnlogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnlogin.titleLabel.font = [UIFont systemFontOfSize:20];
    btnlogin.layer.cornerRadius = btnlogin.frame.size.height / 2;
    btnlogin.layer.borderColor = [[UIColor blackColor] CGColor];
    btnlogin.layer.borderWidth = 2;
    [self addSubview:btnlogin];
    
}
-(void) buttonLoginClickHandler:(UIButton*)sender
{
    if (delegate != nil)
    {
        [delegate V_Base_BT_CallBack:sender];
    }
}

@end
