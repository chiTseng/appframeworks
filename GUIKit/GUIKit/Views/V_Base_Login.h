//
//  V_Base_Login.h
//  GUIKit
//
//  Created by Yu Chi on 2020/1/17.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol V_Base_Login_delegate
@required
-(void) V_Base_Login_CallBack:(UIButton*)btn;
@end
@interface V_Base_Login : UIView <UITextFieldDelegate>
@property (retain,nonatomic) UITextField *txtAccount;
@property (retain,nonatomic) UITextField *txtPassword;
@property (assign) id<V_Base_Login_delegate> delegate;
@end


