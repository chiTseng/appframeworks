//
//  V_Base_BT.h
//  GUIKit
//
//  Created by Yu Chi on 2020/1/17.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol V_Base_BT_delegate
@required
-(void) V_Base_BT_CallBack:(UIButton*)btn;
@end
@interface V_Base_BT : UIView
@property (retain,nonatomic) UILabel *titleLabel;
@property (retain,nonatomic) UILabel *valueLabel;
@property (assign) id<V_Base_BT_delegate> delegate;
@end

NS_ASSUME_NONNULL_END
