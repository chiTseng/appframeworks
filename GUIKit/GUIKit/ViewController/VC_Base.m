//
//  VC_Base.m
//  GUIKit
//
//  Created by Yu Chi on 2020/1/17.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

#import "VC_Base.h"

@interface VC_Base ()

@end

@implementation VC_Base

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)setContaintView:(UIView*)view
{
    [self.view addSubview:view];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
