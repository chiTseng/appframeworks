//
//  VC_Base.h
//  GUIKit
//
//  Created by Yu Chi on 2020/1/17.
//  Copyright © 2020 gsh_mac_2018. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VC_Base : UIViewController
-(void)setContaintView:(UIView*)view;
@end

NS_ASSUME_NONNULL_END
