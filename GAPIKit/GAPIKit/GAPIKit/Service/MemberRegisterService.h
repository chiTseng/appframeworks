//
//  MemberRegisterService.h
//  GAPIKit
//
//  Created by elaine on 2018/12/13.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "BaseSOAPService.h"

@interface MemberRegisterService : BaseSOAPService
/**
 *@brief 利用email查詢密碼
 （int）0=成功,1=Exception,2=ID/密碼錯誤,3=沒有減重計畫 Plan
 */
-(WebServiceAPI*)Service_CheckUserExist:(NSString*)userID firstName:(NSString *)firstName birthday:(NSString *)birthday code:(NSString *)code withDelegate:(id<WebServiceAPIDelegate>)delegate;
@end
