//
//  SleepAuthenticateService.h
//  GAPIKit
//
//  Created by gsh_mac_2018 on 2019/7/3.
//  Copyright © 2019 GSH. All rights reserved.
//

#import <GAPIKit/GAPIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SleepService : BaseSOAPService
/**
 *@brief 取得睡眠原始資料
 id in
 password in
 strId in
 （int）0=成功,1=Exception,2=ID/密碼錯誤
 */
-(WebServiceAPI*)Service_GetOriginalSleepRecords:(NSString*)userID password:(NSString *)password strID:(NSString *)strID withDelegate:(id<WebServiceAPIDelegate>)delegate;
@end

NS_ASSUME_NONNULL_END
