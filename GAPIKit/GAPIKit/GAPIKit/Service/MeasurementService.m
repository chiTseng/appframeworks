//
//  MeasurementService.m
//  GAPIKit
//
//  Created by elaine on 2018/5/29.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "MeasurementService.h"
#define PATH                            @"Measurement.asmx"

#define GETBEGININGDATA                 @"GetBeginingData"
#define GETMEASUREINFO                  @"GetMeasureInfo"
#define GETTARGETDATA                   @"GetTargetData"
#define UPLOADMEASURE                   @"UploadMeasure"
#define UPLOADMEASUREV2                 @"UploadMeasureV2"
#define UPLOADMEASUREV3                 @"UploadMeasureV3"
#define UPLOADMEASUREDATA               @"UploadMeasureData"
#define UPLOADMEASUREDATAV2             @"UploadMeasureDataV2"
#define UPLOADMEASUREDATAV3             @"UploadMeasureDataV3"
#define DELETERECORD                    @"DeleteRecord"
@implementation MeasurementService
-(WebServiceAPI*)Service_GetBeginingDataWithId:(NSString*)Id Password:(NSString*)password WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[self setPath:PATH]
                setAction:GETBEGININGDATA]
               addValue:Id key:@"id"]
              addValue:password key:@"password"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_GetMeasureInfoWithId:(NSString*)Id Password:(NSString*)password WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[self setPath:PATH]
                setAction:GETMEASUREINFO]
               addValue:Id key:@"id"]
              addValue:password key:@"password"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_GetTargetDataWithId:(NSString*)Id Password:(NSString*)password WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[self setPath:PATH]
                setAction:GETTARGETDATA]
               addValue:Id key:@"id"]
              addValue:password key:@"password"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_UploadMeasureWithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks OldmeasureDate:(NSString*)oldmeasureDate WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[[[[[[self setPath:PATH]
                      setAction:UPLOADMEASURE]
                     addValue:Id key:@"id"]
                    addValue:password key:@"password"]
                   addValue:measureDate key:@"measureDate"]
                  addValue:weight key:@"weight"]
                 addValue:bodyFat key:@"bodyFat"]
                addValue:waistline key:@"waistline"]
               addValue:buttocks key:@"buttocks"]
              addValue:oldmeasureDate key:@"oldmeasureDate"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_UploadMeasureV2WithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks LastUpdated:(NSString*)lastUpdated WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[[[[[[self setPath:PATH]
                      setAction:UPLOADMEASUREV2]
                     addValue:Id key:@"id"]
                    addValue:password key:@"password"]
                   addValue:measureDate key:@"measureDate"]
                  addValue:weight key:@"weight"]
                 addValue:bodyFat key:@"bodyFat"]
                addValue:waistline key:@"waistline"]
               addValue:buttocks key:@"buttocks"]
              addValue:lastUpdated key:@"LastUpdated"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_UploadMeasureV3WithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks RateOfBone:(NSString*)rateOfBone SoftLeanMass:(NSString*)softLeanMass TotalBodyWater:(NSString*)totalBodyWater WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[[[[[[[[self setPath:PATH]
                        setAction:UPLOADMEASUREV3]
                       addValue:Id key:@"id"]
                      addValue:password key:@"password"]
                     addValue:measureDate key:@"measureDate"]
                    addValue:weight key:@"weight"]
                   addValue:bodyFat key:@"bodyFat"]
                  addValue:waistline key:@"waistline"]
                 addValue:buttocks key:@"buttocks"]
                addValue:rateOfBone key:@"RateOfBone"]
               addValue:softLeanMass key:@"SoftLeanMass"]
              addValue:totalBodyWater key:@"TotalBodyWater"]
             setEnd]
            start:delegate];
    
}
-(WebServiceAPI*)Service_UploadMeasureDataWithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[[[[[self setPath:PATH]
                     setAction:UPLOADMEASUREDATA]
                    addValue:Id key:@"id"]
                   addValue:password key:@"password"]
                  addValue:measureDate key:@"measureDate"]
                 addValue:weight key:@"weight"]
                addValue:bodyFat key:@"bodyFat"]
               addValue:waistline key:@"waistline"]
              addValue:buttocks key:@"buttocks"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_UploadMeasureDataV2WithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks LastUpdated:(NSString*)lastUpdated WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[[[[[[self setPath:PATH]
                      setAction:UPLOADMEASUREDATAV2]
                     addValue:Id key:@"id"]
                    addValue:password key:@"password"]
                   addValue:measureDate key:@"measureDate"]
                  addValue:weight key:@"weight"]
                 addValue:bodyFat key:@"bodyFat"]
                addValue:waistline key:@"waistline"]
               addValue:buttocks key:@"buttocks"]
              addValue:lastUpdated key:@"LastUpdated"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_UploadMeasureDataV3WithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks RateOfBone:(NSString*)rateOfBone SoftLeanMass:(NSString*)softLeanMass TotalBodyWater:(NSString*)totalBodyWater WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[[[[[[[[self setPath:PATH]
                        setAction:UPLOADMEASUREDATAV3]
                       addValue:Id key:@"id"]
                      addValue:password key:@"password"]
                     addValue:measureDate key:@"measureDate"]
                    addValue:weight key:@"weight"]
                   addValue:bodyFat key:@"bodyFat"]
                  addValue:waistline key:@"waistline"]
                 addValue:buttocks key:@"buttocks"]
                addValue:rateOfBone key:@"RateOfBone"]
               addValue:softLeanMass key:@"SoftLeanMass"]
              addValue:totalBodyWater key:@"TotalBodyWater"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_DeleteRecordWithId:(NSString*)Id Password:(NSString*)password MServerId:(NSString*)mserverId WithDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[self setPath:PATH]
                 setAction:DELETERECORD]
                addValue:Id key:@"id"]
               addValue:password key:@"password"]
              addValue:mserverId key:@"MServerId"]
             setEnd]
            start:delegate];
}
@end
