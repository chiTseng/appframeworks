//
//  BaseSOAPService.h
//  GAPIKit
//
//  Created by elaine on 2018/5/29.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceAPI.h"
#import "WebServiceConstants.h"
@interface BaseSOAPService : NSObject
@property (retain,nonatomic) NSString *  WS_PROTOCOL;
@property (retain,nonatomic) NSString *  WS_HOST;
@property (retain,nonatomic) NSString *  WS_ROOTPATH;
-(BaseSOAPService*)setPath:(NSString*)path;
-(BaseSOAPService*)setAction:(NSString*)action;
-(BaseSOAPService*)addValue:(NSString*)value key:(NSString*)key;
-(BaseSOAPService*)setEnd;
-(WebServiceAPI*)start:(id<WebServiceAPIDelegate>)delegate;
-(void)testServer;
@end
