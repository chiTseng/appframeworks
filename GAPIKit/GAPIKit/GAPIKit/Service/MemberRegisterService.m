//
//  MemberRegisterService.m
//  GAPIKit
//
//  Created by elaine on 2018/12/13.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "MemberRegisterService.h"
#define PATH                            @"MemberRegister.asmx"
#define CHECK_USER_EXIST                @"CheckUserExist"
@implementation MemberRegisterService
-(WebServiceAPI*)Service_CheckUserExist:(NSString*)userID firstName:(NSString *)firstName birthday:(NSString *)birthday code:(NSString *)code withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[[self setPath:PATH] setAction:CHECK_USER_EXIST] addValue:userID key:@"Username"]
                addValue:firstName key:@"FirstName"]
               addValue:birthday key:@"Birthday"]
              addValue:code key:@"code"]
             setEnd]
            start:delegate];
}
@end
