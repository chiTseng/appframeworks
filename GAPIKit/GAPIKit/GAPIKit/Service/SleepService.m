//
//  SleepAuthenticateService.m
//  GAPIKit
//
//  Created by gsh_mac_2018 on 2019/7/3.
//  Copyright © 2019 GSH. All rights reserved.
//

#import "SleepService.h"
#define PATH                            @"Sleep.asmx"
#define GET_ORIGINALSLEEP_RECORDS       @"GetOriginalSleepRecords"
@implementation SleepService
-(WebServiceAPI*)Service_GetOriginalSleepRecords:(NSString*)userID password:(NSString *)password strID:(NSString *)strID withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[self setPath:PATH]
                 setAction:GET_ORIGINALSLEEP_RECORDS]
                addValue:userID key:@"id"]
               addValue:password key:@"password"]
              addValue:strID key:@"strId"]
             setEnd]
            start:delegate];
}
@end
