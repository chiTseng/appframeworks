//
//  BaseSOAPService.m
//  GAPIKit
//
//  Created by elaine on 2018/5/29.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "BaseSOAPService.h"
@interface BaseSOAPService()
@property (nonatomic,retain) NSString*strActionName;
@property (nonatomic,retain) NSString*strSOAPPath;
@property (nonatomic,retain) NSString*strSOAPAction;
@property (nonatomic,retain) NSString*strSOAPBody;
@end
@implementation BaseSOAPService
@synthesize WS_HOST,WS_PROTOCOL,WS_ROOTPATH;
@synthesize strSOAPPath,strSOAPAction,strSOAPBody,strActionName;
- (instancetype)init
{
    self = [super init];
    if (self) {
        //WS_HOST = @"cloud1.wowgohealth.com.tw";
        WS_HOST = @"app.wowgohealth.com.cn";
        WS_ROOTPATH =@"WebService";
        WS_PROTOCOL = @"https";
    }
    return self;
}
-(BaseSOAPService*)setPath:(NSString*)path
{
    strSOAPPath = path;
    return self;
}
-(BaseSOAPService*)setAction:(NSString*)action
{
    strActionName = action;
    strSOAPAction = [NSString stringWithFormat:@"http://tempuri.org/%@",strActionName];
    strSOAPBody = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                   "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
                   "<soap12:Body>"
                   "<%@ xmlns=\"http://tempuri.org/\">"
                   ,strActionName];
    return self;
}
-(BaseSOAPService*)addValue:(NSString*)value key:(NSString*)key
{
    strSOAPBody = [strSOAPBody stringByAppendingString:[NSString stringWithFormat:@"<%@>%@</%@>",key,value,key]];
    return self;
}
-(BaseSOAPService*)setEnd
{
    strSOAPBody = [strSOAPBody stringByAppendingString:[NSString stringWithFormat:@"</%@>"
                                          "</soap12:Body>"
                                          "</soap12:Envelope>",strActionName]];
    NSLog(@"path %@, \naction %@, \nbody %@",strSOAPPath,strSOAPAction,strSOAPBody);
    return self;
}
-(WebServiceAPI*)start:(id<WebServiceAPIDelegate>)delegate
{
    WebServiceAPI *wsApi = [[WebServiceAPI alloc] init];
    [wsApi callWebService:[NSString stringWithFormat:@"%@://%@/%@/%@", self.WS_PROTOCOL, self.WS_HOST,self.WS_ROOTPATH,  strSOAPPath] host:self.WS_HOST soapAction:strSOAPAction soapBody:strSOAPBody withDelegate:delegate];
    //[wsApi requestWebServiceWithURL:[NSString stringWithFormat:@"%@://%@/%@/%@", self.WS_PROTOCOL, self.WS_HOST,self.WS_ROOTPATH,  strSOAPPath] host:self.WS_HOST soapAction:strSOAPAction soapBody:strSOAPBody withDelegate:delegate];
    return wsApi;
}
-(void)testServer
{
    WebServiceAPI *wsApi = [[WebServiceAPI alloc] init];
    [wsApi testServer];
}
@end
