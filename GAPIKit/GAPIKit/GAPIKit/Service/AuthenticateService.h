//
//  AuthenticateService.h
//  GAPIKit
//
//  Created by elaine on 2018/5/29.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "BaseSOAPService.h"
#define GETACCOUNTANDPASSWORDBYEMAIL    @"GetAccountAndPasswordByEmail"
#define GETACCOUNTANDPASSWORDBYEMAILANDCUSTOMER @"GetAccountAndPasswordByEmailAndCustomer"
#define SENDEMAILTORESETPASSWORDV2      @"SendEmailToResetPasswordV2"
#define LOGIN                           @"Login"
#define LOGINV2                         @"LoginV2"
#define DIETITIANLOGIN                  @"LoginByDietitians"
#define DIETITIANLOGINV2                @"LoginByDietitiansV2"
#define DIETITIANLOGINV3                @"LoginByDietitiansV3"
#define LOGINBYCUSTOMER                 @"LoginByCustomer"
#define LOGINBYCUSTOMERV2               @"LoginByCustomerV2"
#define LOGINBYCUSTOMERV3               @"LoginByCustomerV3"
#define GETUNAUTHORISED                 @"GetUnauthorised"
#define USERLOGINBYNAME                 @"UserLoginByNameV2"
#define GETSERIALNUMBER                 @"GetSerialNumber"
/**3.認證管理服務*/
@interface AuthenticateService : BaseSOAPService
/**
 *@brief 利用email查詢密碼
 （int）0=成功,1=Exception,2=ID/密碼錯誤,3=沒有減重計畫 Plan
 */
-(WebServiceAPI*)Service_GetAccountAndPasswordByEmail:(NSString*)email withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 利用email查詢密碼 並根據customerID 區分
 回傳值true 代表寄出成功  false代表寄出失敗
 */
-(WebServiceAPI*)Service_GetAccountAndPasswordByEmailAndCustomerWithEmail:(NSString*)email CustomerID:(NSString*)customerId withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 利用email查詢密碼 並根據customerID 區分  (新增語係)
 回傳值true 代表寄出成功  false代表寄出失敗
 */
-(WebServiceAPI*)Service_SendEmailToResetPasswordV2WithEmail:(NSString*)email Language:(NSString*)language CustomerId:(NSString*)customerId withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 使用者登入 不考慮customerID
 （int）0=成功,1=Exception,2=ID/密碼錯誤3=沒有減重計畫 Plan/減重計畫過期, 4=帳號未啟用, 5=使用者不存在
 */
-(WebServiceAPI*)Service_LoginWithUserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 使用者登入V2 不考慮customerID
 （int）0=成功,1=Exception,2=ID/密碼錯誤3=沒有減重計畫 Plan/減重計畫過期, 4=帳號未啟用, 5=使用者不存在 6=帳號鎖住
 */
-(WebServiceAPI*)Service_LoginV2WithUserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 使用者登入 區分customerID
 （int）0=成功,1=Exception,2=ID/密碼錯誤3=沒有減重計畫 Plan/減重計畫過期, 4=帳號未啟用, 5=使用者不存在
 */
-(WebServiceAPI*)Service_LoginByCustomerID:(NSString*)customerID UserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 使用者登入V2 區分customerID
 （int）0=成功,1=Exception,2=ID/密碼錯誤3=沒有減重計畫 Plan/減重計畫過期, 4=帳號未啟用, 5=使用者不存在 6=帳號鎖住
 */
-(WebServiceAPI*)Service_LoginByCustomerIDV2:(NSString*)customerID UserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 使用者登入V3 區分customerID (新增判斷是否屬於同一個母公司)
 （int）0=成功,1=Exception,2=ID/密碼錯誤3=沒有減重計畫 Plan/減重計畫過期, 4=帳號未啟用, 5=使用者不存在 6=帳號鎖住
 */
-(WebServiceAPI*)Service_LoginByCustomerIDV3:(NSString*)customerID UserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 營養師登入
 （int）0=成功,1=Exception,2=ID/密碼錯誤
 */
-(WebServiceAPI*)Service_LoginByDietitiansWithUserAccount:(NSString*)userAccount Password:(NSString*)password CustomerID:(NSString*)customerID withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 營養師登入v2
 （int）0=成功,1=Exception,2=ID/密碼錯誤
 out customerId , serialNumber
 */
-(WebServiceAPI*)Service_LoginByDietitiansV2WithUserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 營養師登入v3 (泛用型HCare 使用)
 （int）0=成功,1=Exception,2=ID/密碼錯誤
 out customerId , serialNumber
 */
-(WebServiceAPI*)Service_LoginByDietitiansV3WithUserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 查詢停權原因
 （int）0=成功,1=Exception,2=ID/密碼錯誤
 out message 停權原因
 */
-(WebServiceAPI*)Service_GetUnauthorisedWithUserAccount:(NSString*)userAccount withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief User QRCode 登入用 userAccount 及 password 只營養師帳號
 （int）0=成功,1=Exception,2=ID/密碼錯誤,4=使用者不屬於此管理者
 */
-(WebServiceAPI*)Service_UserLoginByName:(NSString*)userAccount Password:(NSString*)password Username:(NSString*)username withDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 取得序號列表
 （int）0=成功,1=Exception
 out
 (jsonSerialNumber包裝成JSON format
 Json欄位    型態    說明
 SPName    String    客戶名稱
 SerialNumber    String    客戶序號)

 */
-(WebServiceAPI*)Service_GetSerialNumberWithCustomerId:(NSString*)customerId withDelegate:(id<WebServiceAPIDelegate>)delegate;
@end
