//
//  AuthenticateService.m
//  GAPIKit
//
//  Created by elaine on 2018/5/29.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "AuthenticateService.h"
#define PATH                            @"Authenticate.asmx"

@implementation AuthenticateService
-(WebServiceAPI*)Service_GetAccountAndPasswordByEmail:(NSString*)email withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[self setPath:PATH]
               setAction:GETACCOUNTANDPASSWORDBYEMAIL]
              addValue:email key:@"email"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_GetAccountAndPasswordByEmailAndCustomerWithEmail:(NSString*)email CustomerID:(NSString*)customerId withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[self setPath:PATH]
                setAction:GETACCOUNTANDPASSWORDBYEMAILANDCUSTOMER]
               addValue:email key:@"email"]
              addValue:customerId key:@"customerId"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_SendEmailToResetPasswordV2WithEmail:(NSString*)email Language:(NSString*)language CustomerId:(NSString*)customerId withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[self setPath:PATH]
                 setAction:SENDEMAILTORESETPASSWORDV2]
                addValue:email key:@"email"]
               addValue:language key:@"Language"]
              addValue:customerId key:@"customerId"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_LoginWithUserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[self setPath:PATH]
                setAction:LOGIN]
               addValue:userAccount key:@"userAccount"]
              addValue:password key:@"password"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_LoginV2WithUserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[self setPath:PATH]
                setAction:LOGINV2]
               addValue:userAccount key:@"userAccount"]
              addValue:password key:@"password"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_LoginByCustomerID:(NSString*)customerID UserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[self setPath:PATH]
                 setAction:LOGINBYCUSTOMER]
                addValue:userAccount key:@"userAccount"]
               addValue:password key:@"password"]
              addValue:customerID key:@"customerId"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_LoginByCustomerIDV2:(NSString*)customerID UserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[self setPath:PATH]
                 setAction:LOGINBYCUSTOMERV2]
                addValue:userAccount key:@"userAccount"]
               addValue:password key:@"password"]
              addValue:customerID key:@"customerId"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_LoginByCustomerIDV3:(NSString*)customerID UserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[self setPath:PATH]
                 setAction:LOGINBYCUSTOMERV3]
                addValue:userAccount key:@"userAccount"]
               addValue:password key:@"password"]
              addValue:customerID key:@"customerId"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_LoginByDietitiansWithUserAccount:(NSString*)userAccount Password:(NSString*)password CustomerID:(NSString*)customerID withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[self setPath:PATH]
                 setAction:DIETITIANLOGIN]
                addValue:userAccount key:@"userAccount"]
               addValue:password key:@"password"]
              addValue:customerID key:@"customerId"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_LoginByDietitiansV2WithUserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[self setPath:PATH]
                setAction:DIETITIANLOGINV2]
               addValue:userAccount key:@"userAccount"]
              addValue:password key:@"password"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_LoginByDietitiansV3WithUserAccount:(NSString*)userAccount Password:(NSString*)password withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[self setPath:PATH]
                setAction:DIETITIANLOGINV3]
               addValue:userAccount key:@"userAccount"]
              addValue:password key:@"password"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_GetUnauthorisedWithUserAccount:(NSString*)userAccount withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[self setPath:PATH]
               setAction:GETUNAUTHORISED]
              addValue:userAccount key:@"UserAccount"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_UserLoginByName:(NSString*)userAccount Password:(NSString*)password Username:(NSString*)username withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[[[self setPath:PATH]
                 setAction:USERLOGINBYNAME]
                addValue:userAccount key:@"userAccount"]
               addValue:password key:@"password"]
              addValue:username key:@"Username"]
             setEnd]
            start:delegate];
}
-(WebServiceAPI*)Service_GetSerialNumberWithCustomerId:(NSString*)customerId withDelegate:(id<WebServiceAPIDelegate>)delegate
{
    return [[[[[self setPath:PATH]
               setAction:GETSERIALNUMBER]
              addValue:customerId key:@"customerId"]
             setEnd]
            start:delegate];
}
@end
