//
//  MeasurementService.h
//  GAPIKit
//
//  Created by elaine on 2018/5/29.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import "BaseSOAPService.h"
/**7.減重管理服務*/
@interface MeasurementService : BaseSOAPService
/**
 *@brief 取得期初量測值
 （int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
out weight, bodyFat, waistline, buttocks
 */
-(WebServiceAPI*)Service_GetBeginingDataWithId:(NSString*)Id Password:(NSString*)password WithDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief IPhone 取得量測資訊
 （int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
 out infoHtml 運動紀錄資料摘要HTML格式含排版
 */
-(WebServiceAPI*)Service_GetMeasureInfoWithId:(NSString*)Id Password:(NSString*)password WithDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 取得目標資料
 （int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
 out 文件沒說會out什麼
 */
-(WebServiceAPI*)Service_GetTargetDataWithId:(NSString*)Id Password:(NSString*)password WithDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 上傳量測紀錄 （第一版）
 （int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
 out 文件沒說會out什麼
 */
-(WebServiceAPI*)Service_UploadMeasureWithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks OldmeasureDate:(NSString*)oldmeasureDate WithDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 上傳量測紀錄 （第二版）
 （int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
 out 文件沒說會out什麼
 */
-(WebServiceAPI*)Service_UploadMeasureV2WithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks LastUpdated:(NSString*)lastUpdated WithDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
*@brief 上傳量測紀錄 （第三版: 加入骨骼率(%)、肌肉量(kg)、總含水量(%)）
（int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
out serverId
*/
-(WebServiceAPI*)Service_UploadMeasureV3WithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks RateOfBone:(NSString*)rateOfBone SoftLeanMass:(NSString*)softLeanMass TotalBodyWater:(NSString*)totalBodyWater WithDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 上傳量測紀錄 (第一版)
 int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
 out 文件沒說會out什麼
 */
-(WebServiceAPI*)Service_UploadMeasureDataWithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks WithDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 上傳量測紀錄 (第二版)
 int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
 out 文件沒說會out什麼
 */
-(WebServiceAPI*)Service_UploadMeasureDataV2WithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks LastUpdated:(NSString*)lastUpdated WithDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 上傳量測紀錄 (第三版: 加入骨骼率(%)、肌肉量、總含水量(%))
 int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
 out 文件沒說會out什麼
 */
-(WebServiceAPI*)Service_UploadMeasureDataV3WithId:(NSString*)Id Password:(NSString*)password MeasureDate:(NSString*)measureDate Weight:(NSString*)weight BodyFat:(NSString*)bodyFat Waistline:(NSString*)waistline Buttocks:(NSString*)buttocks RateOfBone:(NSString*)rateOfBone SoftLeanMass:(NSString*)softLeanMass TotalBodyWater:(NSString*)totalBodyWater WithDelegate:(id<WebServiceAPIDelegate>)delegate;
/**
 *@brief 刪除體重紀錄
 int）0=成功,1=Exception,2=ID/密碼錯誤,4=沒有減重Plan
 out 文件沒說會out什麼
 */
-(WebServiceAPI*)Service_DeleteRecordWithId:(NSString*)Id Password:(NSString*)password MServerId:(NSString*)mserverId WithDelegate:(id<WebServiceAPIDelegate>)delegate;
@end
