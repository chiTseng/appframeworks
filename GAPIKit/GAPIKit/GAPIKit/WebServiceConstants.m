#import "WebServiceConstants.h"

#pragma mark - SPID
//指定客戶
#ifdef NTUT
NSString * const WS_CUSTOMERLOGINID = @"92"; //NTUT
#elif THU
NSString * const WS_CUSTOMERLOGINID = @"93"; //THU
#elif ARCADYAN
NSString * const WS_CUSTOMERLOGINID = @"95"; //ARCADYAN
#elif AFC
NSString * const WS_CUSTOMERLOGINID = @"106"; //AFC
#elif PL2015
NSString * const WS_CUSTOMERLOGINID = @"110"; //PL
#elif GSH2015
NSString * const WS_CUSTOMERLOGINID = @"115"; //gshTarget
#elif HKEG
NSString * const WS_CUSTOMERLOGINID = @"118"; //HKEG
#elif ELDER2015
NSString * const WS_CUSTOMERLOGINID = @"119"; //ELDER2015
#elif ATTEND2015
NSString * const WS_CUSTOMERLOGINID = @"123"; //ATTEND2015
#elif CLIPTEC2015
NSString * const WS_CUSTOMERLOGINID = @"126";
#elif EXPOPARK2015
NSString * const WS_CUSTOMERLOGINID = @"125";
#elif HERAN2015
NSString * const WS_CUSTOMERLOGINID = @"127"; //Heran
#elif ZSRC2015
NSString * const WS_CUSTOMERLOGINID = @"129"; //ZSRC
#elif SEE2015
NSString * const WS_CUSTOMERLOGINID = @"131"; //See
#elif FAS2015
NSString * const WS_CUSTOMERLOGINID = @"132"; //Fas
#elif ANTIFAT2015
NSString * const WS_CUSTOMERLOGINID = @"136"; //Antifat
#elif EPI2015
NSString * const WS_CUSTOMERLOGINID = @"138"; //中國醫藥大學
#elif TEN2015
NSString * const WS_CUSTOMERLOGINID = @"140"; //TEN2015
#elif GUMT2015
NSString * const WS_CUSTOMERLOGINID = @"141"; //GUMT2015
#elif KNOVIA2015
NSString * const WS_CUSTOMERLOGINID = @"142"; //KNOVIA2015
#elif FLC2015
NSString * const WS_CUSTOMERLOGINID = @"145"; //FLC2015
#elif MYT2015
NSString * const WS_CUSTOMERLOGINID = @"146"; //MYT2015
#elif ACER2015
NSString * const WS_CUSTOMERLOGINID = @"147"; //ACER2015
#elif NTHU2015
NSString * const WS_CUSTOMERLOGINID = @"148"; //NTHU2015
#elif EVER2015
NSString * const WS_CUSTOMERLOGINID = @"134"; //EVER2015
#else
NSString * const WS_CUSTOMERLOGINID = @"72";
#endif


#pragma mark - HOST & PROTOCOL
NSString * const WS_PROTOCOL = @"http";
#ifdef TESTSERVER
NSString * const WS_HOST = @"gshtest.wowgohealth.com.tw";//test
//NSString * const WS_HOST = @"220.130.45.207";//test
//NSString *const WS_HOST = @"wowgohealth.azurewebsites.net";//Azure
//NSString * const WS_HOST = @"cloud1.wowgohealth.com.tw";//正式區

NSString * const WS_ROOTPATH = @"WebService";//正式區
//NSString * const WS_ROOTPATH = @"webserviceazure";//正式區Azure
//NSString * const WS_ROOTPATH = @"webservice";//Azure
#else
NSString * const WS_HOST = @"cloud1.wowgohealth.com.tw";
NSString * const WS_ROOTPATH = @"WebService";
#endif

#pragma mark - AUTHENTICATE
NSString * const WS_AUTHENTICATE_PATH = @"Authenticate.asmx";

NSString * const WS_GETACCOUNTANDPASSWORDBYEMAIL_SOAP_ACTION = @"http://tempuri.org/GetAccountAndPasswordByEmail";
NSString * const WS_GETACCOUNTANDPASSWORDBYEMAIL_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<GetAccountAndPasswordByEmail xmlns=\"http://tempuri.org/\">"
"<email>%@</email>"
"</GetAccountAndPasswordByEmail>"
"</soap12:Body>"
"</soap12:Envelope>";

NSString * const WS_LOGIN_SOAP_ACTION = @"http://tempuri.org/Login";
NSString * const WS_LOGIN_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<Login xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"</Login>"
"</soap12:Body>"
"</soap12:Envelope>";

//LoginByDietitians
NSString * const WS_DIETITIAN_LOGIN_SOAP_ACTION = @"http://tempuri.org/LoginByDietitians";
NSString * const WS_DIETITIAN_LOGIN_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<LoginByDietitians xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<customerId>%@</customerId>"
"</LoginByDietitians>"
"</soap12:Body>"
"</soap12:Envelope>";

//@2015/12/10
//LoginByDietitiansV2
NSString * const WS_DIETITIAN_LOGIN_V2_SOAP_ACTION = @"http://tempuri.org/LoginByDietitiansV2";
NSString * const WS_DIETITIAN_LOGIN_V2_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<LoginByDietitiansV2 xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"</LoginByDietitiansV2>"
"</soap12:Body>"
"</soap12:Envelope>";

//@2016/06/28
//LoginByDietitiansV3
NSString * const WS_DIETITIAN_LOGIN_V3_SOAP_ACTION = @"http://tempuri.org/LoginByDietitiansV3";
NSString * const WS_DIETITIAN_LOGIN_V3_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<LoginByDietitiansV3 xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"</LoginByDietitiansV3>"
"</soap12:Body>"
"</soap12:Envelope>";

NSString * const WS_CUSTOMERLOGIN_SOAP_ACTION = @"http://tempuri.org/LoginByCustomer";
NSString * const WS_CUSTOMERLOGIN_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<LoginByCustomer xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<customerId>%@</customerId>"
"</LoginByCustomer>"
"</soap12:Body>"
"</soap12:Envelope>";

NSString * const WS_USERLOGINBYNAME_SOAP_ACTION = @"http://tempuri.org/UserLoginByName";
NSString * const WS_USERLOGINBYNAME_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<UserLoginByName xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<Username>%@</Username>"
"</UserLoginByName>"
"</soap12:Body>"
"</soap12:Envelope>";


#pragma mark - Measurement
NSString * const WS_MEASUREMENT_PATH = @"Measurement.asmx";

NSString * const WS_UPLOADMEASURE_SOAP_ACTION = @"http://tempuri.org/UploadMeasure";
NSString * const WS_UPLOADMEASURE_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<UploadMeasure xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<measureDate>%@</measureDate>"
"<weight>%@</weight>"
"<bodyFat>%@</bodyFat>"
"<waistline>%@</waistline>"
"<buttocks>%@</buttocks>"
"<oldmeasureDate>%@</oldmeasureDate>"
"</UploadMeasure>"
"</soap12:Body>"
"</soap12:Envelope>";

//@2016/04/14
/*
NSString * const WS_MEASUREMENT_ADDRECORD_V2_SOAP_ACTION = @"http://tempuri.org/AddRecordV2";
NSString * const WS_MEASUREMENT_ADDRECORD_V2_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRecordV2 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<measureDate>%@</measureDate>"
"<weight>%@</weight>"
"<bodyFat>%@</bodyFat>"
"<waistline>%@</waistline>"
"<buttocks>%@</buttocks>"
"<RateOfBone>%@</RateOfBone>"
"<SoftLeanMass>%@</SoftLeanMass>"
"<TotalBodyWater>%@</TotalBodyWater>"
"<Impedance>%@</Impedance>"
"</AddRecordV2>"
"</soap12:Body>"
"</soap12:Envelope>";
*/
/*
//@2016/08/16
NSString * const WS_MEASUREMENT_ADDRECORD_V3_SOAP_ACTION = @"http://tempuri.org/AddRecordV3";
NSString * const WS_MEASUREMENT_ADDRECORD_V3_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRecordV3 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<measureDate>%@</measureDate>"
"<weight>%@</weight>"
"<bodyFat>%@</bodyFat>"
"<waistline>%@</waistline>"
"<buttocks>%@</buttocks>"
"<RateOfBone>%@</RateOfBone>"
"<SoftLeanMass>%@</SoftLeanMass>"
"<TotalBodyWater>%@</TotalBodyWater>"
"<Impedance>%@</Impedance>"
"<BMR>%@</BMR>"
"<VisceralFat>%@</VisceralFat>"
"<PhysicalAge>%@</PhysicalAge>"
"</AddRecordV3>"
"</soap12:Body>"
"</soap12:Envelope>";
//@2016/08/16
*/

//@2017/06/09
NSString * const WS_MEASUREMENT_ADDRECORD_V5_SOAP_ACTION = @"http://tempuri.org/AddRecordV5";
NSString * const WS_MEASUREMENT_ADDRECORD_V5_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRecordV5 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<measureDate>%@</measureDate>"
"<weight>%@</weight>"
"<bodyFat>%@</bodyFat>"
"<waistline>%@</waistline>"
"<buttocks>%@</buttocks>"
"<RateOfBone>%@</RateOfBone>"
"<SoftLeanMass>%@</SoftLeanMass>"
"<TotalBodyWater>%@</TotalBodyWater>"
"<Impedance>%@</Impedance>"
"<BMR>%@</BMR>"
"<VisceralFat>%@</VisceralFat>"
"<PhysicalAge>%@</PhysicalAge>"
"<MacAddress>%@</MacAddress>"
"<DeviceType>%@</DeviceType>"
"<autoMeasure>%@</autoMeasure>"
"<place>%@</place>"
"</AddRecordV5>"
"</soap12:Body>"
"</soap12:Envelope>";

/*
//@2015/07/10 upload water/muscle/bone data
NSString * const WS_UPLOADMEASURE_SOAP_ACTION_V3 = @"http://tempuri.org/UploadMeasureV3";
NSString * const WS_UPLOADMEASURE_SOAP_BODY_V3 =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<UploadMeasureV3 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<measureDate>%@</measureDate>"
"<weight>%@</weight>"
"<bodyFat>%@</bodyFat>"
"<waistline>%@</waistline>"
"<buttocks>%@</buttocks>"
"<RateOfBone>%@</RateOfBone>"
"<SoftLeanMass>%@</SoftLeanMass>"
"<TotalBodyWater>%@</TotalBodyWater>"
"<oldmeasureDate>%@</oldmeasureDate>"
"</UploadMeasureV3>"
"</soap12:Body>"
"</soap12:Envelope>";
//@2015/07/10 upload water/muscle/bone data
*/

#pragma mark - Sport
NSString * const WS_SPORT_PATH = @"Sport.asmx";

//add by Tina 2012/5/4 加照片功能
NSString * const WS_UPLOAD_IMG_SPORT_SOAP_ACTION = @"http://tempuri.org/UploadSport";
NSString * const WS_UPLOAD_IMG_SPORT_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<UploadSport xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<sportType>%@</sportType>"
"<sportMinutes>%@</sportMinutes>"
"<recordTime>%@</recordTime>"
"<memo>%@</memo>"
"<sportId>%d</sportId>"
"%@"
"</UploadSport>"
"</soap:Body>"
"</soap:Envelope>";

//add by yue 20140305
NSString * const WS_UPLOAD_IMG_SPORT_SOAP_ACTION_V2 = @"http://tempuri.org/UploadSportV2";
NSString * const WS_UPLOAD_IMG_SPORT_SOAP_BODY_V2 =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<UploadSportV2 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<sportType>%@</sportType>"
"<sportMinutes>%@</sportMinutes>"
"<recordTime>%@</recordTime>"
"<memo>%@</memo>"
"<distance>%@</distance>"
"<footSteps>%@</footSteps>"
"<calorie>%@</calorie>"
"<autoMeasure>%@</autoMeasure>"
"<sportId>%d</sportId>"
"%@"
"</UploadSportV2>"
"</soap:Body>"
"</soap:Envelope>";


//@2015/09/25
NSString * const WS_ADD_SPORT_RECORD_SOAP_ACTION = @"http://tempuri.org/AddRecord";
NSString * const WS_ADD_SPORT_RECORD_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRecord xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<sportType>%@</sportType>"
"<sportMinutes>%@</sportMinutes>"
"<recordTime>%@</recordTime>"
"<memo>%@</memo>"
"<distance>%@</distance>"
"<footSteps>%@</footSteps>"
"<calorie>%@</calorie>"
"<autoMeasure>%@</autoMeasure>"
"%@"
"</AddRecord>"
"</soap12:Body>"
"</soap12:Envelope>";
//@2015/09/25
/*
//@2015/10/30 JSON pedo record upload API
NSString * const WS_ADD_RING_SPORT_RECORDS_SOAP_ACTION = @"http://tempuri.org/AddRingRecords";
NSString * const WS_ADD_RING_SPORT_RECORDS_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRingRecords xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<sportType>%@</sportType>"
"<recordData>%@</recordData>"
"<memo>%@</memo>"
"<autoMeasure>%@</autoMeasure>"
"</AddRingRecords>"
"</soap12:Body>"
"</soap12:Envelope>";
//@2015/10/30
*/

//@2017/06/09
NSString * const WS_ADD_RING_SPORT_RECORDS_V3_SOAP_ACTION = @"http://tempuri.org/AddRingRecordsV3";
NSString * const WS_ADD_RING_SPORT_RECORDS_V3_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRingRecordsV3 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<sportType>%@</sportType>"
"<recordData>%@</recordData>"
"<memo>%@</memo>"
"<autoMeasure>%@</autoMeasure>"
"<MacAddress>%@</MacAddress>"
"<DeviceType>%@</DeviceType>"
"<place>%@</place>"
"</AddRingRecordsV3>"
"</soap12:Body>"
"</soap12:Envelope>";


#pragma mark - HistoryRecord
NSString * const WS_HISTORY_PATH = @"HistoryRecord.asmx";

NSString * const WS_GET_NEW_USER_KEY_ACTION = @"http://tempuri.org/GetNewUserKeyV3";
NSString * const WS_GET_NEW_USER_KEY_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<GetNewUserKeyV3 xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<LastUpdated>%@</LastUpdated>"
"</GetNewUserKeyV3>"
"</soap12:Body>"
"</soap12:Envelope>";

//@2015/04/30 new GetNewUserKeyV2 API
NSString * const WS_GET_NEW_USER_KEY_ACTION_V2 = @"http://tempuri.org/GetNewUserKeyV2";
NSString * const WS_GET_NEW_USER_KEY_BODY_V2 =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<GetNewUserKeyV2 xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<userkey>%@</userkey>"
"</GetNewUserKeyV2>"
"</soap12:Body>"
"</soap12:Envelope>";
//@2015/04/30 new GetNewUserKeyV2 API

#pragma mark - Plan
NSString * const WS_USER_PLAN_PATH = @"Plan.asmx";

NSString * const WS_UPDATE_PLAN_SOAP_ACTION = @"http://tempuri.org/UpdateMemberPlan";
NSString * const WS_UPDATE_PLAN_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<UpdateMemberPlan xmlns=\"http://tempuri.org/\">"
"<Username>%@</Username>"
"<Password>%@</Password>"
"<memberPlanId>%d</memberPlanId>"
"<age>%d</age>"
"<gender>%d</gender>"
"<height>%f</height>"
"<startDate>%@</startDate>"
"<endDate>%@</endDate>"
"<FtWeight>%f</FtWeight>"
"<TgWeight>%f</TgWeight>"
"<FtBodyFat>%f</FtBodyFat>"
"<TgBodyFat>%f</TgBodyFat>"
//"<FtWaistline>%f</FtWaistline>"
//"<TgWaistline>%f</TgWaistline>"
//"<FtButtocks>%f</FtButtocks>"
//"<TgButtocks>%f</TgButtocks>"
//"<sportCount>%d</sportCount>"
//"<sportOneMin>%d</sportOneMin>"
//"<FtBodyFat>1</FtBodyFat>"
//"<TgBodyFat>1</TgBodyFat>"
"<FtWaistline>1</FtWaistline>"
"<TgWaistline>1</TgWaistline>"
"<FtButtocks>1</FtButtocks>"
"<TgButtocks>1</TgButtocks>"
"<sportCount>1</sportCount>"
"<sportOneMin>1</sportOneMin>"
"<breakfristST>1900-01-01T06:00:00+08:00</breakfristST>"
"<breakfristET>1900-01-01T08:00:00+08:00</breakfristET>"
"<lunchST>1900-01-01T11:00:00+08:00</lunchST>"
"<lunchET>1900-01-01T13:00:00+08:00</lunchET>"
"<dinnerST>1900-01-01T17:00:00+08:00</dinnerST>"
"<dinnerET>1900-01-01T19:00:00+08:00</dinnerET>"
"<forbiddenST1>1900-01-01T20:00:00+08:00</forbiddenST1>"
"<forbiddenET1>1900-01-01T06:00:00+08:00</forbiddenET1>"
"<forbiddenST2>1900-01-01T20:00:00+08:00</forbiddenST2>"
"<forbiddenET2>1900-01-01T06:00:00+08:00</forbiddenET2>"
"<activity>%d</activity>"
"<reducingCalorie>%d</reducingCalorie>"
"</UpdateMemberPlan>"
"</soap:Body>"
"</soap:Envelope>";

NSString * const WS_GET_PLAN_SOAP_ACTION = @"http://tempuri.org/GetMemberPlan";
NSString * const WS_GET_PLAN_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<GetMemberPlan xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"</GetMemberPlan>"
"</soap:Body>"
"</soap:Envelope>";

NSString * const WS_GET_GENDER_SOAP_ACTION = @"http://tempuri.org/GetGender";
NSString * const WS_GET_GENDER_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<GetGender xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"</GetGender>"
"</soap:Body>"
"</soap:Envelope>";

#pragma mark - BloodPreasure
NSString * const WS_BPDATA_PATH = @"BloodPreasure.asmx";
/*
NSString * const WS_ADDBPDATA_SOAP_ACTION = @"http://tempuri.org/AddRecord";
NSString * const WS_ADDBPDATA_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRecord xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<recordTime>%@</recordTime>"
"<systolic>%d</systolic>"
"<diastolic>%d</diastolic>"
"<pulse>%d</pulse>"
"<typeID>%d</typeID>"//set to zero according to android
"<memo>%@</memo>"
"</AddRecord>"
"</soap12:Body>"
"</soap12:Envelope>";
*/
/*
//2016/08/16
NSString * const WS_ADDBPDATA_V2_SOAP_ACTION = @"http://tempuri.org/AddRecordV2";
NSString * const WS_ADDBPDATA_V2_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRecordV2 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<recordTime>%@</recordTime>"
"<systolic>%d</systolic>"
"<diastolic>%d</diastolic>"
"<pulse>%d</pulse>"
"<irregularPulseDetectionFlag>%d</irregularPulseDetectionFlag>"
"<typeID>%d</typeID>"//set to zero according to android
"<memo>%@</memo>"
"</AddRecordV2>"
"</soap12:Body>"
"</soap12:Envelope>";
*/
//@2017/06/09
NSString * const WS_ADDBPDATA_V5_SOAP_ACTION = @"http://tempuri.org/AddRecordV5";
NSString * const WS_ADDBPDATA_V5_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRecordV5 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<recordTime>%@</recordTime>"
"<systolic>%d</systolic>"
"<diastolic>%d</diastolic>"
"<pulse>%d</pulse>"
"<irregularPulseDetectionFlag>%d</irregularPulseDetectionFlag>"
"<typeID>%d</typeID>"//set to zero according to android
"<memo>%@</memo>"
"<MacAddress>%@</MacAddress>"
"<DeviceType>%@</DeviceType>"
"<autoMeasure>%@</autoMeasure>"
"<place>%@</place>"
"</AddRecordV5>"
"</soap12:Body>"
"</soap12:Envelope>";

NSString * const WS_UPDATEBPDATA_SOAP_ACTION = @"http://tempuri.org/UpdateRecord";
NSString * const WS_UPDATEBPDATA_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<UpdateRecord xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<serverId>%d</serverId>"
"<recordTime>%@</recordTime>"
"<systolic>%d</systolic>"
"<diastolic>%d</diastolic>"
"<pulse>%d</pulse>"
"<typeID>0</typeID>"//set to zero according to android
"<memo>%@</memo>"
"</UpdateRecord>"
"</soap12:Body>"
"</soap12:Envelope>";

#pragma mark - MemberRegister
NSString * const WS_REGISTER_USER_PATH = @"MemberRegister.asmx";

//Jora add code i.e serial
NSString * const WS_CHECK_USER_EXIST_SOAP_ACTION = @"http://tempuri.org/CheckUserExist";
NSString * const WS_CHECK_USER_EXIST_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<CheckUserExist xmlns=\"http://tempuri.org/\">"
"<Username>%@</Username>"
"<FirstName>%@</FirstName>"
"<Birthday>%@</Birthday>"
"<code>%@</code>"
"</CheckUserExist>"
"</soap:Body>"
"</soap:Envelope>";

NSString * const WS_UPLOAD_PHOTO_ACTION = @"http://tempuri.org/UploadProfileImageToTemp";
NSString * const WS_UPLOAD_PHOTO_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<UploadProfileImageToTemp xmlns=\"http://tempuri.org/\">"
"<pic>%@</pic>"
"<fileName>%@</fileName>"
"</UploadProfileImageToTemp>"
"</soap:Body>"
"</soap:Envelope>";

//2016/06/28
NSString * const WS_REGISTER_USER_ACTION = @"http://tempuri.org/CreateUserNoVerification";
NSString * const WS_REGISTER_USER_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<CreateUserNoVerification xmlns=\"http://tempuri.org/\">"
"<Username>%@</Username>"
"<FirstName>%@</FirstName>"
"<DisplayName>%@</DisplayName>"
"<isMale>%@</isMale>"
"<isAgreement>%@</isAgreement>"
"<Cell>%@</Cell>"
"<Mail>%@</Mail>"
"<Password>%@</Password>"
"<TempPhoto>%@</TempPhoto>"
"<Birthday>%@</Birthday>"
"<Add_secCode>%d</Add_secCode>"
"<code>%@</code>"
"</CreateUserNoVerification>"
"</soap:Body>"
"</soap:Envelope>";
/*
 NSString * const WS_REGISTER_USER_ACTION = @"http://tempuri.org/CreateNewUser";
NSString * const WS_REGISTER_USER_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<CreateNewUser xmlns=\"http://tempuri.org/\">"
"<Username>%@</Username>"
"<FirstName>%@</FirstName>"
"<DisplayName>%@</DisplayName>"
"<isMale>%@</isMale>"
"<isAgreement>%@</isAgreement>"
"<Cell>%@</Cell>"
"<Mail>%@</Mail>"
"<Password>%@</Password>"
"<TempPhoto>%@</TempPhoto>"
"<Birthday>%@</Birthday>"
"<Add_secCode>%d</Add_secCode>"
"<code>%@</code>"
"</CreateNewUser>"
"</soap:Body>"
"</soap:Envelope>";
 */

NSString * const WS_GET_USER_INFO_ACTION = @"http://tempuri.org/GetUserInfo";
NSString * const WS_GET_USER_INFO_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<GetUserInfo xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"</GetUserInfo>"
"</soap:Body>"
"</soap:Envelope>";

NSString * const WS_UPDATE_USER_INFO_ACTION = @"http://tempuri.org/UpdateUser";
NSString * const WS_UPDATE_USER_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<UpdateUser xmlns=\"http://tempuri.org/\">"
"<Username>%@</Username>"
"<FirstName>%@</FirstName>"
"<DisplayName>%@</DisplayName>"
"<isMale>%@</isMale>"
"<Cell>%@</Cell>"
"<Mail>%@</Mail>"
"<Password>%@</Password>"
"<TempPhoto>%@</TempPhoto>"
"<oldPwd>%@</oldPwd>"
"<Birthday>%@</Birthday>"
"<Add_sec>%d</Add_sec>"
"</UpdateUser>"
"</soap:Body>"
"</soap:Envelope>";

NSString * const WS_UPDATE_USER_NO_PASSWORD_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<UpdateUser xmlns=\"http://tempuri.org/\">"
"<Username>%@</Username>"
"<FirstName>%@</FirstName>"
"<DisplayName>%@</DisplayName>"
"<isMale>%@</isMale>"
"<Cell>%@</Cell>"
"<Mail>%@</Mail>"
"<TempPhoto>%@</TempPhoto>"
"<oldPwd>%@</oldPwd>"
"<Birthday>%@</Birthday>"
"<Add_sec>%d</Add_sec>"
"</UpdateUser>"
"</soap:Body>"
"</soap:Envelope>";

NSString * const WS_DELETE_PROFILE_IMAGE_ACTION = @"http://tempuri.org/DeleteProfileImage";
NSString * const WS_DELETE_PROFILE_IMAGE_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<DeleteProfileImage xmlns=\"http://tempuri.org/\">"
"<userAccount>string</userAccount>"
"<password>string</password>"
"</DeleteProfileImage>"
"</soap:Body>"
"</soap:Envelope>";

NSString * const WS_GET_USER_INFO_V2_SOAP_ACTION = @"http://tempuri.org/GetUserInfoV2";
NSString * const WS_GET_USER_INFO_V2_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<GetUserInfoV2 xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<customerId >%@</customerId>"
"</GetUserInfoV2>"
"</soap:Body>"
"</soap:Envelope>";
//@2015/12/10

//@2015/04/23 new UpdateMemberSetting API
NSString * const WS_UPDATE_USER_ACTION_V2 = @"http://tempuri.org/UpdateMemberSetting";
NSString * const WS_UPDATE_USER_SOAP_BODY_V2 =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<UpdateMemberSetting xmlns=\"http://tempuri.org/\">"
"<Username>%@</Username>"
"<Password>%@</Password>"
"<memberPlanId>%d</memberPlanId>"
"<age>%d</age>"
"<gender>%@</gender>"
"<height>%f</height>"
"<FtWeight>%f</FtWeight>"
"<TgWeight>%f</TgWeight>"
"</UpdateMemberSetting>"
"</soap12:Body>"
"</soap12:Envelope>";
//@2015/04/23 new UpdateMemberSetting API

#pragma mark - SP
NSString * const WS_SP_PATH = @"SP.asmx";

// Upload DeviceKey
/*
NSString * const WS_ADD_USER_KEY_ACTION = @"http://tempuri.org/AddUserKey";
NSString * const WS_ADD_USER_KEY_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddUserKey xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<DeviceId>%@</DeviceId>"
"<UserKey>%@</UserKey>"
"<UserName>%@</UserName>"
"<ActionMode>%d</ActionMode>"
"</AddUserKey>"
"</soap12:Body>"
"</soap12:Envelope>";
*/

/*
NSString * const WS_ADD_USER_KEY_ACTION_V3 = @"http://tempuri.org/AddUserKeyV3";
NSString * const WS_ADD_USER_KEY_SOAP_BODY_V3 =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddUserKeyV3 xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<jsonCloudKeyData>%@</jsonCloudKeyData>"
"</AddUserKeyV3>"
"</soap12:Body>"
"</soap12:Envelope>";
*/

NSString * const WS_ADD_USER_KEY_BY_ADMINISTER_SOAP_ACTION = @"http://tempuri.org/AddUserKeyByAdministerV2";
NSString * const WS_ADD_USER_KEY_BY_ADMINISTER_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddUserKeyByAdministerV2 xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<jsonCloudKeyData>%@</jsonCloudKeyData>"
"</AddUserKeyByAdministerV2>"
"</soap12:Body>"
"</soap12:Envelope>";

NSString * const WS_DELETE_USER_KEY_BY_ADMINISTER_SOAP_ACTION = @"http://tempuri.org/DeleteUserKeyByAdminister";
NSString * const WS_DELETE_USER_KEY_BY_ADMINISTER_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<DeleteUserKeyByAdminister xmlns=\"http://tempuri.org/\">"
"<userAccount>%@</userAccount>"
"<password>%@</password>"
"<deleteUserAccount>%@</deleteUserAccount>"
"<DeviceId>%@</DeviceId>"
"<LastUpdated>%@</LastUpdated>"
"</DeleteUserKeyByAdminister>"
"</soap12:Body>"
"</soap12:Envelope>";

#pragma mark - Sleep
//@2015/11/17 sleep record
NSString * const WS_SLEEP_PATH = @"Sleep.asmx";

//@2016/08/01
NSString * const WS_ADD_ORIGINAL_SLEEP_RECORDS_SOAP_ACTION = @"http://tempuri.org/AddOriginalSleepRecords";
NSString * const WS_ADD_ORIGINAL_SLEEP_RECORDS_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddOriginalSleepRecords xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<Utc>%@</Utc>"
"<OriginalSleeps>%@</OriginalSleeps>"
"</AddOriginalSleepRecords>"
"</soap12:Body>"
"</soap12:Envelope>";
//@2016/08/01

NSString * const WS_ADD_SLEEP_RECORDS_SOAP_ACTION = @"http://tempuri.org/AddSleepRecords";
NSString * const WS_ADD_SLEEP_RECORDS_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddSleepRecords xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<startTime>%@</startTime>"
"<endTime>%@</endTime>"
"<deepMinutes>%@</deepMinutes>"
"<lightMinutes>%@</lightMinutes>"
"<sleepRawData>%@</sleepRawData>"
"</AddSleepRecords>"
"</soap12:Body>"
"</soap12:Envelope>";
//@2015/11/17 sleep record

#pragma mark - BloodSugar
NSString * const WS_BSDATA_PATH = @"BloodSugar.asmx";

NSString * const WS_ADDBSDATA_SOAP_ACTION = @"http://tempuri.org/AddRecord";
NSString * const WS_ADDBSDATA_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRecord xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<memo>%@</memo>"
"<recordTime>%@</recordTime>"
"<typeId>%d</typeId>"
"<consistency>%d</consistency>"
//  "<typeID>%d</typeID>"//set to zero according to android
"</AddRecord>"
"</soap12:Body>"
"</soap12:Envelope>";

NSString * const WS_UPDATEBSDATA_SOAP_ACTION = @"http://tempuri.org/UpdateRecord";
NSString * const WS_UPDATEBSDATA_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<UpdateRecord xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<serverId>%d</serverId>"
"<recordTime>%@</recordTime>"
"<consistency>%d</consistency>"
"<typeId>%d</typeId>"
"<memo>%@</memo>"
"</UpdateRecord>"
"</soap12:Body>"
"</soap12:Envelope>";

#pragma mark - HeartRate
NSString * const WS_HEARTRATE_PATH = @"HeartRate.asmx";

NSString * const WS_ADD_HEARTRATE_RECORD_SOAP_ACTION = @"http://tempuri.org/AddHeartRateRecord";
NSString * const WS_ADD_HEARTRATE_RECORD_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddHeartRateRecord xmlns=\"http://tempuri.org/\">"
"<UserAccount>%@</UserAccount>"
"<Password>%@</Password>"
"<recordTime>%@</recordTime>"
"<LastUpdated>%@</LastUpdated>"
"<HR>%@</HR>"
"<Remark>%@</Remark>"
"</AddHeartRateRecord>"
"</soap12:Body>"
"</soap12:Envelope>";


NSString * const WS_ADD_USER_DEVICE_INFO_SOAP_ACTION = @"http://tempuri.org/AddUserDeviceInfo";
NSString * const WS_ADD_USER_DEVICE_INFO_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddUserDeviceInfo xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<DeviceId>%@</DeviceId>"
"<DeviceSn>%@</DeviceSn>"
"<BroadcastId>%@</BroadcastId>"
"<DeviceType>%@</DeviceType>"
"<HardwareVersion>%@</HardwareVersion>"
"<FirmwareVersion>%@</FirmwareVersion>"
"<ProtocolType>%@</ProtocolType>"
"<UserNumber>%d</UserNumber>"
" <DeviceName>%@</DeviceName>"
" <ModelNumber>%@</ModelNumber>"
"<CreateDate>%@</CreateDate>"
"<LastUpdated>%@</LastUpdated>"
"</AddUserDeviceInfo>"
"</soap12:Body>"
"</soap12:Envelope>";


NSString * const WS_DELETE_USER_DEVICE_INFO_SOAP_ACTION = @"http://tempuri.org/DeleteUserDeviceInfo";
NSString * const WS_DELETE_USER_DEVICE_INFO_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<DeleteUserDeviceInfo xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<DeviceSn>%@</DeviceSn>"
"<LastUpdated>%@</LastUpdated>"
"</DeleteUserDeviceInfo>"
"</soap12:Body>"
"</soap12:Envelope>";

#pragma mark - RFID
//@2017/2/9 RFID record
NSString * const WS_RFID_PATH = @"RFID.asmx";

//@2017/2/9
NSString * const WS_ADD_USER_RFID_ADMINISTER_SOAP_ACTION = @"http://tempuri.org/AddUserRFIdByAdminister";
NSString * const WS_ADD_USER_RFID_ADMINISTER_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<AddUserRFIdByAdminister xmlns=\"http://tempuri.org/\">"
"<AdminAccount>%@</AdminAccount>"
"<AdminPWD>%@</AdminPWD>"
"<UserAccount>%@</UserAccount>"
"<LastUpdated>%@</LastUpdated>"
"<RFIdMac>%@</RFIdMac>"
"</AddUserRFIdByAdminister>"
"</soap:Body>"
"</soap:Envelope>";
//@2017/2/9
NSString * const WS_DELETE_USER_RFID_ADMINISTER_SOAP_ACTION = @"http://tempuri.org/DeleteUserRFIdByAdminister";
NSString * const WS_DELETE_USER_RFID_ADMINISTER_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<DeleteUserRFIdByAdminister xmlns=\"http://tempuri.org/\">"
"<AdminAccount>%@</AdminAccount>"
"<AdminPWD>%@</AdminPWD>"
"<UserAccount>%@</UserAccount>"
"<LastUpdated>%@</LastUpdated>"
"<RFIdMac>%@</RFIdMac>"
"</DeleteUserRFIdByAdminister>"
"</soap:Body>"
"</soap:Envelope>";
//@2017/2/9
NSString * const WS_GET_NEW_USER_RFID_SOAP_ACTION = @"http://tempuri.org/GetNewUserRFId";
NSString * const WS_GET_NEW_USER_RFID_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<GetNewUserRFId xmlns=\"http://tempuri.org/\">"
"<AdminAccount>%@</AdminAccount>"
"<AdminPWD>%@</AdminPWD>"
"<LastUpdated>%@</LastUpdated>"
"</GetNewUserRFId>"
"</soap:Body>"
"</soap:Envelope>";
//@2017/2/9

//@2017/06/09
NSString * const WS_VISITOR_PATH = @"Visitor.asmx";
NSString * const WS_ADD_MEASURE_RECORD_SOAP_ACTION = @"http://tempuri.org/AddMeasureRecord";
NSString * const WS_ADD_MEASURE_RECORD_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<AddMeasureRecord xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<MacAddress>%@</MacAddress>"
"<DeviceType>%@</DeviceType>"
"<recordTime>%@</recordTime>"
"<place>%@</place>"
"<MeasureType>%@</MeasureType>"
"<jsonMeasure>%@</jsonMeasure>"
"</AddMeasureRecord>"
"</soap:Body>"
"</soap:Envelope>";
extern NSString * const WS_GET_VISITOR_COUNT_SOAP_ACTION = @"http://tempuri.org/GetVisitorCount";
extern NSString * const WS_GET_VISITOR_COUNT_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
"<soap:Body>"
"<GetVisitorCount xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<StartDate>%@</StartDate>"
"<EndDate>%@</EndDate>"
"<Place>%@</Place>"
"</GetVisitorCount>"
"</soap:Body>"
"</soap:Envelope>";

NSString * const WS_TEMPERATURE_PATH = @"temperature.asmx";

NSString * const WS_TEMPERATURE_ADD_RECORDV2_SOAP_ACTION = @"http://tempuri.org/AddRecordV2";
NSString * const WS_TEMPERATURE_ADD_RECORDV2_SOAP_BODY = @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddRecordV2 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"//帳號
"<password>%@</password>"//密碼
"<recordTime>%@</recordTime>"//量測時間
"<Degrees>%@</Degrees>"//量測值(攝氏)
"<MeasureType>%d</MeasureType>"//量測型態(0:耳溫，1:額溫)
"<MeasureUnit>%d</MeasureUnit>"//量測單位(0:攝氏，1:華氏)
"<memo>%@</memo>"//備註
"<MacAddress>%@</MacAddress>"//產品唯一識別碼
"<DeviceType>%@</DeviceType>"//產品型號
"<autoMeasure>%@</autoMeasure>"//是否為自動量測的識別碼(Y:自動，N:手動)
"</AddRecordV2>"
"</soap12:Body>"
"</soap12:Envelope>";

NSString * const WS_TEMPERATURE_UPDATE_RECORDV2_SOAP_ACTION = @"http://tempuri.org/UpdateRecordV2";
NSString * const WS_TEMPERATURE_UPDATE_RECORDV2_SOAP_BODY = @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<UpdateRecordV2 xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<serverId>%d</serverId>"//告訴後台哪一筆資料更新
"<recordTime>%@</recordTime>"
"<Degrees>%@</Degrees>"//量測值
"<MeasureType>%d</MeasureType>"//量測型態(0:耳溫，1:額溫)
"<MeasureUnit>%d</MeasureUnit>"//量測單位(0:攝氏，1:華氏)
"<memo>%@</memo>"
"<autoMeasure>%@</autoMeasure>"//是否為自動量測的識別碼(Y:自動，N:手動)
"</UpdateRecordV2>"
"</soap12:Body>"
"</soap12:Envelope>";
NSString * const WS_ECG_PATH = @"ECG.asmx";
//HR API
NSString * const WS_ADD_ECG_HR_RECORD_SOAP_ACTION = @"http://tempuri.org/AddECGHRRecord";
NSString * const WS_ADD_ECG_HR_RECORD_SOAP_BODY = @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddECGHRRecord xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<RecordTime>%@</RecordTime>"
"<Heartbeat>%d</Heartbeat>"
"<Pressure>%d</Pressure>"
"<VitalityAge>%d</VitalityAge>"
"<EmoIndex>%d</EmoIndex>"
"<MacAddress>%@</MacAddress>"
"<Memo>%@</Memo>"
"<AppInfo>%@</AppInfo>"
"</AddECGHRRecord>"
"</soap12:Body>"
"</soap12:Envelope>";
// 2017/09/29 Add By Lynn
NSString * const WS_ECGHRV_PATH = @"HRV.asmx";

NSString * const WS_ADD_ECG_INDICATOR_RECORD_SOAP_ACTION = @"http://tempuri.org/AddIndexRecord";
NSString * const WS_ADD_ECG_INDICATOR_RECORD_SOAP_BODY = @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<AddIndexRecord xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<RecordTime>%@</RecordTime>"
"<R2r>%@</R2r>"
"<MacAddress>%@</MacAddress>"
"<Memo>%@</Memo>"
"<AppInfo>%@</AppInfo>"
"</AddIndexRecord>"
"</soap12:Body>"
"</soap12:Envelope>";

extern NSString * const WS_UPDATE_ECG_INDICATOR_RECORD_RESULT_SOAP_ACTION= @"http://tempuri.org/UpdateIndexResult";
extern NSString * const WS_UPDATE_ECG_INDICATOR_RECORD_RESULT_SOAP_BODY =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<UpdateIndexResult xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<IndexID>%d</IndexID>"
"<HRVIndex>%@</HRVIndex>"
"</UpdateIndexResult>"
"</soap12:Body>"
"</soap12:Envelope>";

NSString * const WS_SYNC_ECG_INDICATOR_RECORD_RESULT_BY_DATE_SOAP_ACTION=@"http://tempuri.org/GetIndexResultByDate";
NSString * const WS_SYNC_ECG_INDICATOR_RECORD_RESULT_BY_DATE_SOAP_BODY=@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
"<soap12:Body>"
"<GetIndexResultByDate xmlns=\"http://tempuri.org/\">"
"<id>%@</id>"
"<password>%@</password>"
"<StartDate>%@</StartDate>"
"<EndDate>%@</EndDate>"
"<strId>%@</strId>"
"</GetIndexResultByDate>"
"</soap12:Body>"
"</soap12:Envelope>";
