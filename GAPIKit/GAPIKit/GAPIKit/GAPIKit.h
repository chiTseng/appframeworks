//
//  GAPIKit.h
//  GAPIKit
//
//  Created by elaine on 2018/5/29.
//  Copyright © 2018年 GSH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthenticateService.h"
#import "MeasurementService.h"
#import "MemberRegisterService.h"
#import "SleepService.h"
//! Project version number for GAPIKit.
FOUNDATION_EXPORT double GAPIKitVersionNumber;

//! Project version string for GAPIKit.
FOUNDATION_EXPORT const unsigned char GAPIKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GAPIKit/PublicHeader.h>


