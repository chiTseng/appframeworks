//
//  WebServiceConstants.h
//  SoHappy
//
//  Created by Huang Tsung-Jen on 2012/2/16.
//  Copyright (c) 2012年 ITRI. All rights reserved.
//
#import <Foundation/Foundation.h>
//2017/06/09
extern NSString * const WS_VISITOR_PATH;
extern NSString * const WS_ADD_MEASURE_RECORD_SOAP_ACTION;
extern NSString * const WS_ADD_MEASURE_RECORD_SOAP_BODY;
extern NSString * const WS_GET_VISITOR_COUNT_SOAP_ACTION;
extern NSString * const WS_GET_VISITOR_COUNT_SOAP_BODY;

#pragma mark - SPID
extern NSString * const WS_CUSTOMERLOGINID;

#pragma mark - HOST & PROTOCOL
extern NSString * const WS_PROTOCOL;
extern NSString * const WS_HOST;
extern NSString * const WS_ROOTPATH;

#pragma mark - AUTHENTICATE
extern NSString * const WS_AUTHENTICATE_PATH;

extern NSString * const WS_GETACCOUNTANDPASSWORDBYEMAIL_SOAP_ACTION;
extern NSString * const WS_GETACCOUNTANDPASSWORDBYEMAIL_SOAP_BODY;

extern NSString * const WS_LOGIN_SOAP_ACTION;
extern NSString * const WS_LOGIN_SOAP_BODY;

extern NSString * const WS_DIETITIAN_LOGIN_SOAP_ACTION;
extern NSString * const WS_DIETITIAN_LOGIN_SOAP_BODY;

extern NSString * const WS_DIETITIAN_LOGIN_V2_SOAP_ACTION;
extern NSString * const WS_DIETITIAN_LOGIN_V2_SOAP_BODY;

extern NSString * const WS_DIETITIAN_LOGIN_V3_SOAP_ACTION;
extern NSString * const WS_DIETITIAN_LOGIN_V3_SOAP_BODY;

extern NSString * const WS_CUSTOMERLOGIN_SOAP_ACTION;
extern NSString * const WS_CUSTOMERLOGIN_SOAP_BODY;

extern NSString * const WS_USERLOGINBYNAME_SOAP_ACTION;
extern NSString * const WS_USERLOGINBYNAME_SOAP_BODY;

#pragma mark - Measurement
extern NSString * const WS_MEASUREMENT_PATH;

extern NSString * const WS_UPLOADMEASURE_SOAP_ACTION;
extern NSString * const WS_UPLOADMEASURE_SOAP_BODY;
/*
extern NSString * const WS_MEASUREMENT_ADDRECORD_V2_SOAP_ACTION;
extern NSString * const WS_MEASUREMENT_ADDRECORD_V2_SOAP_BODY;

extern NSString * const WS_UPLOADMEASURE_SOAP_ACTION_V3;
extern NSString * const WS_UPLOADMEASURE_SOAP_BODY_V3;
*/
/*
//@2016/08/16
extern NSString * const WS_MEASUREMENT_ADDRECORD_V3_SOAP_ACTION;
extern NSString * const WS_MEASUREMENT_ADDRECORD_V3_SOAP_BODY;
//@2016/08/16
*/

//@2017/06/09
extern NSString * const WS_MEASUREMENT_ADDRECORD_V5_SOAP_ACTION;
extern NSString * const WS_MEASUREMENT_ADDRECORD_V5_SOAP_BODY;

#pragma mark - Sport
extern NSString * const WS_SPORT_PATH;

extern NSString * const WS_UPLOAD_IMG_SPORT_SOAP_ACTION;
extern NSString * const WS_UPLOAD_IMG_SPORT_SOAP_BODY;

extern NSString * const WS_UPLOAD_IMG_SPORT_SOAP_ACTION_V2;
extern NSString * const WS_UPLOAD_IMG_SPORT_SOAP_BODY_V2;

extern NSString * const WS_ADD_SPORT_RECORD_SOAP_ACTION;
extern NSString * const WS_ADD_SPORT_RECORD_SOAP_BODY;
/*
extern NSString * const WS_ADD_RING_SPORT_RECORDS_SOAP_ACTION;
extern NSString * const WS_ADD_RING_SPORT_RECORDS_SOAP_BODY;
*/

//@2017/06/09
extern NSString * const WS_ADD_RING_SPORT_RECORDS_V3_SOAP_ACTION;
extern NSString * const WS_ADD_RING_SPORT_RECORDS_V3_SOAP_BODY;

#pragma mark - HistoryRecord
extern NSString * const WS_HISTORY_PATH;

extern NSString * const WS_GET_NEW_USER_KEY_ACTION;
extern NSString * const WS_GET_NEW_USER_KEY_SOAP_BODY;

extern NSString * const WS_GET_NEW_USER_KEY_ACTION_V2;
extern NSString * const WS_GET_NEW_USER_KEY_BODY_V2;

#pragma mark - Plan
extern NSString * const WS_USER_PLAN_PATH;

extern NSString * const WS_UPDATE_PLAN_SOAP_ACTION;
extern NSString * const WS_UPDATE_PLAN_SOAP_BODY;

extern NSString * const WS_GET_PLAN_SOAP_ACTION;
extern NSString * const WS_GET_PLAN_SOAP_BODY;

extern NSString * const WS_GET_GENDER_SOAP_ACTION;
extern NSString * const WS_GET_GENDER_SOAP_BODY;

#pragma mark - BloodPreasure
extern NSString * const WS_BPDATA_PATH;

/*
extern NSString * const WS_ADDBPDATA_SOAP_ACTION;
extern NSString * const WS_ADDBPDATA_SOAP_BODY;
*/
/*
//@2016/08/16
extern NSString * const WS_ADDBPDATA_V2_SOAP_ACTION;
extern NSString * const WS_ADDBPDATA_V2_SOAP_BODY;
//@2016/08/16
*/
//@2017/06/09
extern NSString * const WS_ADDBPDATA_V5_SOAP_ACTION;
extern NSString * const WS_ADDBPDATA_V5_SOAP_BODY;

extern NSString * const WS_UPDATEBPDATA_SOAP_ACTION;
extern NSString * const WS_UPDATEBPDATA_SOAP_BODY;

#pragma mark - MemberRegister
extern NSString * const WS_REGISTER_USER_PATH;

extern NSString * const WS_CHECK_USER_EXIST_SOAP_ACTION;
extern NSString * const WS_CHECK_USER_EXIST_SOAP_BODY;

extern NSString * const WS_UPLOAD_PHOTO_ACTION;
extern NSString * const WS_UPLOAD_PHOTO_SOAP_BODY;

extern NSString * const WS_REGISTER_USER_ACTION;
extern NSString * const WS_REGISTER_USER_SOAP_BODY;

extern NSString * const WS_GET_USER_INFO_ACTION;
extern NSString * const WS_GET_USER_INFO_SOAP_BODY;

extern NSString * const WS_UPDATE_USER_INFO_ACTION;
extern NSString * const WS_UPDATE_USER_SOAP_BODY;

extern NSString * const WS_UPDATE_USER_NO_PASSWORD_SOAP_BODY;

extern NSString * const WS_DELETE_PROFILE_IMAGE_ACTION;
extern NSString * const WS_DELETE_PROFILE_IMAGE_SOAP_BODY;

extern NSString * const WS_GET_USER_INFO_V2_SOAP_ACTION;
extern NSString * const WS_GET_USER_INFO_V2_SOAP_BODY;

extern NSString * const WS_UPDATE_USER_ACTION_V2;
extern NSString * const WS_UPDATE_USER_SOAP_BODY_V2 ;

#pragma mark - SP
extern NSString * const WS_SP_PATH;

/*
extern NSString * const WS_ADD_USER_KEY_ACTION;
extern NSString * const WS_ADD_USER_KEY_SOAP_BODY;
*/
/*
extern NSString * const WS_ADD_USER_KEY_ACTION_V3;
extern NSString * const WS_ADD_USER_KEY_SOAP_BODY_V3;
*/

extern NSString * const WS_ADD_USER_KEY_BY_ADMINISTER_SOAP_ACTION;
extern NSString * const WS_ADD_USER_KEY_BY_ADMINISTER_SOAP_BODY;

extern NSString * const WS_DELETE_USER_KEY_BY_ADMINISTER_SOAP_ACTION;
extern NSString * const WS_DELETE_USER_KEY_BY_ADMINISTER_SOAP_BODY;

extern NSString * const WS_RFID_PATH;
extern NSString * const WS_GET_NEW_USER_RFID_SOAP_BODY;
extern NSString * const WS_GET_NEW_USER_RFID_SOAP_ACTION;

extern NSString * const WS_DELETE_USER_RFID_ADMINISTER_SOAP_BODY ;
extern NSString * const WS_DELETE_USER_RFID_ADMINISTER_SOAP_ACTION;

extern NSString * const WS_ADD_USER_RFID_ADMINISTER_SOAP_BODY;
extern NSString * const WS_ADD_USER_RFID_ADMINISTER_SOAP_ACTION;

#pragma mark - Sleep
extern NSString * const WS_SLEEP_PATH;

extern NSString * const WS_ADD_SLEEP_RECORDS_SOAP_ACTION;
extern NSString * const WS_ADD_SLEEP_RECORDS_SOAP_BODY;

extern NSString * const WS_ADD_ORIGINAL_SLEEP_RECORDS_SOAP_ACTION;
extern NSString * const WS_ADD_ORIGINAL_SLEEP_RECORDS_SOAP_BODY;

#pragma mark - BloodSugar
extern NSString * const WS_BSDATA_PATH;

extern NSString * const WS_ADDBSDATA_SOAP_ACTION;
extern NSString * const WS_ADDBSDATA_SOAP_BODY;

extern NSString * const WS_UPDATEBSDATA_SOAP_ACTION;
extern NSString * const WS_UPDATEBSDATA_SOAP_BODY;

#pragma mark - HeartRate
extern NSString * const WS_HEARTRATE_PATH;
extern NSString * const WS_ADD_HEARTRATE_RECORD_SOAP_ACTION;
extern NSString * const WS_ADD_HEARTRATE_RECORD_SOAP_BODY;

#pragma mark - UserDevice
extern NSString * const WS_ADD_USER_DEVICE_INFO_SOAP_ACTION;
extern NSString * const WS_ADD_USER_DEVICE_INFO_SOAP_BODY;

extern NSString * const WS_DELETE_USER_DEVICE_INFO_SOAP_ACTION;
extern NSString * const WS_DELETE_USER_DEVICE_INFO_SOAP_BODY;

#pragma mark - temperature
extern NSString * const WS_TEMPERATURE_PATH;

extern NSString * const WS_TEMPERATURE_ADD_RECORDV2_SOAP_ACTION ;
extern NSString * const WS_TEMPERATURE_ADD_RECORDV2_SOAP_BODY ;

extern NSString * const WS_TEMPERATURE_UPDATE_RECORDV2_SOAP_ACTION;
extern NSString * const WS_TEMPERATURE_UPDATE_RECORDV2_SOAP_BODY;

#pragma mark - ECG
extern NSString * const WS_ECG_PATH;

extern NSString * const WS_ADD_ECG_HR_RECORD_SOAP_ACTION ;
extern NSString * const WS_ADD_ECG_HR_RECORD_SOAP_BODY;

// 2017/09/29 Add By Lynn
extern NSString * const WS_ECGHRV_PATH;
extern NSString * const WS_ADD_ECG_INDICATOR_RECORD_SOAP_ACTION;
extern NSString * const WS_ADD_ECG_INDICATOR_RECORD_SOAP_BODY;

extern NSString * const WS_UPDATE_ECG_INDICATOR_RECORD_RESULT_SOAP_ACTION;
extern NSString * const WS_UPDATE_ECG_INDICATOR_RECORD_RESULT_SOAP_BODY;

extern NSString * const WS_SYNC_ECG_INDICATOR_RECORD_RESULT_BY_DATE_SOAP_ACTION;
extern NSString * const WS_SYNC_ECG_INDICATOR_RECORD_RESULT_BY_DATE_SOAP_BODY;
