//
//  WebServiceAPI.m
//  SoHappy
//
//  Created by Huang Tsung-Jen on 2012/2/14.
//  Copyright (c) 2012年 ITRI. All rights reserved.
//

#import "WebServiceAPI.h"
#import "WebServiceConstants.h"
@implementation WebServiceAPI
@synthesize callbackDelegate = _callbackDelegate;
@synthesize timeoutSeconds;
@synthesize soapAction;
@synthesize mTableName;
@synthesize sessionDataTask;
- (id)init
{
    self = [super init];
    if (self != nil)
    {
        httpPostResponse = [[NSMutableData alloc] init];
        timeoutSeconds = 45.0;
    }
    return self;
}
- (NSData *) requestSyncWebServiceWithURL:(NSString *)urlPath host:(NSString *)h soapAction:(NSString *)action soapBody:(NSString *)soapBody withError:(NSError **)requestError
{   // mTableName=iTableName;
    NSURLResponse *urlResponse = nil;
    NSURL *url = [NSURL URLWithString:urlPath];
    NSMutableURLRequest *urlReq = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:timeoutSeconds];
    [urlReq setHTTPMethod:@"POST"];
    [urlReq setValue:h forHTTPHeaderField:@"HOST"];
    [urlReq setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlReq setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[soapBody length]] forHTTPHeaderField:@"Content-Length"];
    [urlReq setValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    [urlReq setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *response = [NSURLConnection sendSynchronousRequest:urlReq returningResponse:&urlResponse error:requestError];
    return response;
}

- (void)requestWebServiceWithURL_update:(NSString *)urlPath host:(NSString *)h soapAction:(NSString *)action soapBody:(NSString *)soapBody withDelegate:(id<WebServiceAPIDelegate>) del iTableName:(NSString*)iTableName 
{
    mTableName=iTableName;
    //DLog(@"SOAP BODY 1:%@", soapBody);
    self.soapAction = action;
    _callbackDelegate = del;
    NSURL *url = [NSURL URLWithString:urlPath];
    NSMutableURLRequest *urlReq = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:timeoutSeconds];
    [urlReq setHTTPMethod:@"POST"];
    [urlReq setValue:h forHTTPHeaderField:@"HOST"];
    [urlReq setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlReq setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[soapBody length]] forHTTPHeaderField:@"Content-Length"];
    [urlReq setValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    [urlReq setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    theConnection = [[NSURLConnection alloc] initWithRequest:urlReq delegate:self];
    timer = [NSTimer scheduledTimerWithTimeInterval:timeoutSeconds target:self selector:@selector(timeoutHandler) userInfo:nil repeats:NO];
}
- (void) callWebService:(NSString *)urlPath host:(NSString *)h soapAction:(NSString *)action soapBody:(NSString *)soapBody withDelegate:(id<WebServiceAPIDelegate>) del
{
    // 1.创建一个网络路径
    mTableName=@"";
    //    DLog(@"SOAP BODY 2:%@", soapBody);
    self.soapAction = action;
    _callbackDelegate = del;
    httpPostResponse = [[NSMutableData alloc] init];
    NSURL *url = [NSURL URLWithString:urlPath];
    // 2.创建一个网络请求，分别设置请求方法、请求参数
    NSMutableURLRequest *urlReq = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:timeoutSeconds];
    [urlReq setHTTPMethod:@"POST"];
    [urlReq setValue:h forHTTPHeaderField:@"HOST"];
    [urlReq setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlReq setValue:[NSString stringWithFormat:@"%d", (int)[soapBody length]] forHTTPHeaderField:@"Content-Length"];
    [urlReq setValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    [urlReq setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    
    // 3.获得会话对象
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                          delegate:self
                                                     delegateQueue:[[NSOperationQueue alloc] init]];
    // 4.根据会话对象，创建一个Task任务
    sessionDataTask = [session dataTaskWithRequest:urlReq];
    //5.最后一步，执行任务，(resume也是继续执行)。
    [sessionDataTask resume];
}
- (void)requestWebServiceWithURL:(NSString *)urlPath host:(NSString *)h soapAction:(NSString *)action soapBody:(NSString *)soapBody withDelegate:(id<WebServiceAPIDelegate>) del
{
    
    mTableName=@"";
//    DLog(@"SOAP BODY 2:%@", soapBody);
    self.soapAction = action;
    _callbackDelegate = del;
    NSURL *url = [NSURL URLWithString:urlPath];
    NSMutableURLRequest *urlReq = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:timeoutSeconds];
    [urlReq setHTTPMethod:@"POST"];
    [urlReq setValue:h forHTTPHeaderField:@"HOST"];
    [urlReq setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlReq setValue:[NSString stringWithFormat:@"%d", (int)[soapBody length]] forHTTPHeaderField:@"Content-Length"];
    [urlReq setValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    [urlReq setHTTPBody:[soapBody dataUsingEncoding:NSUTF8StringEncoding]];
    theConnection = [[NSURLConnection alloc] initWithRequest:urlReq delegate:self];
    NSLog(@"======================");
    timer = [NSTimer scheduledTimerWithTimeInterval:timeoutSeconds target:self selector:@selector(timeoutHandler) userInfo:nil repeats:NO];
}
- (void) testServer
{
    NSURL *url = [NSURL URLWithString:@"https://www.google.com.tw"];
    NSMutableURLRequest *urlReq = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:timeoutSeconds];
    theConnection = [[NSURLConnection alloc] initWithRequest:urlReq delegate:self];
    timer = [NSTimer scheduledTimerWithTimeInterval:timeoutSeconds target:self selector:@selector(timeoutHandler) userInfo:nil repeats:NO];
}
- (void) cancel
{    
    /*[theConnection cancel];
//    [theConnection release];
    theConnection = nil;
    _callbackDelegate = nil;
    [timer invalidate];*/
    if (sessionDataTask == nil) {
        return;
    }
    [sessionDataTask cancel];
    if (timer != nil)
    {
        [timer invalidate];
        timer = nil;
    }
    _callbackDelegate = nil;
}
- (void)dealloc
{
//    [httpPostResponse release];
//    [soapAction release];
//    [super dealloc];

}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [httpPostResponse setLength:0];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [httpPostResponse appendData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [timer invalidate];
    if(_callbackDelegate==nil)return;
//    [theConnection release];
    
    NSString *xmlString = [[NSString alloc] initWithBytes:[httpPostResponse mutableBytes] length:[httpPostResponse length] encoding:NSUTF8StringEncoding];
    //DLog(@"XML Result: %@",xmlString);
//    [xmlString release];
    if(mTableName.length==0){
        [_callbackDelegate webServiceCallFinished:httpPostResponse soapAction: self.soapAction];
    }else{
         // 網路同步資料專用 powenko
        [_callbackDelegate webServiceCallFinished:httpPostResponse soapAction: self.soapAction iTableName:mTableName];
     }
 //  iTableName:(NSString*)iTableName;
   

}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [timer invalidate];
//    [theConnection release];
    
    if(_callbackDelegate==nil){
        //DLog(@"callbackDelegate==nil");
        return;
    }
    //DLog(@"Connection failed! Error %ld - %@", (long)[error code], [error localizedDescription]);
    [_callbackDelegate webServiceCallError:error soapAction: self.soapAction];
    /*
     -1009 - The Internet connection appears to be offline.
     -1003 - A server with the specified hostname could not be found.
     -1004 - Could not connect to the server.
     -9001 - ConnectionTimeout
     */
}
- (BOOL)shouldTrustProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    // Load up the bundled certificate.
    NSString *build = [NSBundle mainBundle].bundlePath;
    NSString *certPath = [NSString stringWithFormat:@"%@/%@",build,@"wow.der"];
    NSData *certData = [[NSData alloc] initWithContentsOfFile:certPath];
    CFDataRef certDataRef = (__bridge_retained CFDataRef)certData;
    SecCertificateRef cert = SecCertificateCreateWithData(NULL, certDataRef);
    
    // Establish a chain of trust anchored on our bundled certificate.
    CFArrayRef certArrayRef = CFArrayCreate(NULL, (void *)&cert, 1, NULL);
    SecTrustRef serverTrust = protectionSpace.serverTrust;
    
    SecTrustSetAnchorCertificates(serverTrust, certArrayRef);
    
    // Verify that trust.
    SecTrustResultType trustResult;
    SecTrustEvaluate(serverTrust, &trustResult);
    
    // Clean up.
    CFRelease(certArrayRef);
    CFRelease(cert);
    CFRelease(certDataRef);
    
    // Did our custom trust chain evaluate successfully?
    return trustResult == kSecTrustResultUnspecified;
    
}
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([self shouldTrustProtectionSpace:challenge.protectionSpace]) {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    } else {
        [challenge.sender performDefaultHandlingForAuthenticationChallenge:challenge];
    }
}
-(void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler
{
    if ([self shouldTrustProtectionSpace:challenge.protectionSpace]) {
        //[challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
        completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust: serverTrust]);
    } else {
        //[challenge.sender performDefaultHandlingForAuthenticationChallenge:challenge];
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}
- (void) timeoutHandler
{
    /*if(theConnection==nil){
        //DLog(@"theConnection==nil");
        return;
    }*/
    if (sessionDataTask == nil) {
        return;
    }
    //DLog(@"Time out!");
    /*[theConnection cancel];
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:NSLocalizedString(@"目前網路訊號不穩定，請確認wifi或3G訊號是否正常！",nil) forKey:NSLocalizedDescriptionKey];
    NSError *error = [NSError errorWithDomain:NSLocalizedString(@"目前網路訊號不穩定，請確認wifi或3G訊號是否正常！",nil) code:-9001 userInfo:errorDetail];
    [self connection:theConnection didFailWithError:error];*/
    [sessionDataTask cancel];
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:NSLocalizedString(@"目前網路訊號不穩定，請確認wifi或3G訊號是否正常！",nil) forKey:NSLocalizedDescriptionKey];
    NSError *error = [NSError errorWithDomain:NSLocalizedString(@"目前網路訊號不穩定，請確認wifi或3G訊號是否正常！",nil) code:-9001 userInfo:errorDetail];
    [timer invalidate];
    //    [theConnection release];
    
    if(_callbackDelegate==nil){
        //DLog(@"callbackDelegate==nil");
        return;
    }
    //DLog(@"Connection failed! Error %ld - %@", (long)[error code], [error localizedDescription]);
    [_callbackDelegate webServiceCallError:error soapAction: self.soapAction];
    
}
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    
    //【注意：此处需要允许处理服务器的响应，才会继续加载服务器的数据。 若在接收响应时需要对返回的参数进行处理(如获取响应头信息等),那么这些处理应该放在这个允许操作的前面。】
    completionHandler(NSURLSessionResponseAllow);
}

// 2.接收到服务器的数据（此方法在接收数据过程会多次调用）
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    // 处理每次接收的数据，例如每次拼接到自己创建的数据receiveData
    [httpPostResponse appendData:data];
}

// 3.3.任务完成时调用（如果成功，error == nil）
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
    if(error == nil){
        [timer invalidate];
        if(_callbackDelegate==nil)return;
        //    [theConnection release];
        
        NSString *xmlString = [[NSString alloc] initWithBytes:[httpPostResponse mutableBytes] length:[httpPostResponse length] encoding:NSUTF8StringEncoding];
        //DLog(@"XML Result: %@",xmlString);
        //    [xmlString release];
        if(mTableName.length==0){
            [_callbackDelegate webServiceCallFinished:httpPostResponse soapAction: self.soapAction];
        }else{
            // 網路同步資料專用 powenko
            [_callbackDelegate webServiceCallFinished:httpPostResponse soapAction: self.soapAction iTableName:mTableName];
        }
        //  iTableName:(NSString*)iTableName;
    }
    else{
        NSLog(@"请求失败:%@",error);
        [timer invalidate];
        //    [theConnection release];
        
        if(_callbackDelegate==nil){
            //DLog(@"callbackDelegate==nil");
            return;
        }
        //DLog(@"Connection failed! Error %ld - %@", (long)[error code], [error localizedDescription]);
        [_callbackDelegate webServiceCallError:error soapAction: self.soapAction];
        /*
         -1009 - The Internet connection appears to be offline.
         -1003 - A server with the specified hostname could not be found.
         -1004 - Could not connect to the server.
         -9001 - ConnectionTimeout
         */
    }
}
@end
