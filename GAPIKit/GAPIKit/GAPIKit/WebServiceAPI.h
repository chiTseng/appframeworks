//
//  WebServiceAPI.h
//  SoHappy
//
//  Created by Huang Tsung-Jen on 2012/2/14.
//  Copyright (c) 2012年 ITRI. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WebServiceAPIDelegate<NSObject> 
- (void) webServiceCallFinished: (NSData*) returnData soapAction: (NSString*) soapAction;
- (void) webServiceCallError: (NSError *) error soapAction: (NSString*) soapAction;
@optional
-(void)webServiceCallFinished:(NSData*)returnData soapAction: (NSString*) soapAction iTableName:mTableName;
@end

@interface WebServiceAPI : NSObject<NSURLConnectionDataDelegate,NSURLSessionDataDelegate>
{
    id<WebServiceAPIDelegate> callbackDelegate;
    NSURLConnection *theConnection;
    NSTimer *timer;
    NSMutableData *httpPostResponse;
    double timeoutSeconds;
    NSString *soapAction;
    NSString *mTableName;
}
@property (assign) id<WebServiceAPIDelegate> callbackDelegate;
@property (assign) double timeoutSeconds;
@property (nonatomic, retain) NSString *soapAction;
@property (nonatomic, retain) NSString *mTableName;
@property (nonatomic, retain) NSMutableData *receiveData;
@property (nonatomic, retain) NSURLSessionTask *sessionDataTask;


- (id)init;
- (void) requestWebServiceWithURL: (NSString*) urlPath host:(NSString*) host soapAction:(NSString*)action soapBody:(NSString*) soapBody withDelegate: (id<WebServiceAPIDelegate>) delegate;
- (NSData *) requestSyncWebServiceWithURL:(NSString *)urlPath host:(NSString *)h soapAction:(NSString *)action soapBody:(NSString *)soapBody withError:(NSError **)requestError;
- (void)requestWebServiceWithURL_update:(NSString *)urlPath host:(NSString *)h soapAction:(NSString *)action soapBody:(NSString *)soapBody withDelegate:(id<WebServiceAPIDelegate>) del iTableName:(NSString*)iTableName;
- (void) cancel;
- (void) callWebService:(NSString *)urlPath host:(NSString *)h soapAction:(NSString *)action soapBody:(NSString *)soapBody withDelegate:(id<WebServiceAPIDelegate>) del;
- (void) testServer;
@end
