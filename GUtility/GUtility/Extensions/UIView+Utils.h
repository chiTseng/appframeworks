//
//  UIView+Utils.h
//  GUtility
//
//  Created by gsh_mac_2018 on 2019/7/29.
//  Copyright © 2019 gsh_mac_2018. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIView (Utils)

-(UIViewController *)parentViewController;

@end
