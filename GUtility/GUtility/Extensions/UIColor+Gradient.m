//
//  UIColor+Gradient.m
//  SoHappy
//
//  Created by elaine on 2018/8/1.
//

#import "UIColor+Gradient.h"

@implementation UIColor (Gradient)
+(UIImage*)gradientImgWithColors:(NSArray*)colors frame:(CGRect)frame
{
    CAGradientLayer *gradient = [UIColor gradientLayerWithColors:colors frame:frame];
    UIGraphicsBeginImageContext(gradient.frame.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
+(UIImage*)gradientImgWithColorsVertical:(NSArray*)colors frame:(CGRect)frame
{
    CAGradientLayer *gradient = [UIColor gradientLayerWithColorsVertical:colors frame:frame];
    UIGraphicsBeginImageContext(gradient.frame.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
+(CAGradientLayer*)gradientLayerWithColors:(NSArray*)colors frame:(CGRect)frame
{
    CAGradientLayer *gradient = [CAGradientLayer new];
    //CGFloat sizeLenth = [[UIScreen mainScreen] bounds].size.width;
    CGRect frameAndStatusBar = frame;//CGRectMake(0, 0, sizeLenth, 64);
    gradient.frame = frameAndStatusBar;
    gradient.colors = colors;
    //gradient.locations = @[@0.0,@0.7];
    gradient.startPoint = CGPointMake(0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    return gradient;
}
+(CAGradientLayer*)gradientLayerWithColorsVertical:(NSArray*)colors frame:(CGRect)frame
{
    CAGradientLayer *gradient = [CAGradientLayer new];
    //CGFloat sizeLenth = [[UIScreen mainScreen] bounds].size.width;
    CGRect frameAndStatusBar = frame;//CGRectMake(0, 0, sizeLenth, 64);
    gradient.frame = frameAndStatusBar;
    gradient.colors = colors;
    gradient.locations = @[@0.0,@0.7];

    return gradient;
}
@end
