//
//  UIImage_Extra.h
//  SoHappy
//
//  Created by Huang Tsung-Jen on 2012/2/18.
//  Copyright (c) 2012年 ITRI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extras)
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
@end
