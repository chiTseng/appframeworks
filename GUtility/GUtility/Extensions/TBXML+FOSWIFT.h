//
//  TBXML+FOSWIFT.h
//  WGH_INHOUSE
//
//  Created by gsh_mac_2018 on 2019/12/26.
//

#import <Foundation/Foundation.h>
#import <GUtility/GUtility.h>
NS_ASSUME_NONNULL_BEGIN

@interface TBXML (TBXMLFORSWIFT)
+ (NSMutableDictionary*)getAllChildDataWithReturnData:(NSData*)returnData response:(NSString*)response;
@end

NS_ASSUME_NONNULL_END
