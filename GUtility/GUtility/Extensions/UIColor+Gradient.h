//
//  UIColor+Gradient.h
//  SoHappy
//
//  Created by elaine on 2018/8/1.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UIColor (Gradient)
/**取得顏色漸層img*/
+(UIImage*)gradientImgWithColors:(NSArray*)colors frame:(CGRect)frame;
/**取得顏色漸層layer*/
+(CAGradientLayer*)gradientLayerWithColors:(NSArray*)colors frame:(CGRect)frame;
+(UIImage*)gradientImgWithColorsVertical:(NSArray*)colors frame:(CGRect)frame;
+(CAGradientLayer*)gradientLayerWithColorsVertical:(NSArray*)colors frame:(CGRect)frame;
@end
