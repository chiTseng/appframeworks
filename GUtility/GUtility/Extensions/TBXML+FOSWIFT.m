//
//  TBXML+FOSWIFT.m
//  WGH_INHOUSE
//
//  Created by gsh_mac_2018 on 2019/12/26.
//

#import "TBXML+FOSWIFT.h"

@implementation  TBXML (TBXMLFORSWIFT)
+ (NSMutableDictionary*)getAllChildDataWithReturnData:(NSData*)returnData response:(NSString*)response
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    TBXML *xmlParser = [[TBXML alloc] initWithXMLData:returnData];
    TBXMLElement *root = xmlParser.rootXMLElement;
    while (root != nil && ![[TBXML elementName:root] isEqualToString:response]) {
        root = root->firstChild;
    }
    if (root == nil)
    {
        return dic;
    }
    root = root->firstChild;
    while (root != nil)
    {
        NSLog(@"開始增加了喔");
        [dic setValue:[TBXML textForElement:root] forKey:[TBXML elementName:root]];
        /*if ([[TBXML elementName:root] isEqualToString:@"SyncWCRecordResult"])
        {
            returnCode = [[TBXML textForElement:root] intValue];
        }
        else if ([[TBXML elementName:root] isEqualToString:@"maxversion"])
        {
            maxversion = [TBXML textForElement:root];
        }
        else if ([[TBXML elementName:root] isEqualToString:@"comments"])
        {
            resultSet = [[[TBXML textForElement:root] stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"]stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
            DLog(@"resultSet:%@",resultSet);
        }*/
        root = root->nextSibling;
    }
    return dic;
}
@end
