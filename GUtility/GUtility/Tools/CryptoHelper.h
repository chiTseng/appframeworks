//
//  CryptoHelper.h
//  SoHappy
//
//  Created by gsh_mac_2018 on 2019/6/13.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>

NS_ASSUME_NONNULL_BEGIN

@interface CryptoHelper : NSObject
/**
 *  加密方式,MAC算法: HmacSHA256
 *
 *  @param secret       秘钥
 *  @param content 要加密的文本
 *
 *  @return 加密后的字符串
 */
+ (NSString *)hmacSHA256WithSecret:(NSString *)secret content:(NSString *)content;
+ (NSString *)base64String:(NSString*)content;
@end

NS_ASSUME_NONNULL_END
