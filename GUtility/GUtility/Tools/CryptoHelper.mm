//
//  CryptoHelper.m
//  SoHappy
//
//  Created by gsh_mac_2018 on 2019/6/13.
//

#import "CryptoHelper.h"

@implementation CryptoHelper
/**
 *  加密方式,MAC算法: HmacSHA256
 *
 *  @param secret       秘钥
 *  @param content 要加密的文本
 *
 *  @return 加密后的字符串
 */
+ (NSString *)hmacSHA256WithSecret:(NSString *)secret content:(NSString *)content
{
    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:secret options:0];
    /*const unsigned char *buffer = (const unsigned char *)[decodedData bytes];
    NSMutableString *aHMAC = [NSMutableString stringWithCapacity:decodedData.length * 2];
    for (int i = 0; i < decodedData.length; ++i){
        [aHMAC appendFormat:@"%02x", buffer[i]];
    }*/
    //NSLog(@"key is %@",aHMAC);
    //const char *cKey  = [secret cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cData = [content cStringUsingEncoding:NSUTF8StringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, [decodedData bytes], decodedData.length, cData, strlen(cData), &cHMAC);
    
    NSData *HMACData = [NSData dataWithBytes:cHMAC length:sizeof(cHMAC)];
    NSString *HMAC = [HMACData base64EncodedStringWithOptions:0];
    
    const unsigned char *buffer = (const unsigned char *)[HMACData bytes];
    NSMutableString *aHMAC = [NSMutableString stringWithCapacity:HMACData.length * 2];
    for (int i = 0; i < HMACData.length; ++i){
        [aHMAC appendFormat:@"%02x", buffer[i]];
    }
    NSLog(@"HMAC is %@",aHMAC);
    return HMAC;
    /*const char *cKey  = [secret cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cData = [content cStringUsingEncoding:NSUTF8StringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSString *hash;
    
    NSMutableString* output = [NSMutableString   stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", cHMAC[i]];
    hash = output;
    NSLog(@"HMAC is %@",hash);
    return hash;*/
    
 
}
+ (NSString *)base64String:(NSString*)content
{
   // Encoding
    NSData *plainData = [content dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainData base64EncodedStringWithOptions:0];
    NSLog(@"%@", base64String); // SGFwcHlNYW4=
    return base64String;
}

@end
