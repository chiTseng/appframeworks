//
//  GInputTools.h
//  GUtility
//
//  Created by gsh_mac_2018 on 2019/7/22.
//  Copyright © 2019 gsh_mac_2018. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GInputTools : NSObject
+ (BOOL) hasNameSpecialCharacter:(NSString *)string;
+ (BOOL) hasSpecialCharacter:(NSString *)string;
@end

NS_ASSUME_NONNULL_END
