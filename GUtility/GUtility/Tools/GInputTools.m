//
//  GInputTools.m
//  GUtility
//
//  Created by gsh_mac_2018 on 2019/7/22.
//  Copyright © 2019 gsh_mac_2018. All rights reserved.
//

#import "GInputTools.h"

@implementation GInputTools
+ (BOOL) hasNameSpecialCharacter:(NSString *)string
{
    NSCharacterSet * invalidNumberSet = [NSCharacterSet characterSetWithCharactersInString:@"[`~!@#$^&*()=|{}':;',\\[\\].<>/?~!@#￥……&*()——|{}【】‘;:”“'。,、?%]€£¥•+_-\""];
    
    NSScanner * scanner = [NSScanner scannerWithString:string];
    NSString  * scannerResult;
    
    [scanner setCharactersToBeSkipped:nil];
    
    while (![scanner isAtEnd])
    {
        if([scanner scanUpToCharactersFromSet:invalidNumberSet intoString:&scannerResult])
        {
            return NO;
        }
        else
        {
            if(![scanner isAtEnd])
            {
                [scanner setScanLocation:[scanner scanLocation]+1];
            }
        }
    }
    
    return YES;
}
+ (BOOL) hasSpecialCharacter:(NSString *)string
{//"[`~!@#$^&*()=|{}':;',\\[\\].<>/?~!@#￥……&*()——|{}【】‘;:”“'。,、?%]"
    NSCharacterSet * invalidNumberSet = [NSCharacterSet characterSetWithCharactersInString:@"\n!#$%^&*()[]{}'\",<>:;|\\/?+=\t~` •¥£€"];
    
    NSScanner * scanner = [NSScanner scannerWithString:string];
    NSString  * scannerResult;
    
    [scanner setCharactersToBeSkipped:nil];
    
    while (![scanner isAtEnd])
    {
        if([scanner scanUpToCharactersFromSet:invalidNumberSet intoString:&scannerResult])
        {
            return NO;
        }
        else
        {
            if(![scanner isAtEnd])
            {
                [scanner setScanLocation:[scanner scanLocation]+1];
            }
        }
    }
    
    return YES;
}
@end
