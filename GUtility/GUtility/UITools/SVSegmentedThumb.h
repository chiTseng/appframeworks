//
// SVSegmentedThumb.h
// SVSegmentedControl
//
// Created by Sam Vermette on 25.05.11.
// Copyright 2011 Sam Vermette. All rights reserved.
//
// https://github.com/samvermette/SVSegmentedControl
//

#import <UIKit/UIKit.h>

@class SVSegmentedControl;

@interface SVSegmentedThumb : UIView
{
    UIImage *backgroundImage; // default is nil;
    UIImage *highlightedBackgroundImage; // default is nil;
    
    UIColor *tintColor; // default is [UIColor grayColor]
    UIColor *textColor; // default is [UIColor whiteColor]
    UIColor *textShadowColor; // default is [UIColor blackColor]
    CGSize textShadowOffset; // default is CGSizeMake(0, -1)
    BOOL shouldCastShadow; // default is YES (NO when backgroundImage is set)
    
    // deprecated properties
    UIColor *shadowColor DEPRECATED_ATTRIBUTE; // use textShadowColor instead
    CGSize shadowOffset DEPRECATED_ATTRIBUTE; // use textShadowOffset instead
    BOOL castsShadow DEPRECATED_ATTRIBUTE; // use shouldCastShadow instead    
}
@property (nonatomic, retain) UIImage *backgroundImage; // default is nil;
@property (nonatomic, retain) UIImage *highlightedBackgroundImage; // default is nil;

@property (nonatomic, retain) UIColor *tintColor; // default is [UIColor grayColor]
@property (nonatomic, retain) UIColor *textColor; // default is [UIColor whiteColor]
@property (nonatomic, retain) UIColor *textShadowColor; // default is [UIColor blackColor]
@property (nonatomic, assign) CGSize textShadowOffset; // default is CGSizeMake(0, -1)
@property (nonatomic, assign) BOOL shouldCastShadow; // default is YES (NO when backgroundImage is set)

// deprecated properties
@property (nonatomic, retain) UIColor *shadowColor DEPRECATED_ATTRIBUTE; // use textShadowColor instead
@property (nonatomic, assign) CGSize shadowOffset DEPRECATED_ATTRIBUTE; // use textShadowOffset instead
@property (nonatomic, assign) BOOL castsShadow DEPRECATED_ATTRIBUTE; // use shouldCastShadow instead

@end
