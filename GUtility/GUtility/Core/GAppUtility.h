//
//  AppUtility.h
//  SoHappy
//
//  Created by Huang Tsung-Jen on 2012/2/10.
//  Copyright (c) 2012年 ITRI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define BUNDLE_PATH(BUNDLE_NAME,_URL) [GAppUtility fullBundlePath:[NSString stringWithFormat:@"%@/%@", BUNDLE_NAME,_URL]]

@interface GAppUtility : NSObject
{
    
}
+ (NSString*) fullBundlePath:(NSString*)bundlePath;
+ (NSString *) generateUuidString;
+ (NSString *) getDocumentFolderPath;
+ (NSString *) stringFromInternetDateTime:(NSDate *)date withFormat: (NSString *)format;
+ (void) resignKeyboard:(UIView*) view;
+ (BOOL) validEmail:(NSString*) emailString;
+ (BOOL) validNumeric:(NSString *)inputString;
+ (BOOL) validDecimalNumeric:(NSString *)inputString;
+ (NSDate *) dateForNow;
+ (NSDateComponents *) getDateComponents:(NSDate *)date;
+ (UIImage *) FixImageSize:(UIImage *)source;
+ (UIImage *) scaleImage:(UIImage *)image toScale:(float)scaleSize;
+ (void) setRoundedDateForDatePicker:(UIDatePicker *) datePicker withDate:(NSDate *) date withInterval: (int) interval;
+ (BOOL) hasDigits:(NSString *)inputString;
+ (UIImage*) resizeImage:(UIImage*)image size:(CGSize)size ;
+ (NSString *) getSpecifyStringFrom:(NSString *)original With:(NSString *)stringSet;
+ (NSString *) dateStringConvertToRFC3339String:(NSString *)date Time:(NSString *)timeString;
+ (NSString *) getSubstring:(NSString *)value betweenStringStart:(NSString *)StartSeparator betweenStringEnd:(NSString *)EndSeparator;
+ (UIColor *) colorWithHexString:(NSString *)hexString;
+ (NSString *) hexFromInt:(NSNumber *)val;
+ (CIImage *) createQRForString:(NSString *)qrString;
+ (void) saveQRCodeToAlbum:(NSString*)str;
+ (UIBarButtonItem*)UIButtonToUIBarButtonItem:(UIButton*)btn size:(CGSize)size;
@end
