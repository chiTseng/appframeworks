//
//  DateTimeHelper.h
//  SoHappy
//
//  Created by 52-000n400-95 on 12/2/29.
//  Copyright (c) 2012年 ITRI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateTimeHelper : NSObject
-(NSString *) getDateTimeString:(NSDate *)_date;
@end
