//
//  AppUtility.m
//  SoHappy
//
//  Created by Huang Tsung-Jen on 2012/2/10.
//  Copyright (c) 2012年 ITRI. All rights reserved.
//

#import "GAppUtility.h"

@implementation GAppUtility
+ (NSString*) fullBundlePath:(NSString*)bundlePath{
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:bundlePath];
}

+ (NSString *)getDocumentFolderPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [paths objectAtIndex:0];
    return documentDir;
}
+ (NSString *)generateUuidString
{
    // create a new UUID which you own
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    // create a new CFStringRef (toll-free bridged to NSString)
    // that you own
    NSString *uuidString = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuid));
    // transfer ownership of the string
    // to the autorelease pool
    // release the UUID
    CFRelease(uuid);
    return uuidString;
}

+ (NSString *)stringFromInternetDateTime:(NSDate *)date withFormat: (NSString *)format
{
    NSDateFormatter *dateFormatter  = [[NSDateFormatter alloc] init];
    //強制只使用ＧＭＴ日曆
    NSCalendar* c = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [dateFormatter setCalendar:c];
    [dateFormatter setDateFormat:format];
    NSString* planStartDate = [dateFormatter stringFromDate:date];
    return planStartDate;
}
// Patrick 101/02/24
+ (void) resignKeyboard:(UIView*) view
{
    for (UIView *subview in view.subviews)
    {
        if ([subview isKindOfClass:[UITextField class]])
            [subview resignFirstResponder];
        else if ([subview isKindOfClass:[UIScrollView class]])
        {
            [GAppUtility resignKeyboard:subview];
        }
    }
}
+(BOOL) validEmail:(NSString*) emailString
{
    if (emailString == nil || emailString.length < 2)
        return NO;
    BOOL ret = NO;
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];

    if (regExMatches == 0)
        ret = NO;
    else
        ret = YES;

    return ret;
}
+ (BOOL) validNumeric:(NSString *)inputString
{
    if (inputString == nil || inputString.length < 1)
        return NO;
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}
+ (BOOL) validDecimalNumeric:(NSString *)inputString
{
    if (inputString == nil || inputString.length < 1)
        return NO;
    BOOL ret = NO;

    NSString *regExPattern = @"[0-9]*\\.?[0-9]*";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:inputString options:0 range:NSMakeRange(0, [inputString length])];

    if (regExMatches == 0)
        ret = NO;
    else
        ret = YES;
    return ret;
}
+ (BOOL) hasDigits:(NSString *)inputString
{
    if (inputString == nil || inputString.length < 1)
        return NO;
    BOOL ret = NO;
    NSString *regExPattern = @"[0-9]";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:inputString options:0 range:NSMakeRange(0, [inputString length])];

    if (regExMatches == 0)
        ret = NO;
    else
        ret = YES;

    return ret;
}


+ (NSDate *)dateForNow {
    NSDate* sourceDate = [NSDate date];
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    return [NSDate dateWithTimeInterval:interval sinceDate:sourceDate];
}

+ (NSDateComponents *)getDateComponents:(NSDate *)date {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:
                                    (NSYearCalendarUnit |
                                     NSMonthCalendarUnit |
                                     NSDayCalendarUnit |
                                     NSWeekdayCalendarUnit |
                                     NSHourCalendarUnit |
                                     NSMinuteCalendarUnit |
                                     NSSecondCalendarUnit ) fromDate:date];
    return components;
}
+ (UIImage *)FixImageSize:(UIImage *)source{
    UIImage *scaledImage = source;
    @try{
        if(source.size.width == source.size.height) return source;
        CGSize newSize = CGSizeMake(source.size.width, source.size.width);
        UIGraphicsBeginImageContext(newSize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        //CGRect imageRect = CGRectMake(0, ((newSize.width - newSize.height)/2.0), newSize.width, newSize.height);
        CGRect imageRect = CGRectMake(0, ((newSize.width - source.size.height)/2.0), newSize.width, source.size.height);
        CGContextTranslateCTM(context, 0, newSize.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
        CGContextFillRect(context, CGRectMake(0, 0, newSize.width, newSize.width));
        CGContextDrawImage(context, imageRect, source.CGImage);
        scaledImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    @catch (NSException *exception){
        
    }
    @finally {
        //        [scaledImage release];
        return scaledImage;
    }
}

+ (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize
{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width*scaleSize,image.size.height*scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height *scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
+ (void) setRoundedDateForDatePicker:(UIDatePicker *) datePicker withDate:(NSDate *) date withInterval: (int) interval
{
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSMinuteCalendarUnit fromDate:date];
    NSInteger minutes = [dateComponents minute];
    NSInteger minutesRounded = ( (NSInteger)(minutes / interval) ) * interval;
    NSDate *roundedDate = [[NSDate alloc] initWithTimeInterval:60.0 * (minutesRounded - minutes) sinceDate:date];
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Set the date picker's value (and the selected date on the UI display) to
    // the rounded date.
    if ([roundedDate isEqualToDate:datePicker.date])
    {
        // We need to set the date picker's value to something different than
        // the rounded date, because the second call to set the date picker's
        // date with the same value is ignored. Which could be bad since the
        // call above to set the date picker's minute interval can leave the
        // date picker with the wrong selected date (the whole reason why we are
        // doing this).
        NSDate *diffrentDate = [[NSDate alloc] initWithTimeInterval:60 sinceDate:roundedDate];
        datePicker.date = diffrentDate;
        
    }
    datePicker.date = roundedDate;
   
}

+ (BOOL) isEmailCharacter:(NSString *)string
{
    NSCharacterSet * invalidNumberSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
    NSScanner * scanner = [NSScanner scannerWithString:string];
    NSString  * scannerResult;
    
    [scanner setCharactersToBeSkipped:nil];
    
    while (![scanner isAtEnd])
    {
        if(![scanner scanUpToCharactersFromSet:invalidNumberSet intoString:&scannerResult])
        {
            return NO;
        }
        else
        {
            if(![scanner isAtEnd])
            {
                [scanner setScanLocation:[scanner scanLocation]+1];
            }
        }
    }
    
    return YES;
}

+ (UIImage*)resizeImage:(UIImage*)image size:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), image.CGImage);
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

+ (NSString *)getSpecifyStringFrom:(NSString *)original With:(NSString *)stringSet{
    NSMutableString *strippedString = [NSMutableString
                                       stringWithCapacity:original.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:original];
    NSCharacterSet *numbers = [NSCharacterSet
                               characterSetWithCharactersInString:stringSet];
    
    while ([scanner isAtEnd] == NO) {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
            [strippedString appendString:buffer];
            
        } else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    return strippedString;
}



+ (NSString *) dateStringConvertToRFC3339String:(NSString *)dateString Time:(NSString *)timeString
{
    return [NSString stringWithFormat:@"%@T%@", dateString, timeString];
}

+ (NSString *)getSubstring:(NSString *)value betweenStringStart:(NSString *)StartSeparator betweenStringEnd:(NSString *)EndSeparator
{
    NSRange firstInstance = [value rangeOfString:StartSeparator];
    if(firstInstance.length==0){ return @"";}
    NSRange secondInstance = [[value substringFromIndex:firstInstance.location + firstInstance.length] rangeOfString:EndSeparator];
    if(secondInstance.length==0){ return @"";}
    NSRange finalRange = NSMakeRange(firstInstance.location + StartSeparator.length, secondInstance.location);
    
    return [value substringWithRange:finalRange];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString
{
    unsigned int hex;
    [[NSScanner scannerWithString:hexString] scanHexInt:&hex];
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:1.0f];
}

/** Convert an int to a hex.
 
 @param val Integer value.
 @return Returns the hexadecimal value as string.
 @see isHexadecimalNumber:
 @see intFromHex:
 
 */
+ (NSString *)hexFromInt:(NSNumber *)val {
    return [NSString stringWithFormat:@"%02lX", [val longValue]];
}


+ (CIImage *)createQRForString:(NSString *)qrString {
    NSData *stringData = [qrString dataUsingEncoding: NSISOLatin1StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    //[qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    //filter.setValue("Q", forKey: "inputCorrectionLevel")
    return qrFilter.outputImage;
}

+(void) saveQRCodeToAlbum:(NSString*)str
{
    NSString *deviceMacAddr = str;
    
    CIImage *cimageFromMacQR = [GAppUtility createQRForString:deviceMacAddr];
    UIImage *QRCodeImage = [[UIImage alloc] initWithCIImage:cimageFromMacQR];
    UIGraphicsBeginImageContext(CGSizeMake(QRCodeImage.size.width * 15, QRCodeImage.size.height * 15 + 28.0));
    CGContextRef resizeContext = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(resizeContext, kCGInterpolationNone);
    
    UIFont *helveticaFont = [UIFont fontWithName:@"Helvetica" size:28.0f];
    UILabel *deviceMacLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, QRCodeImage.size.width * 15, 28.0)];
    deviceMacLbl.textAlignment = NSTextAlignmentCenter;
    deviceMacLbl.font = helveticaFont;
    deviceMacLbl.textColor = [UIColor blackColor];
    deviceMacLbl.text = deviceMacAddr;
    
    [deviceMacLbl drawTextInRect:CGRectMake(0.0, 0.0, QRCodeImage.size.width * 15, 28.0)];
    [QRCodeImage drawInRect:CGRectMake(0, 28.0, QRCodeImage.size.width * 15, QRCodeImage.size.height * 15)];
    UIImage *qrcodeImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Save QRCode to album
    UIImageWriteToSavedPhotosAlbum(qrcodeImage, nil, nil, nil);
}
+(UIBarButtonItem*)UIButtonToUIBarButtonItem:(UIButton*)btn size:(CGSize)size
{
    if (@available(iOS 9, *)) {
        [btn.widthAnchor constraintEqualToConstant: size.width].active = YES;
        [btn.heightAnchor constraintEqualToConstant: size.height].active = YES;
    }
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return barBtn;
}
@end
