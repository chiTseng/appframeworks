//
//  DateTimeHelper.m
//  SoHappy
//
//  Created by 52-000n400-95 on 12/2/29.
//  Copyright (c) 2012年 ITRI. All rights reserved.
//

#import "DateTimeHelper.h"

@implementation DateTimeHelper
-(NSString *) getDateTimeString:(NSDate *)_date
{
    NSString *result=@"";
    NSTimeInterval now = _date.timeIntervalSinceNow;
    if(now <-24*60*60) //-86400
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd HH:mm"];
        result= [NSString stringWithFormat:@"%@", [formatter stringFromDate:_date]] ;
    }
    else if(now <-1*60*60)//-3600
    {
        result= [NSString stringWithFormat:@"%d%@", (int) -_date.timeIntervalSinceNow/3600,NSLocalizedString(@"小時前",@"xxx小時前")] ;
    }
    else
    {

        result= [NSString stringWithFormat:@"%d%@",(int) -_date.timeIntervalSinceNow/60,NSLocalizedString(@"分鐘前",@"xxx分鐘前")] ;
    }
    return result;
}
@end
